import { AuthService } from './../providers/auth.service';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, CanActivate, CanActivateChild } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {
  constructor( private authService: AuthService, private router: Router ) {
  }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {

    let token = localStorage.getItem('token');
    // check token if not null
    if (token != null) {
        let role = localStorage.getItem('role');
            if (role != null) {
                return true; // allowed in
            } else {
                this.router.navigate(['/login']);
                return false; // go to login
            }
    } else {
        this.router.navigate(['/login']);
        return false; // go to login
    }

  }

  canActivateChild( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {

    let token = localStorage.getItem('token');
    // check token if not null
    if (token != null) {
        let role = localStorage.getItem('role');
        if (role != null) {
            return true; // allowed in
        } else {
            this.router.navigate(['/login']);
            return false; // go to login
        }
    } else {
        this.router.navigate(['/login']);
        return false; // go to login
    }

  }

}
