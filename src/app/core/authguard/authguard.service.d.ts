import { AuthService } from './../providers/auth.service';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, CanActivate, CanActivateChild } from '@angular/router';
export declare class AuthGuardService implements CanActivate, CanActivateChild {
    private authService;
    private router;
    constructor(authService: AuthService, router: Router);
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean;
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean;
}
