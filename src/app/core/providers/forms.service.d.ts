import { Apollo } from 'apollo-angular';
import { QueryForms, QueryFormsById, QueryFormsByName } from '../gqlquery/gqlquery';
export declare class FormsService {
    apollo: Apollo;
    constructor(apollo: Apollo);
    getFormsAll(): import("apollo-angular/build/src/ApolloQueryObservable").ApolloQueryObservable<QueryForms>;
    getFormById(no_form: string): import("apollo-angular/build/src/ApolloQueryObservable").ApolloQueryObservable<QueryFormsById>;
    getFormByName(name_form: string): import("apollo-angular/build/src/ApolloQueryObservable").ApolloQueryObservable<QueryFormsByName>;
    addForm(no_form: string, name_form: string, json_form: string): import("rxjs/Observable").Observable<import("apollo-client/core/types").ApolloExecutionResult<{}>>;
    deleteForm(no_form: string): import("rxjs/Observable").Observable<import("apollo-client/core/types").ApolloExecutionResult<{}>>;
    updateForm(no_form: string, name_form: string, json_form: string): import("rxjs/Observable").Observable<import("apollo-client/core/types").ApolloExecutionResult<{}>>;
    loadFormSubscription(): import("rxjs/Observable").Observable<any>;
}
