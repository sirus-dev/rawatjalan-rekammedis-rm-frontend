import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import {
  QueryForms,
  getFormsAll,
  QueryFormsById,
  getFormsById,
  QueryFormsByName,
  getFormsByName,
  addFormQuery,
  deleteFormQuery,
  updateFormQuery,
  FormsExchangeQuery
} from '../gqlquery/gqlquery';

@Injectable()
export class FormsService {

  // private API_EMAIL = "/api/edit/request";

  constructor(public apollo: Apollo) { }


  public getFormsAll() {
    return this.apollo.watchQuery<QueryForms>({
      query: getFormsAll
      // pollInterval: 1000
    });
  }

  public getFormById(no_form: string) {
    return this.apollo.watchQuery<QueryFormsById>({
      query: getFormsById,
      variables: {
        no_form: no_form
      }
    });
  }

  public getFormByName(name_form: string) {
    return this.apollo.watchQuery<QueryFormsByName>({
      query: getFormsByName,
      variables: {
        name_form: name_form
      }
    });
  }

  public addForm(no_form: string, name_form: string, json_form: string) {
    return this.apollo.mutate({
      mutation: addFormQuery,
      variables: {
        no_form: no_form,
        name_form: name_form,
        json_form: json_form
      },
      refetchQueries: [{
        query: getFormsAll
      }]
    });
  }

  public deleteForm(no_form: string) {
    return this.apollo.mutate({
      mutation: deleteFormQuery,
      variables: {
        no_form: no_form
      },
      refetchQueries: [{
        query: getFormsAll
      }]
    });
  }

  public updateForm(no_form: string, name_form: string, json_form: string) {
    return this.apollo.mutate({
      mutation: updateFormQuery,
      variables: {
        no_form: no_form,
        name_form: name_form,
        json_form: json_form
      },
      refetchQueries: [{
        query: getFormsAll
      }, {
        query: getFormsById,
        variables: {
          no_form: no_form
        }
      }],
    });
  }

  public loadFormSubscription() {
    return this.apollo.subscribe({
      query: FormsExchangeQuery,
    });
  }

}
