var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { getFormsAll, getFormsById, getFormsByName, addFormQuery, deleteFormQuery, updateFormQuery, FormsExchangeQuery } from '../gqlquery/gqlquery';
var FormsService = /** @class */ (function () {
    // private API_EMAIL = "/api/edit/request";
    function FormsService(apollo) {
        this.apollo = apollo;
    }
    FormsService.prototype.getFormsAll = function () {
        return this.apollo.watchQuery({
            query: getFormsAll
            // pollInterval: 1000
        });
    };
    FormsService.prototype.getFormById = function (no_form) {
        return this.apollo.watchQuery({
            query: getFormsById,
            variables: {
                no_form: no_form
            }
        });
    };
    FormsService.prototype.getFormByName = function (name_form) {
        return this.apollo.watchQuery({
            query: getFormsByName,
            variables: {
                name_form: name_form
            }
        });
    };
    FormsService.prototype.addForm = function (no_form, name_form, json_form) {
        return this.apollo.mutate({
            mutation: addFormQuery,
            variables: {
                no_form: no_form,
                name_form: name_form,
                json_form: json_form
            },
            refetchQueries: [{
                    query: getFormsAll
                }]
        });
    };
    FormsService.prototype.deleteForm = function (no_form) {
        return this.apollo.mutate({
            mutation: deleteFormQuery,
            variables: {
                no_form: no_form
            },
            refetchQueries: [{
                    query: getFormsAll
                }]
        });
    };
    FormsService.prototype.updateForm = function (no_form, name_form, json_form) {
        return this.apollo.mutate({
            mutation: updateFormQuery,
            variables: {
                no_form: no_form,
                name_form: name_form,
                json_form: json_form
            },
            refetchQueries: [{
                    query: getFormsAll
                }, {
                    query: getFormsById,
                    variables: {
                        no_form: no_form
                    }
                }],
        });
    };
    FormsService.prototype.loadFormSubscription = function () {
        return this.apollo.subscribe({
            query: FormsExchangeQuery,
        });
    };
    FormsService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Apollo])
    ], FormsService);
    return FormsService;
}());
export { FormsService };
//# sourceMappingURL=forms.service.js.map