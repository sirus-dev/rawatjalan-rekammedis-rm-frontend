import { Http, Headers, Response } from '@angular/http';
import { JwtHelper } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
// import { LoginComponent } from 'app/pages/auth/login/login.component';



@Injectable()
export class AuthService {

  private API_AUTH          = "/auth/authentication";
  private API_TOKEN         = "/auth/authentication/token";
  private APIDATA           = "/auth/data";

  contentHeader             = new Headers({"Content-Type": "application/json"});
  jwtHelper                 = new JwtHelper();
  userEmail: string;
  validateLoading: boolean  = false;
  messageAlert: string;

  constructor(private http: Http, private router: Router) {}

  public login(credentials): Observable<any> {

    if (credentials.email === null || credentials.password === null) {
      return Observable.throw('Please Insert Credentials');
    } else {
      return this.http.post(this.API_AUTH, JSON.stringify(credentials), { headers: this.contentHeader })
      .map(res => res.json());
    }

  }

  public forgetPassword(credentials): Observable<any> {

    if (credentials.email === null) {
      return Observable.throw('Please Insert Credentials');
    } else {
      return this.http.get(this.API_AUTH + "/forget/" + credentials.email, { headers: this.contentHeader })
      .map(res => res.json());
    }

  }

  public async saveToken(data, credentials) {
      // save token to localstorage
      let expirationMS = 30 * 60 * 1000;
      let session = new Date().getTime() + expirationMS;

      await localStorage.setItem('token', data.token);
      await localStorage.setItem('session', session.toString());

      let userToken = localStorage.getItem('token');
      if (userToken) {
        await this.getAccount(credentials); // get data account from credentials
      }

  }

  private getAccount(credentials) {

    if (credentials.email === null || credentials.password === null) {
        return Observable.throw('Please Insert Credentials');
    } else {
        return this.http.get(this.APIDATA + "/" +  credentials.email, { headers: this.contentHeader })
        .map(res  => res.json())
        .subscribe(
            data  => this.saveDataAccount(JSON.stringify(data)),
            error => console.log(error)
        );
    }

  }

  private saveDataAccount(data) {
    // save nik and role
    let dataaccount = JSON.parse(data);
    let staff = dataaccount;
    console.log(staff[0].role);
    if (staff[0].role === "rekammedis") {
        localStorage.setItem('nik', staff[0].nik);
        localStorage.setItem('role', staff[0].role);
    }

  }

  public tokenCompare(token): Observable<any> {

    return this.http.get(this.API_TOKEN + "/" + token)
                  .map((res: Response)  => res.json())
                  .catch((error: any)   => Observable.throw(error.json().error || 'Server error'));

  }

  public logout() {

    return Observable.create(observer => {
      // destroy token
      let removeToken = localStorage.removeItem('token');
      observer.next(removeToken);
      observer.complete();
    });

  }

}
