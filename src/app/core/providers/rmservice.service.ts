import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {
    getCTByDate,
    getDoctorByIdQuery,
    getExamineByVisit,
    getExamineJoinByDateRange,
    getIcd9,
    getIntegratedNote1Query,
    getIntegratedNoteExamineQuery,
    getIntegratedNoteQuery,
    getListDoctorsQuery,
    getPatientByIdQuery,
    getPatientByNameQuery,
    getPatientExamineQuery,
    getPatientMedRecByIdQuery,
    getPatientMedRecQuery,
    getPatientQuery,
    getReportBasic,
    getReportBasicById,
    getReportBasicNodok,
    getReviewByIdQuery,
    QueryCTByDate,
    QueryCTByExamine,
    QueryCTById,
    QueryCTById1,
    QueryDoctorById,
    QueryExamineAll,
    QueryExamineAllByNodok,
    QueryExamineJoinByDateRange,
    QueryExamineJoinById,
    QueryExamineJoinByVisit,
    QueryICD9,
    QueryListDoctors,
    QueryPatient,
    QueryPatientById,
    QueryPatientByName,
    QueryPatientExamine,
    QueryPatientMedRec,
    QueryPatientMedRecById,
    QueryReviewById,
    updateCT,
    QueryCTByIdDate,
    getIntegratedNoteByIdDateQuery,
} from '../gqlquery/gqlquery';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';

@Injectable()
export class RmserviceService {

  private API_EMAIL = "/api/edit/request";

  constructor(public apollo: Apollo, private http: Http) { }

  public emailrequest(no_periksa, alasan, date) {

    if (no_periksa === null || alasan === null) {
      return Observable.throw('Pelase Insert Credentials');
    } else {
      return this.http.get(this.API_EMAIL + "/" + no_periksa + "&" + alasan + "&" + date).map(res => res.json());
    }

  }

  public getDataPatient() {
    return this.apollo.watchQuery<QueryPatient>({
      query: getPatientQuery
    });
  }

  public getDataPatientMedRec() {
    return this.apollo.watchQuery<QueryPatientMedRec>({
      query: getPatientMedRecQuery,
      variables: {
        offset: 0,
        limit: 25
      }
    });
  }

  public getDataPatientMedRecById(no_medrec: string) {
    return this.apollo.watchQuery<QueryPatientMedRecById>({
      query: getPatientMedRecByIdQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getReviewById(no_medrec: string) {
    return this.apollo.watchQuery<QueryReviewById>({
      query: getReviewByIdQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getPatientById(no_medrec: string) {
    return this.apollo.watchQuery<QueryPatientById>({
      query: getPatientByIdQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getPatientByName(nama: string) {
    return this.apollo.watchQuery<QueryPatientByName>({
      query: getPatientByNameQuery,
      variables: {
        nama: nama
      }
    });
  }

  public getDoctorById(nodok: string) {
    return this.apollo.watchQuery<QueryDoctorById>({
      query: getDoctorByIdQuery,
      variables: {
        nodok: nodok
      }
    });
  }

  public getDataIntegratedNote(no_medrec: string) {
    return this.apollo.watchQuery<QueryCTById>({
      query: getIntegratedNoteQuery,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getDataIntegratedNoteByDate(no_medrec: string, tgl_ct: string) {
    return this.apollo.watchQuery<QueryCTByIdDate>({
      query: getIntegratedNoteByIdDateQuery,
      variables: {
        no_medrec: no_medrec,
        tgl_ct: tgl_ct
      }
    });
  }

  public getDataIntegratedNoteExamine(no_periksa: string) {
    return this.apollo.watchQuery<QueryCTByExamine>({
      query: getIntegratedNoteExamineQuery,
      variables: {
        no_periksa: no_periksa
      }
    });
  }

  public getDataIntegratedNote1(no_medrec: string) {
    return this.apollo.watchQuery<QueryCTById1>({
      query: getIntegratedNote1Query,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getDataListDoctors(offset: number, limit: number) {
    return this.apollo.watchQuery<QueryListDoctors>({
      query: getListDoctorsQuery,
      variables: {
        offset: offset,
        limit: limit
      }
    });
  }

  public getDataPatientExamine(nodok: string) {
    return this.apollo.watchQuery<QueryPatientExamine>({
      query: getPatientExamineQuery,
      variables: {
        nodok: nodok
      }
    });
  }

  public getDataICD9(deskripsi: string, offset: number, limit: number) {
    return this.apollo.watchQuery<QueryICD9>({
      query: getIcd9,
      variables: {
        deskripsi: deskripsi,
        offset: offset,
        limit: limit
      }
    });
  }

  public updateCTICD9(no_medrec: string, a_icd9: any) {
    return this.apollo.mutate({
      mutation: updateCT,
      variables: {
        no_medrec: no_medrec,
        a_icd9: a_icd9
      }
    });
  }

  public getDataReportBasic(offset: number, limit: number) {
    return this.apollo.watchQuery<QueryExamineAll>({
      query: getReportBasic,
      variables: {
        offset: offset,
        limit: limit
      }
    });
  }

  public getDataReportBasicNodok(offset: number, limit: number) {
    return this.apollo.watchQuery<QueryExamineAllByNodok>({
      query: getReportBasicNodok,
      variables: {
        offset: offset,
        limit: limit
      }
    });
  }

  public getDataReportBasicById(no_medrec: string) {
    return this.apollo.watchQuery<QueryExamineJoinById>({
      query: getReportBasicById,
      variables: {
        no_medrec: no_medrec
      }
    });
  }

  public getIntegratedNoteByDateQuery(no_medrec: string, tgl_ct: string) {
    return this.apollo.watchQuery<QueryCTByDate>({
      query: getCTByDate,
      variables: {
        no_medrec: no_medrec,
        tgl_ct: tgl_ct
      }
    });
  }

  public getDataReportByVisit(kunjungan: string, offset: number, limit: number) {
    return this.apollo.watchQuery<QueryExamineJoinByVisit>({
      query: getExamineByVisit,
      variables: {
        kunjungan: kunjungan,
        offset: offset,
        limit: limit
      }
    });
  }

  public getDataReportByDateRange(startdate: string, enddate: string) {
    return this.apollo.watchQuery<QueryExamineJoinByDateRange>({
      query: getExamineJoinByDateRange,
      variables: {
        startDate: startdate,
        endDate: enddate
      }
    });
  }

}
