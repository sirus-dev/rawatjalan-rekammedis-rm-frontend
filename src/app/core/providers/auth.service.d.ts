import { Http, Headers } from '@angular/http';
import { JwtHelper } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
export declare class AuthService {
    private http;
    private router;
    private API_AUTH;
    private API_TOKEN;
    private APIDATA;
    contentHeader: Headers;
    jwtHelper: JwtHelper;
    userEmail: string;
    validateLoading: boolean;
    messageAlert: string;
    constructor(http: Http, router: Router);
    login(credentials: any): Observable<any>;
    forgetPassword(credentials: any): Observable<any>;
    saveToken(data: any, credentials: any): Promise<void>;
    private getAccount;
    private saveDataAccount;
    tokenCompare(token: any): Observable<any>;
    logout(): any;
}
