var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { getCTByDate, getDoctorByIdQuery, getExamineByVisit, getExamineJoinByDateRange, getIcd9, getIntegratedNote1Query, getIntegratedNoteExamineQuery, getIntegratedNoteQuery, getListDoctorsQuery, getPatientByIdQuery, getPatientByNameQuery, getPatientExamineQuery, getPatientMedRecByIdQuery, getPatientMedRecQuery, getPatientQuery, getReportBasic, getReportBasicById, getReportBasicNodok, getReviewByIdQuery, updateCT, getIntegratedNoteByIdDateQuery, } from '../gqlquery/gqlquery';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
var RmserviceService = /** @class */ (function () {
    function RmserviceService(apollo, http) {
        this.apollo = apollo;
        this.http = http;
        this.API_EMAIL = "/api/edit/request";
    }
    RmserviceService.prototype.emailrequest = function (no_periksa, alasan, date) {
        if (no_periksa === null || alasan === null) {
            return Observable.throw('Pelase Insert Credentials');
        }
        else {
            return this.http.get(this.API_EMAIL + "/" + no_periksa + "&" + alasan + "&" + date).map(function (res) { return res.json(); });
        }
    };
    RmserviceService.prototype.getDataPatient = function () {
        return this.apollo.watchQuery({
            query: getPatientQuery
        });
    };
    RmserviceService.prototype.getDataPatientMedRec = function () {
        return this.apollo.watchQuery({
            query: getPatientMedRecQuery,
            variables: {
                offset: 0,
                limit: 25
            }
        });
    };
    RmserviceService.prototype.getDataPatientMedRecById = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getPatientMedRecByIdQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    RmserviceService.prototype.getReviewById = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getReviewByIdQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    RmserviceService.prototype.getPatientById = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getPatientByIdQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    RmserviceService.prototype.getPatientByName = function (nama) {
        return this.apollo.watchQuery({
            query: getPatientByNameQuery,
            variables: {
                nama: nama
            }
        });
    };
    RmserviceService.prototype.getDoctorById = function (nodok) {
        return this.apollo.watchQuery({
            query: getDoctorByIdQuery,
            variables: {
                nodok: nodok
            }
        });
    };
    RmserviceService.prototype.getDataIntegratedNote = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getIntegratedNoteQuery,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    RmserviceService.prototype.getDataIntegratedNoteByDate = function (no_medrec, tgl_ct) {
        return this.apollo.watchQuery({
            query: getIntegratedNoteByIdDateQuery,
            variables: {
                no_medrec: no_medrec,
                tgl_ct: tgl_ct
            }
        });
    };
    RmserviceService.prototype.getDataIntegratedNoteExamine = function (no_periksa) {
        return this.apollo.watchQuery({
            query: getIntegratedNoteExamineQuery,
            variables: {
                no_periksa: no_periksa
            }
        });
    };
    RmserviceService.prototype.getDataIntegratedNote1 = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getIntegratedNote1Query,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    RmserviceService.prototype.getDataListDoctors = function (offset, limit) {
        return this.apollo.watchQuery({
            query: getListDoctorsQuery,
            variables: {
                offset: offset,
                limit: limit
            }
        });
    };
    RmserviceService.prototype.getDataPatientExamine = function (nodok) {
        return this.apollo.watchQuery({
            query: getPatientExamineQuery,
            variables: {
                nodok: nodok
            }
        });
    };
    RmserviceService.prototype.getDataICD9 = function (deskripsi, offset, limit) {
        return this.apollo.watchQuery({
            query: getIcd9,
            variables: {
                deskripsi: deskripsi,
                offset: offset,
                limit: limit
            }
        });
    };
    RmserviceService.prototype.updateCTICD9 = function (no_medrec, a_icd9) {
        return this.apollo.mutate({
            mutation: updateCT,
            variables: {
                no_medrec: no_medrec,
                a_icd9: a_icd9
            }
        });
    };
    RmserviceService.prototype.getDataReportBasic = function (offset, limit) {
        return this.apollo.watchQuery({
            query: getReportBasic,
            variables: {
                offset: offset,
                limit: limit
            }
        });
    };
    RmserviceService.prototype.getDataReportBasicNodok = function (offset, limit) {
        return this.apollo.watchQuery({
            query: getReportBasicNodok,
            variables: {
                offset: offset,
                limit: limit
            }
        });
    };
    RmserviceService.prototype.getDataReportBasicById = function (no_medrec) {
        return this.apollo.watchQuery({
            query: getReportBasicById,
            variables: {
                no_medrec: no_medrec
            }
        });
    };
    RmserviceService.prototype.getIntegratedNoteByDateQuery = function (no_medrec, tgl_ct) {
        return this.apollo.watchQuery({
            query: getCTByDate,
            variables: {
                no_medrec: no_medrec,
                tgl_ct: tgl_ct
            }
        });
    };
    RmserviceService.prototype.getDataReportByVisit = function (kunjungan, offset, limit) {
        return this.apollo.watchQuery({
            query: getExamineByVisit,
            variables: {
                kunjungan: kunjungan,
                offset: offset,
                limit: limit
            }
        });
    };
    RmserviceService.prototype.getDataReportByDateRange = function (startdate, enddate) {
        return this.apollo.watchQuery({
            query: getExamineJoinByDateRange,
            variables: {
                startDate: startdate,
                endDate: enddate
            }
        });
    };
    RmserviceService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Apollo, Http])
    ], RmserviceService);
    return RmserviceService;
}());
export { RmserviceService };
//# sourceMappingURL=rmservice.service.js.map