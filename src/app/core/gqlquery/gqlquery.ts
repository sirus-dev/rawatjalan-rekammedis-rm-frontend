import gql from 'graphql-tag';

export interface QueryPatient {
    patientAll;
}

export interface QueryPatientMedRec {
    allMedrec;
}

export interface QueryPatientMedRecById {
    medRec;
}

export interface QueryPatientById {
    patientByNorm;
}

export interface QueryPatientByName {
    patientByName;
}

export interface QueryReviewById {
    reviewByMedrec;
}

export interface QueryDoctorById {
    doctorByNodok;
}

export interface QueryCTById {
    ctByMedrec;
}

export interface QueryCTByIdDate {
  ctByMedrecTgl;
}

export interface QueryCTById1 {
  ctByMedrec1;
}

export interface QueryCTByExamine {
  ctByExamine;
}

export interface QueryListDoctors {
    doctors;
}

export interface QueryPatientExamine {
    examineByNodok;
}

export interface QueryICD9 {
    icd9Pagination;
}

export interface QueryExamineAll {
    examineAll;
}

export interface QueryExamineAllByNodok {
    examineAllByNodok;
}

export interface QueryExamineJoinById {
    examineJoinById;
}

export interface QueryExamineJoinByVisit {
  examineJoinByVisit;
}

export interface QueryExamineJoinByDateRange {
  examineJoinByDateRange;
}

export interface QueryCTByDate {
  ctByDate;
}

export interface QueryForms {
  formAll;
}

export interface QueryFormsById {
  formById;
}

export interface QueryFormsByName {
  formByName;
}

export interface FormsExchangeQuery {
  formDataExchange;
}

export const getPatientMedRecQuery = gql`
query getPatientMedRecQuery($offset:Int!, $limit:Int!){
  allMedrec(offset:$offset, limit:$limit) {
    no_medrec
    pasien {
      no_medrec
      name
      nickName
      gender
    }
    kajian {
      no_kaji
      no_medrec
      td
      nadi
      nadi1
      pernafasan
      pernafasan1
      suhu
      tb
      bb
    }
    periksa {
      nama_dok
    }
  }
}
`;

export const getPatientMedRecByIdQuery = gql`
query getPatientMedRecByIdQuery($no_medrec:String!){
  medRec(no_medrec:$no_medrec) {
    no_medrec
    pasien {
      no_medrec
      name
      nickName
      gender
    }
    kajian {
      no_kaji
      no_medrec
      td
      nadi
      pernafasan
      suhu
      tb
      bb
    }
    periksa {
      nama_dok
    }
  }
}
`;

export const getPatientQuery = gql`
query getPatientQuery{
    patientAll{
        nama,
        no_medrec,
        tgl_lahir,
        tahun,
        kelamin,
        agama,
        pekerjaan,
        telp,
        kota,
        kecamatan,
        desa,
        jln1,
        no,
        rt,
        rw,
    }
}
`;

export const getPatientByNameQuery = gql`
query getPatientByNameQuery($nama: String!) {
  patientByName(nama: $nama) {
    no_medrec
  }
}
`;

export const getReviewByIdQuery = gql`
query getReviewByIdQuery($no_medrec: String!) {
  reviewByMedrec(no_medrec: $no_medrec) {
    keluhan,
    td,
    nadi,
    pernafasan,
    suhu,
    tb,
    bb
  }
}
`;

export const getPatientByIdQuery = gql`
query getPatientByIdQuery($no_medrec: String!) {
  patientByNorm(no_medrec: $no_medrec) {
    nama,
    no_medrec,
    tgl_lahir,
    tahun,
    kelamin,
    agama,
    pekerjaan,
    telp,
    kota,
    kecamatan,
    desa,
    jln1,
    no,
    rt,
    rw,
  }
}
`;

export const getDoctorByIdQuery = gql`
query getDoctorByIdQuery($nodok: String!) {
  doctorByNodok(nodok: $nodok) {
    nama_dok,
    specialis
  }
}
`;

export const getIntegratedNoteExamineQuery = gql`
query IntegratedNoteExamineQuery(
  $no_periksa: String
){
  ctByExamine(
    no_periksa: $no_periksa,
  ){
    id_ct,
    no_medrec,
    nama_pasien,
    nama_dok,
    no_periksa,
    jaminan,
    tgl_lahir,
    umur,
    kelamin,
    kelas,
    tgl_ct,
    s_stroke,
    s_description,
    o_stroke,
    o_description,
    a_stroke,
    a_description,
    a_icdx,
    a_icd9,
    p_lab_stroke,
    p_lab_description,
    p_rad_stroke,
    p_rad_description,
    p_fisio_stroke,
    p_fisio_description,

    p_tindakan_stroke,
    p_tindakan_description,
    p_obat_stroke,
    p_obat_description,
    p_obat_nama
  }
}
`;

export const getIntegratedNoteQuery = gql`
query IntegratedNoteQuery(
  $no_medrec: String!
){
  ctByMedrec(
    no_medrec: $no_medrec,
  ){
    id_ct,
    no_medrec,
    nama_pasien,
    nama_dok,
    no_periksa,
    jaminan,
    tgl_lahir,
    umur,
    kelamin,
    kelas,
    tgl_ct,
    s_stroke,
    s_description,
    o_stroke,
    o_description,
    a_stroke,
    a_description,
    a_icdx,
    p_lab_stroke,
    p_lab_description,
    p_rad_stroke,
    p_rad_description,
    p_fisio_stroke,
    p_fisio_description,
    p_tindakan_stroke,
    p_tindakan_description,
    p_obat_stroke,
    p_obat_description,
    p_obat_nama
  }
}
`;

export const getIntegratedNoteByIdDateQuery = gql`
query getIntegratedNoteByIdDateQuery(
  $no_medrec: String!
  $tgl_ct: String!
){
  ctByMedrecTgl(
    no_medrec: $no_medrec,
    tgl_ct: $tgl_ct
  ){
    id_ct,
    no_medrec,
    nama_pasien,
    nama_dok,
    no_periksa,
    jaminan,
    tgl_lahir,
    umur,
    kelamin,
    kelas,
    tgl_ct,
    s_stroke,
    s_description,
    o_stroke,
    o_description,
    a_stroke,
    a_description,
    a_icdx,
    p_lab_stroke,
    p_lab_description,
    p_rad_stroke,
    p_rad_description,
    p_fisio_stroke,
    p_fisio_description,
    p_tindakan_stroke,
    p_tindakan_description,
    p_obat_stroke,
    p_obat_description,
    p_obat_nama
  }
}
`;

export const getIntegratedNote1Query = gql`
query IntegratedNote1Query(
  $no_medrec: String!
){
  ctByMedrec1(
    no_medrec: $no_medrec,
  ){
    id_ct,
    no_medrec,
    nama_pasien,
    nodok,
    nama_dok,
    jaminan,
    tgl_lahir,
    umur,
    kelamin,
    kelas,
    tgl_ct,
    s_description,
    o_description,
    a_description,
    a_icdx,
    a_icd9,
    p_lab_description,
    p_rad_description,
    p_fisio_description,
    p_tindakan_description,
    p_obat_description,
    p_obat_nama
  }
}
`;

export const getListDoctorsQuery = gql`
query getListDoctorsQuery($offset:Int!, $limit:Int!){
  doctors(offset:$offset, limit:$limit) {
    nodok
    nama_dok
    specialis
    nm_panggil
    pekerjaan
    pt
  }
}
`;

export const getPatientExamineQuery = gql`
query getPatientExamineQuery($nodok:String!) {
  examineByNodok(nodok:$nodok) {
    no_periksa
    poli
    tgl
    nodok
    nama_dok
    no_medrec
    nama_pasien
    jaminan
    umur
    kelamin
    flag
  }
}
`;

export const getIcd9 = gql`
query getIcd9($deskripsi:String!, $offset:Int, $limit:Int){
  icd9Pagination(deskripsi:$deskripsi, offset:$offset, limit:$limit){
    kode,
    deskripsi,
    klasifikasi,
    kelompok
  }
}
`;

export const updateCT = gql`
mutation updateCT($no_medrec: String!, $a_icd9: String!) {
  updateCtIcd9(no_medrec: $no_medrec, a_icd9: $a_icd9) {
  no_medrec,
  a_icd9
}
}
`;

export const getReportBasicById = gql`
query getReportBasicById($no_medrec:String!){
  examineJoinById(no_medrec:$no_medrec) {
    no_periksa
    kode_unit
    poli
    tgl
    nodok {
      number
      specialist
    }
    nama_dok
    no_registrasi
    no_medrec {
      no_medrec
      address
    }
    nama_pasien
    diagnosa
    diagnosa_stroke
    no_antrian
    kasus
    jaminan
    kunjungan
    rujuk
    umur
    tgl_lahir
    kelamin
    status_pasien
    kaji
    flag
  }
}
`;

export const getReportBasic = gql`
query getReportBasic($offset:Int!, $limit:Int!){
  examineAll(offset:$offset,limit:$limit) {
    no_periksa
    kode_unit
    poli
    tgl
    nodok {
      nodok
      specialist
    }
    nama_dok
    no_registrasi
    no_medrec {
      no_medrec
      address
    }
    nama_pasien
    diagnosa
    diagnosa_stroke
    no_antrian
    kasus
    jaminan
    kunjungan
    rujuk
    umur
    tgl_lahir
    kelamin
    status_pasien
    kaji
    flag
  }
}
`;

export const getReportBasicNodok = gql`
query getReportBasic($offset:Int!, $limit:Int!){
  examineAllByNodok(offset:$offset,limit:$limit) {
    no_periksa
    kode_unit
    poli
    tgl
    nodok {
      nodok
      specialist
    }
    nama_dok
    no_registrasi
    no_medrec {
      no_medrec
      address
    }
    nama_pasien
    diagnosa
    diagnosa_stroke
    no_antrian
    kasus
    jaminan
    kunjungan
    rujuk
    umur
    tgl_lahir
    kelamin
    status_pasien
    kaji
    flag
  }
}
`;

export const getCTByDate = gql`
query getCTByDate($no_medrec:String, $tgl_ct:String){
  ctByDate(no_medrec:$no_medrec, tgl_ct:$tgl_ct) {
    id_ct
    no_medrec
    nama_pasien
    nodok
    nama_dok
    jaminan
    tgl_lahir
    umur
    kelamin
    kelas
    tgl_ct
    s_description
    s_stroke
    o_description
    o_stroke
    a_description
    a_stroke
    a_icd9
    a_icdx
    p_lab_description
    p_lab_stroke    
    p_rad_description
    p_rad_stroke
    p_fisio_description
    p_fisio_stroke
    p_tindakan_description
    p_tindakan_stroke
    p_obat_description
    p_obat_stroke
    p_obat_nama
    p_obat_alergi
  }
}
`;

export const getExamineByVisit = gql`
query getExamineByVisit($kunjungan: String, $offset:Int, $limit:Int) {
  examineJoinByVisit(kunjungan: $kunjungan, offset:$offset, limit:$limit) {
    no_periksa
    kode_unit
    poli
    tgl
    nodok {
      nodok
      specialist
    }
    nama_dok
    no_registrasi
    no_medrec {
      no_medrec
      address
    }
    nama_pasien
    diagnosa
    diagnosa_stroke
    no_antrian
    kasus
    jaminan
    kunjungan
    rujuk
    umur
    tgl_lahir
    kelamin
    status_pasien
    kaji
    flag
  }
}
`;

export const getExamineJoinByDateRange = gql`
query getExamineJoinByDateRange($startDate: String!, $endDate: String!) {
  examineJoinByDateRange(startDate:$startDate, endDate:$endDate) {
    no_periksa
    kode_unit
    poli
    tgl
    nodok {
      nodok
      specialist
    }
    nama_dok
    no_registrasi
    no_medrec {
      no_medrec
      address
    }
    nama_pasien
    diagnosa
    diagnosa_stroke
    no_antrian
    kasus
    jaminan
    kunjungan
    rujuk
    umur
    tgl_lahir
    kelamin
    status_pasien
    kaji
    flag
  }
}
`;

export const getFormsAll = gql`
query getFormsAll {
  formAll {
    no_form
    name_form
    json_form
  }
}
`;

export const getFormsById = gql`
query getFormsById($no_form: String!) {
  formById(no_form: $no_form) {
    no_form
    name_form
    json_form
  }
}
`;

export const getFormsByName = gql`
query getFormsByName($name_form: String!) {
  formByName(name_form: $name_form) {
    no_form
    name_form
    json_form
  }
}
`;

export const addFormQuery = gql`
mutation addFormQuery($no_form: String, $name_form: String, $json_form: String!) {
  addForm(no_form: $no_form, name_form: $name_form, json_form: $json_form) {
    no_form
    name_form
    json_form
}
}
`;

export const deleteFormQuery = gql`
mutation deleteFormQuery($no_form: String) {
  deleteForm(no_form: $no_form) {
    no_form
}
}
`;

export const updateFormQuery = gql`
mutation updateFormQuery($no_form: String, $name_form: String, $json_form: String) {
  updateForm(no_form: $no_form, name_form: $name_form, json_form: $json_form) {
    no_form
    name_form
    json_form
}
}
`;

export const FormsExchangeQuery = gql`
subscription FormsExchangeQuery {
  formDataExchange {
    no_form,
    name_form
  }
}
`;
