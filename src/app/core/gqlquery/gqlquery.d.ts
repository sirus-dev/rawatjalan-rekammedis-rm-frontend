export interface QueryPatient {
    patientAll: any;
}
export interface QueryPatientMedRec {
    allMedrec: any;
}
export interface QueryPatientMedRecById {
    medRec: any;
}
export interface QueryPatientById {
    patientByNorm: any;
}
export interface QueryPatientByName {
    patientByName: any;
}
export interface QueryReviewById {
    reviewByMedrec: any;
}
export interface QueryDoctorById {
    doctorByNodok: any;
}
export interface QueryCTById {
    ctByMedrec: any;
}
export interface QueryCTByIdDate {
    ctByMedrecTgl: any;
}
export interface QueryCTById1 {
    ctByMedrec1: any;
}
export interface QueryCTByExamine {
    ctByExamine: any;
}
export interface QueryListDoctors {
    doctors: any;
}
export interface QueryPatientExamine {
    examineByNodok: any;
}
export interface QueryICD9 {
    icd9Pagination: any;
}
export interface QueryExamineAll {
    examineAll: any;
}
export interface QueryExamineAllByNodok {
    examineAllByNodok: any;
}
export interface QueryExamineJoinById {
    examineJoinById: any;
}
export interface QueryExamineJoinByVisit {
    examineJoinByVisit: any;
}
export interface QueryExamineJoinByDateRange {
    examineJoinByDateRange: any;
}
export interface QueryCTByDate {
    ctByDate: any;
}
export interface QueryForms {
    formAll: any;
}
export interface QueryFormsById {
    formById: any;
}
export interface QueryFormsByName {
    formByName: any;
}
export interface FormsExchangeQuery {
    formDataExchange: any;
}
export declare const getPatientMedRecQuery: any;
export declare const getPatientMedRecByIdQuery: any;
export declare const getPatientQuery: any;
export declare const getPatientByNameQuery: any;
export declare const getReviewByIdQuery: any;
export declare const getPatientByIdQuery: any;
export declare const getDoctorByIdQuery: any;
export declare const getIntegratedNoteExamineQuery: any;
export declare const getIntegratedNoteQuery: any;
export declare const getIntegratedNoteByIdDateQuery: any;
export declare const getIntegratedNote1Query: any;
export declare const getListDoctorsQuery: any;
export declare const getPatientExamineQuery: any;
export declare const getIcd9: any;
export declare const updateCT: any;
export declare const getReportBasicById: any;
export declare const getReportBasic: any;
export declare const getReportBasicNodok: any;
export declare const getCTByDate: any;
export declare const getExamineByVisit: any;
export declare const getExamineJoinByDateRange: any;
export declare const getFormsAll: any;
export declare const getFormsById: any;
export declare const getFormsByName: any;
export declare const addFormQuery: any;
export declare const deleteFormQuery: any;
export declare const updateFormQuery: any;
export declare const FormsExchangeQuery: any;
