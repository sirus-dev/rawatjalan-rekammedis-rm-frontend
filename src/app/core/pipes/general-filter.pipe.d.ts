import { PipeTransform } from '@angular/core';
export declare class GeneralFilterPipe implements PipeTransform {
    transform(value: any, input: string): any;
}
