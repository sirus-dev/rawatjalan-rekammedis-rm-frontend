import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterByPatient'
})
@Injectable()
export class PatientFilterPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            if (input.match(/^\d/)) {
                return value.filter(function (el: any) {
                    return el.no_medrec.toLowerCase().indexOf(input.toLowerCase()) > -1;
                });
            } else {
                return value.filter(function (el: any) {
                    return el.nama_pasien.toLowerCase().indexOf(input.toLowerCase()) > -1;
                });
            }
        }
        return value;
    }
}
