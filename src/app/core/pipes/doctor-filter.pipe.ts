import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterByDoctor'
})
@Injectable()
export class DoctorFilterPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            if (input.match(/^\d/)) {
                    return value.filter(function (el: any) {
                        return el.nodok[0].nodok.toLowerCase().indexOf(input.toLowerCase()) > -1;
                    });
            } else {
                console.log(input);
                    return value.filter(function (el: any) {
                        return el.nama_dok.toLowerCase().indexOf(input.toLowerCase()) > -1;
                    });
            }
        }
        return value;
    }
}
