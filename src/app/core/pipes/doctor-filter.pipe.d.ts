import { PipeTransform } from '@angular/core';
export declare class DoctorFilterPipe implements PipeTransform {
    transform(value: any, input: string): any;
}
