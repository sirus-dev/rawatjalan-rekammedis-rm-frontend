import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'groupBy'})
export class GroupByPipe implements PipeTransform {
  transform(value, args: string[]): any {
    const groups = {};
    value.forEach(function(o) {
      const group = o.nama_pasien;
      groups[group] = groups[group] ?
         groups[group] : { nama_pasien: group, no_medrec: [] };
      groups[group].resources.push(o);
    });

    return Object.keys(groups).map(function (key) {return groups[key]; });
  }
}
