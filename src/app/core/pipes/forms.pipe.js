var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable, Pipe } from '@angular/core';
var FormsFilterPipe = /** @class */ (function () {
    function FormsFilterPipe() {
    }
    FormsFilterPipe.prototype.transform = function (value, input) {
        if (input) {
            // if (input.startsWith('FORM')) {
            //         return value.filter(function (el: any) {
            //             return el.no_form.toLowerCase().indexOf(input.toLowerCase()) > -1;
            //         });
            // } else {
            console.log(input);
            return value.filter(function (el) {
                return el.name_form.toLowerCase().indexOf(input.toLowerCase()) > -1;
            });
            // }
        }
        return value;
    };
    FormsFilterPipe = __decorate([
        Pipe({
            name: 'filterForms'
        }),
        Injectable()
    ], FormsFilterPipe);
    return FormsFilterPipe;
}());
export { FormsFilterPipe };
//# sourceMappingURL=forms.pipe.js.map