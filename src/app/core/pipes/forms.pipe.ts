import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterForms'
})
@Injectable()
export class FormsFilterPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            // if (input.startsWith('FORM')) {
            //         return value.filter(function (el: any) {
            //             return el.no_form.toLowerCase().indexOf(input.toLowerCase()) > -1;
            //         });
            // } else {
                console.log(input);
                    return value.filter(function (el: any) {
                        return el.name_form.toLowerCase().indexOf(input.toLowerCase()) > -1;
                    });
            // }
        }
        return value;
    }
}
