import { PipeTransform } from '@angular/core';
export declare class FormsFilterPipe implements PipeTransform {
    transform(value: any, input: string): any;
}
