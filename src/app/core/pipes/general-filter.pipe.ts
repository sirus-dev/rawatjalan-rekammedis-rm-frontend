import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterBy'
})
@Injectable()
export class GeneralFilterPipe implements PipeTransform {
    transform(value: any, input: string) {
        if (input) {
            if (input.match(/^\d/)) {
                return value.filter(function (el: any) {
                    return el.pasien[0].no_medrec.toLowerCase().indexOf(input.toLowerCase()) > -1;
                });
            } else {
                console.log(input);
                    return value.filter(function (el: any) {
                        return el.pasien[0].name.toLowerCase().indexOf(input.toLowerCase()) > -1;
                    });
            }
        }
        return value;
    }
}
