import { PipeTransform } from '@angular/core';
export declare class PatientFilterPipe implements PipeTransform {
    transform(value: any, input: string): any;
}
