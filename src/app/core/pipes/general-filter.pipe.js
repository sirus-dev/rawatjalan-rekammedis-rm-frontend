var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable, Pipe } from '@angular/core';
var GeneralFilterPipe = /** @class */ (function () {
    function GeneralFilterPipe() {
    }
    GeneralFilterPipe.prototype.transform = function (value, input) {
        if (input) {
            if (input.match(/^\d/)) {
                return value.filter(function (el) {
                    return el.pasien[0].no_medrec.toLowerCase().indexOf(input.toLowerCase()) > -1;
                });
            }
            else {
                console.log(input);
                return value.filter(function (el) {
                    return el.pasien[0].name.toLowerCase().indexOf(input.toLowerCase()) > -1;
                });
            }
        }
        return value;
    };
    GeneralFilterPipe = __decorate([
        Pipe({
            name: 'filterBy'
        }),
        Injectable()
    ], GeneralFilterPipe);
    return GeneralFilterPipe;
}());
export { GeneralFilterPipe };
//# sourceMappingURL=general-filter.pipe.js.map