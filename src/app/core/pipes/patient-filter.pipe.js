var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable, Pipe } from '@angular/core';
var PatientFilterPipe = /** @class */ (function () {
    function PatientFilterPipe() {
    }
    PatientFilterPipe.prototype.transform = function (value, input) {
        if (input) {
            if (input.match(/^\d/)) {
                return value.filter(function (el) {
                    return el.no_medrec.toLowerCase().indexOf(input.toLowerCase()) > -1;
                });
            }
            else {
                return value.filter(function (el) {
                    return el.nama_pasien.toLowerCase().indexOf(input.toLowerCase()) > -1;
                });
            }
        }
        return value;
    };
    PatientFilterPipe = __decorate([
        Pipe({
            name: 'filterByPatient'
        }),
        Injectable()
    ], PatientFilterPipe);
    return PatientFilterPipe;
}());
export { PatientFilterPipe };
//# sourceMappingURL=patient-filter.pipe.js.map