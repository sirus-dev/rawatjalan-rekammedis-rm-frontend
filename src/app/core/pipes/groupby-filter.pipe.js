var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Pipe } from '@angular/core';
var GroupByPipe = /** @class */ (function () {
    function GroupByPipe() {
    }
    GroupByPipe.prototype.transform = function (value, args) {
        var groups = {};
        value.forEach(function (o) {
            var group = o.nama_pasien;
            groups[group] = groups[group] ?
                groups[group] : { nama_pasien: group, no_medrec: [] };
            groups[group].resources.push(o);
        });
        return Object.keys(groups).map(function (key) { return groups[key]; });
    };
    GroupByPipe = __decorate([
        Pipe({ name: 'groupBy' })
    ], GroupByPipe);
    return GroupByPipe;
}());
export { GroupByPipe };
//# sourceMappingURL=groupby-filter.pipe.js.map