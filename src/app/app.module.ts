// CORE MODULE
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ClarityModule } from 'clarity-angular';
import { MyDatePickerModule } from 'mydatepicker';
import { ApolloModule } from 'apollo-angular';
import { ChartsModule } from 'ng2-charts';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';

// COMPONENT
import { AppComponent } from './app.component';
import { ForgetPasswordComponent } from './pages/auth/forgetpassword/forgetpassword.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { ReportComponent } from './pages/report/report.component';
import { RMPatientComponent } from './pages/rmpatient/rmpatient.component';
import { RequestEditComponent } from './pages/requestedit/requestedit.component';
import { HomeComponent } from './pages/home/home.component';
import { CodingComponent } from './pages/coding/coding.component';

// ROUTING
import { ROUTING } from "./app.routing";

// SERVICE
import { AuthService } from './core/providers/auth.service';
import { AuthGuardService } from './core/authguard/authguard.service';
import { RmserviceService } from './core/providers/rmservice.service';
import { ApolloClient, createNetworkInterface } from 'apollo-client';

// PIPE SERVICE
import { GeneralFilterPipe } from './core/pipes/general-filter.pipe';
import { GroupByPipe } from './core/pipes/groupby-filter.pipe';
import { PatientFilterPipe } from './core/pipes/patient-filter.pipe';
import { DoctorFilterPipe } from './core/pipes/doctor-filter.pipe';

// FORM
import { FieldInputModelComponent } from '../models/fieldInput';
import { FieldDatetimeModelComponent } from '../models/fieldDatetime';
import { FieldRadioModelComponent } from '../models/fieldRadio';
import { FieldMultiinputModelComponent } from '../models/fieldMultiinput';
import { FieldTextareaModelComponent } from '../models/fieldTextarea';
import { FieldCheckboxModelComponent } from '../models/fieldCheckbox';
import { FieldTimepickerModelComponent } from '../models/fieldTimepicker';
import { FieldMulticheckboxModelComponent } from '../models/fieldMulticheckbox';
import { FieldRangeModelComponent } from '../models/fieldRange';
import { FieldLabelBoldModelComponent } from './../models/fieldLabelBold';
import { FieldHeaderTextModelComponent } from './../models/fieldHeaderText';
import { FieldLabelModelComponent } from './../models/fieldLabel';
import { FormsFilterPipe } from "./core/pipes/forms.pipe";
import { FormsService } from "./core/providers/forms.service";
import { EditFormComponent } from "./pages/edit-form/edit-form.component";
import { FormBuilderComponent } from "./pages/form-builder/form-builder.component";
import { AdminComponent } from "./pages/admin/admin.component";
import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws';


// Create WebSocket client
const wsClient = new SubscriptionClient('ws://104.248.151.31:14113/subscriptions', {
  reconnect: true
});

// CONFIG APOLLO NETWORK
const networkInterface = createNetworkInterface({ uri: 'api/graphql', opts: {
  mode: 'cors'} });
networkInterface.useAfter([{
  applyAfterware({ response }, next) {
    if (response.status === 401) {
      console.log("Network Error");
    }
    next();
  }
}]);

// Extend the network interface with the WebSocket
const networkInterfaceWithSubscriptions = addGraphQLSubscriptions(
  networkInterface,
  wsClient,
);

// Create Apollo Client
const client = new ApolloClient({
  networkInterface : networkInterfaceWithSubscriptions,
});

export function provideClient(): ApolloClient {
  return client;
}


@NgModule({
    declarations: [
        AdminComponent,
        AppComponent,
        LoginComponent,
        ForgetPasswordComponent,
        HomeComponent,
        RMPatientComponent,
        CodingComponent,
        ReportComponent,
        RequestEditComponent,
        EditFormComponent,
        FormBuilderComponent,
        GeneralFilterPipe,
        DoctorFilterPipe,
        PatientFilterPipe,
        GroupByPipe,
        FormsFilterPipe,
        FieldInputModelComponent,
        FieldDatetimeModelComponent,
        FieldRadioModelComponent,
        FieldMultiinputModelComponent,
        FieldMulticheckboxModelComponent,
        FieldTextareaModelComponent,
        FieldCheckboxModelComponent,
        FieldTimepickerModelComponent,
        FieldLabelModelComponent,
        FieldHeaderTextModelComponent,
        FieldLabelBoldModelComponent,
        FieldRangeModelComponent
    ],
    imports: [
        ReactiveFormsModule,
        FormlyModule.forRoot(),
        FormlyBootstrapModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        MyDatePickerModule,
        ClarityModule.forRoot(),
        ApolloModule.forRoot(provideClient),
        ROUTING,
        ChartsModule,
        FormlyModule.forRoot(
          {
            types: [
              { name: 'input', component: FieldInputModelComponent },
              { name: 'datetime', component: FieldDatetimeModelComponent },
              { name: 'radio', component: FieldRadioModelComponent },
              { name: 'timepicker', component: FieldTimepickerModelComponent },
              { name: 'label', component: FieldLabelModelComponent },
              { name: 'header-text', component: FieldHeaderTextModelComponent },
              { name: 'label-bold', component: FieldLabelBoldModelComponent },
              { name: 'range', component: FieldRangeModelComponent }
            ],
            wrappers: [
              { name: 'multiinput', component: FieldMultiinputModelComponent },
              { name: 'multicheckbox', component: FieldMulticheckboxModelComponent }
            ]
          }
        ),
    ],
  providers: [AuthService, AuthGuardService, RmserviceService, FormBuilder, FormsService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
