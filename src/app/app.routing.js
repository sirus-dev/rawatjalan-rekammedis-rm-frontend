import { AdminComponent } from './pages/admin/admin.component';
import { ForgetPasswordComponent } from './pages/auth/forgetpassword/forgetpassword.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { RMPatientComponent } from './pages/rmpatient/rmpatient.component';
import { RequestEditComponent } from './pages/requestedit/requestedit.component';
import { ReportComponent } from './pages/report/report.component';
import { CodingComponent } from './pages/coding/coding.component';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuardService } from './core/authguard/authguard.service';
import { RouterModule } from '@angular/router';
import { FormBuilderComponent } from './pages/form-builder/form-builder.component';
import { EditFormComponent } from './pages/edit-form/edit-form.component';
export var ROUTES = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'forget', component: ForgetPasswordComponent },
    {
        path: 'home', runGuardsAndResolvers: 'always', canActivate: [AuthGuardService], component: HomeComponent, children: [
            { path: '', canActivateChild: [AuthGuardService], redirectTo: 'rmpasien', pathMatch: 'full' },
            { path: 'rmpasien', canActivateChild: [AuthGuardService], component: RMPatientComponent },
            { path: 'coding', canActivateChild: [AuthGuardService], component: CodingComponent },
            { path: 'laporan', canActivateChild: [AuthGuardService], component: ReportComponent },
            { path: 'requestedit', canActivateChild: [AuthGuardService], component: RequestEditComponent },
            { path: 'admin', canActivateChild: [AuthGuardService], component: AdminComponent },
            { path: 'forms', canActivateChild: [AuthGuardService], component: FormBuilderComponent },
            { path: 'editform', canActivateChild: [AuthGuardService], component: EditFormComponent },
            { path: 'editform/:id', canActivateChild: [AuthGuardService], component: EditFormComponent }
        ]
    },
];
export var ROUTING = RouterModule.forRoot(ROUTES);
//# sourceMappingURL=app.routing.js.map