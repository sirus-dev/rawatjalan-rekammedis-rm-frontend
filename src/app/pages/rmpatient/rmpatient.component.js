var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { RmserviceService } from './../../core/providers/rmservice.service';
// import { AbstractComponent, InkGrabber } from '@sirus/stylus';
// import { InkGrabber } from '@sirus/stylus';
import { Component } from "@angular/core";
import * as moment from 'moment';
var RMPatientComponent = /** @class */ (function () {
    function RMPatientComponent(rmService) {
        this.rmService = rmService;
        this.dateArray = [];
        this.statusSOAP = [];
        // private components: AbstractComponent[] = [];
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.width = 800;
        this.height = 400;
        this.timeout = 1;
        // BOOLEAN TYPE
        this.checkoutDoctor = false;
        this.notificationWarning = false;
        this.statusIntegratedNote = false;
        this.modalStatusIntegratedNote = false;
        this.modalStatusCheckoutDoctor = false;
        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        this.statusDetailSoap = false;
    }
    // tslint:disable-next-line:use-life-cycle-interface
    RMPatientComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    RMPatientComponent.prototype.loadData = function () {
        var _this = this;
        this.rmService.getDataPatientMedRec().subscribe(function (_a) {
            var data = _a.data;
            var datapatient = data.allMedrec.slice();
            _this.dataPatient = datapatient;
            console.log(datapatient);
        });
    };
    RMPatientComponent.prototype.listDoctor = function (no_medrec) {
        var _this = this;
        this.dataDoctor = [];
        this.rmService.getDataIntegratedNote1(no_medrec).subscribe(function (_a) {
            var data = _a.data;
            var parsData = _this.parseDataJSON(data);
            var listdoctor = parsData.ctByMedrec1;
            if (listdoctor.length === 0) {
                _this.checkoutDoctor = false;
                _this.dataDoctor = [{ nodok: '', tgl_ct: '' }];
                _this.detailDataDoctor = [{ nama_dok: '', specialis: '' }];
            }
            else {
                _this.checkoutDoctor = true;
                _this.dataDoctor = [listdoctor[0]];
                _this.dateofcheckout = _this.mapToMoment(listdoctor[0].tgl_ct); // Unused
                _this.rmService.getDoctorById(listdoctor[0].nodok).subscribe(function (_a) {
                    var data = _a.data;
                    var parDataDoctor = _this.parseDataJSON(data);
                    var dataDoctor = parDataDoctor.doctorByNodok;
                    if (dataDoctor == null) {
                        _this.detailDataDoctor = [{ nama_dok: '', specialis: '' }];
                    }
                    else {
                        _this.checkoutDoctor = true;
                        _this.detailDataDoctor = [dataDoctor];
                    }
                });
            }
        });
        this.modalStatusCheckoutDoctor = true;
    };
    RMPatientComponent.prototype.getListintegratedNote = function (no_medrec) {
        var _this = this;
        this.medrecPatient = no_medrec;
        this.dataDateCT = [];
        this.statusDetailSoap = false;
        this.notificationWarning = false;
        this.rmService.getPatientById(no_medrec).subscribe(function (_a) {
            var data = _a.data;
            var parseData = _this.parseDataJSON(data);
            var pasiendetail = parseData.patientByNorm;
            _this.detailDataPatient = [pasiendetail];
            _this.mappingArray(pasiendetail.tahun, pasiendetail.agama);
            _this.rmService.getDataIntegratedNote(pasiendetail.no_medrec).subscribe(function (_a) {
                var data = _a.data;
                if (data) {
                    var parseData_1 = _this.parseDataJSON(data);
                    var datactpasien = parseData_1.ctByMedrec;
                    _this.dataDateCT = datactpasien;
                    if (_this.dataDateCT.length > 0) {
                        _this.statusIntegratedNote = true;
                    }
                    else {
                        _this.statusIntegratedNote = false;
                    }
                }
            });
            _this.modalStatusIntegratedNote = true;
        });
    };
    RMPatientComponent.prototype.getIntegratedNote = function (no_medrec, tgl_ct) {
        var _this = this;
        this.statusDetailSoap = true;
        this.medrecPatient = no_medrec;
        this.dataCT = [];
        this.rmService.getDataIntegratedNoteByDate(no_medrec, tgl_ct).subscribe(function (_a) {
            var data = _a.data;
            if (data) {
                var parseData = _this.parseDataJSON(data);
                var datactpasien = parseData.ctByMedrecTgl;
                _this.dataCT = datactpasien;
                if (_this.dataCT.length > 0) {
                    _this.statusIntegratedNote = true;
                }
                else {
                    _this.statusIntegratedNote = false;
                }
                _this.dateArray = [];
                for (var i = 0; i < datactpasien.length; i++) {
                    _this.notificationWarning = false;
                    _this.styluss[i] = false;
                    _this.styluso[i] = false;
                    _this.stylusa[i] = false;
                    _this.styluspo[i] = false;
                    if (datactpasien[i].s_description === "" || datactpasien[i].s_stroke === "") {
                        _this.notificationWarning = true;
                        _this.doctorName = datactpasien[i].nama_dok;
                        if (_this.statusSOAP.values().next().value !== "Subject") {
                            _this.statusSOAP.push("Subject");
                        }
                    }
                    if (datactpasien[i].o_description === "" || datactpasien[i].o_stroke === "") {
                        _this.notificationWarning = true;
                        _this.doctorName = datactpasien[i].nama_dok;
                        if (_this.statusSOAP.values().next().value !== "Object") {
                            _this.statusSOAP.push("Object");
                        }
                    }
                    if (datactpasien[i].a_description === "" || datactpasien[i].a_stroke === "") {
                        _this.notificationWarning = true;
                        _this.doctorName = datactpasien[i].nama_dok;
                        if (_this.statusSOAP.values().next().value !== "Assessment") {
                            _this.statusSOAP.push("Assessment");
                        }
                    }
                    if (datactpasien[i].p_obat_description === "" || datactpasien[i].p_obat_stroke === "") {
                        _this.notificationWarning = true;
                        _this.doctorName = datactpasien[i].nama_dok;
                        if (_this.statusSOAP.values().next().value !== "Planning Obat") {
                            _this.statusSOAP.push("Planning Obat");
                        }
                    }
                    _this.dateArray.push(datactpasien[i]);
                }
            }
        });
        this.modalStatusIntegratedNote = true;
    };
    RMPatientComponent.prototype.filteringByDate = function () {
        var _this = this;
        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        var year = this.filterDate.date.year;
        var month = this.filterDate.date.month;
        var day = this.filterDate.date.day;
        if (month < 10) {
            month = '0' + month;
        }
        if (day < 10) {
            day = '0' + day;
        }
        var date = year + "-" + month + "-" + day;
        this.rmService.getIntegratedNoteByDateQuery(this.medrecPatient, date).subscribe(function (_a) {
            var data = _a.data;
            var parseData = _this.parseDataJSON(data);
            var datactpasien = parseData.ctByDate;
            if (datactpasien.length > 0) {
                _this.dataCT = datactpasien;
                _this.statusIntegratedNote = true;
            }
            else {
                _this.statusIntegratedNote = false;
            }
        });
    };
    RMPatientComponent.prototype.resetData = function () {
        var _this = this;
        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        this.statusIntegratedNote = true;
        this.dataCT = [];
        this.rmService.getPatientById(this.medrecPatient).subscribe(function (_a) {
            var data = _a.data;
            var parseData = _this.parseDataJSON(data);
            var pasiendetail = parseData.patientByNorm;
            _this.detailDataPatient = [pasiendetail];
            _this.mappingArray(pasiendetail.tahun, pasiendetail.agama);
            _this.rmService.getDataIntegratedNote(pasiendetail.no_medrec).subscribe(function (_a) {
                var data = _a.data;
                if (data) {
                    var parseData_2 = _this.parseDataJSON(data);
                    var datactpasien = parseData_2.ctByMedrec;
                    _this.dataCT = datactpasien;
                }
            });
            _this.modalStatusIntegratedNote = true;
        });
    };
    RMPatientComponent.prototype.drawcanvas = function (index, strokedata, status) {
        // this.components = [];
        if (status === "s") {
            this.styluss[index] = !this.styluss[index];
        }
        else if (status === "o") {
            this.styluso[index] = !this.styluso[index];
        }
        else if (status === "a") {
            this.stylusa[index] = !this.stylusa[index];
        }
        else if (status === "po") {
            this.styluspo[index] = !this.styluspo[index];
        }
        var x = [];
        var y = [];
        var t = [];
        if (strokedata !== null) {
            var datastroke = JSON.parse(strokedata);
            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);
            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);
            this.element = document.getElementById(index);
            var imageRenderingCanvas = this.element;
            var ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            // this.inkGrabber = new InkGrabber(ctx);
            // this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (var i = 1; i < x.length - 1; i++) {
                if (x[i] == null) {
                    // this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
                    // let stroke = this.inkGrabber.getStroke();
                    // this.components.push(stroke);
                    // this.inkGrabber = new InkGrabber(ctx);
                    // this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                }
                else {
                    // this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            // this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
            // let stroke = this.inkGrabber.getStroke();
            // this.components.push(stroke);
            // this.inkGrabberFinal = new InkGrabber(ctx);
            // for (let i = 0; i < this.components.length; i++) {
            //     this.inkGrabberFinal.drawComponent(this.components[i]);
            // }
            var image = new Image();
            image.id = "pic" + index;
            var content = this.element;
            image.src = content.toDataURL();
            this.dataCT[index]["image" + status] = image.src;
        }
        else {
            this.dataCT[index]["image" + status] = "images/no_image.png";
        }
    };
    // REUSABLE FUNCTION
    RMPatientComponent.prototype.generateArray = function (obj) {
        if (obj) {
            return JSON.parse(obj);
        }
        else {
            return null;
        }
    };
    RMPatientComponent.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
            // return moment(date, "YYYY-MM-DD").format('YYYY');
        }
        return "-";
    };
    RMPatientComponent.prototype.filterArray = function (obj) {
        if (obj.length > 0) {
            return [obj[0]];
        }
        else {
            return [{ nama_dok: '' }];
        }
    };
    RMPatientComponent.prototype.parseDataJSON = function (data) {
        var strData = JSON.stringify(data);
        var parsData = JSON.parse(strData);
        return parsData;
    };
    RMPatientComponent.prototype.mappingArray = function (year, religion) {
        // GET AGE OF PATIENT FROM YEAR BIRTH
        var year_birth = year;
        var date = new Date();
        var year_now = date.getFullYear();
        var year_now_str = year_now.toString();
        var age = Number(year_now_str) - Number(year_birth);
        this.agePatient = age;
        // MAPPING RELIGION FROM ID
        if (religion === "1") {
            this.religionPatient = "Islam";
        }
        else if (religion === "2") {
            this.religionPatient = "Kristen";
        }
        else if (religion === "4") {
            this.religionPatient = "Buddha";
        }
        else if (religion === "0") {
            this.religionPatient = "Hindu";
        }
        else if (religion === "3") {
            this.religionPatient = "katholik";
        }
    };
    RMPatientComponent = __decorate([
        Component({
            styleUrls: ['./rmpatient.component.scss'],
            templateUrl: './rmpatient.component.html',
        }),
        __metadata("design:paramtypes", [RmserviceService])
    ], RMPatientComponent);
    return RMPatientComponent;
}());
export { RMPatientComponent };
//# sourceMappingURL=rmpatient.component.js.map