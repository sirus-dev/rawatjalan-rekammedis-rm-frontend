import { RmserviceService } from './../../core/providers/rmservice.service';
// import { AbstractComponent, InkGrabber } from '@sirus/stylus';
// import { InkGrabber } from '@sirus/stylus';
import { Component } from "@angular/core";
import * as moment from 'moment';

@Component({
    styleUrls: ['./rmpatient.component.scss'],
    templateUrl: './rmpatient.component.html',
})
export class RMPatientComponent {

    filterDate: any;
    searchPatient: any;
    agePatient: number;
    religionPatient: any;
    medrecPatient: any;
    doctorName: string;
    dateofcheckout: any;
    dateofbirth: any;

    // ARRAY TYPE
    dataPatient: Array<any>;
    dataDoctor: Array<any>;
    detailDataDoctor: Array<any>;
    detailDataPatient: Array<any>;
    dataCT: Array<any>;
    dataDateCT: Array<any>;
    dateArray: Array<any> = [];
    statusSOAP: Array<any> = [];

    // STYLUS CONFIGURATION
    // private inkGrabber: InkGrabber;
    // private inkGrabberFinal: InkGrabber;
    private element: HTMLElement;
    // private components: AbstractComponent[] = [];
    arrayX: number[] = [];
    arrayY: number[] = [];
    arrayT: number[] = [];
    width: number = 800;
    height: number = 400;
    timeout: number = 1;
    imgSource: any;

    // BOOLEAN TYPE
    checkoutDoctor: boolean = false;
    notificationWarning: boolean = false;
    statusIntegratedNote = false;
    modalStatusIntegratedNote: boolean = false;
    modalStatusCheckoutDoctor: boolean = false;
    styluss: boolean[] = [false];
    styluso: boolean[] = [false];
    stylusa: boolean[] = [false];
    styluspo: boolean[] = [false];

    statusDetailSoap = false;

    constructor(private rmService: RmserviceService) { }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnInit(): void {
        this.loadData();
    }

    loadData() {
        this.rmService.getDataPatientMedRec().subscribe(({ data }) => {
            let datapatient = [...data.allMedrec];
            this.dataPatient = datapatient;
            console.log(datapatient);
        });
    }

    listDoctor(no_medrec) {
        this.dataDoctor = [];
        this.rmService.getDataIntegratedNote1(no_medrec).subscribe(({ data }) => {
            let parsData = this.parseDataJSON(data);
            let listdoctor = parsData.ctByMedrec1;
            if (listdoctor.length === 0) {
                this.checkoutDoctor = false;
                this.dataDoctor = [{ nodok: '', tgl_ct: '' }];
                this.detailDataDoctor = [{ nama_dok: '', specialis: '' }];
            } else {
                this.checkoutDoctor = true;
                this.dataDoctor = [listdoctor[0]];
                this.dateofcheckout = this.mapToMoment(listdoctor[0].tgl_ct); // Unused

                this.rmService.getDoctorById(listdoctor[0].nodok).subscribe(({ data }) => {
                    let parDataDoctor = this.parseDataJSON(data);
                    let dataDoctor = parDataDoctor.doctorByNodok;
                    if (dataDoctor == null) {
                        this.detailDataDoctor = [{ nama_dok: '', specialis: '' }];
                    } else {
                        this.checkoutDoctor = true;
                        this.detailDataDoctor = [dataDoctor];
                    }
                });
            }
        });
        this.modalStatusCheckoutDoctor = true;
    }

    getListintegratedNote(no_medrec) {

        this.medrecPatient = no_medrec;
        this.dataDateCT = [];
        this.statusDetailSoap = false;
        this.notificationWarning = false;

        this.rmService.getPatientById(no_medrec).subscribe(({ data }) => {

            let parseData = this.parseDataJSON(data);
            let pasiendetail = parseData.patientByNorm;
            this.detailDataPatient = [pasiendetail];
            this.mappingArray(pasiendetail.tahun, pasiendetail.agama);

            this.rmService.getDataIntegratedNote(pasiendetail.no_medrec).subscribe(({ data }) => {
                if (data) {
                    let parseData = this.parseDataJSON(data);
                    let datactpasien = parseData.ctByMedrec;
                    this.dataDateCT = datactpasien;
                    if (this.dataDateCT.length > 0) {
                        this.statusIntegratedNote = true;
                    } else {
                        this.statusIntegratedNote = false;
                    }
                }
            });
            this.modalStatusIntegratedNote = true;

        });

    }

    getIntegratedNote(no_medrec: string, tgl_ct: string) {
        this.statusDetailSoap = true;
        this.medrecPatient = no_medrec;
        this.dataCT = [];
            this.rmService.getDataIntegratedNoteByDate(no_medrec, tgl_ct).subscribe(({ data }) => {
                if (data) {
                    let parseData = this.parseDataJSON(data);
                    let datactpasien = parseData.ctByMedrecTgl;
                    this.dataCT = datactpasien;
                    if (this.dataCT.length > 0) {
                        this.statusIntegratedNote = true;
                    } else {
                        this.statusIntegratedNote = false;
                    }
                    this.dateArray = [];
                    for (let i = 0; i < datactpasien.length; i++) {
                        this.notificationWarning = false;
                        this.styluss[i] = false;
                        this.styluso[i] = false;
                        this.stylusa[i] = false;
                        this.styluspo[i] = false;
                        if (datactpasien[i].s_description === "" || datactpasien[i].s_stroke === "") {
                            this.notificationWarning = true;
                            this.doctorName = datactpasien[i].nama_dok;
                            if (this.statusSOAP.values().next().value !== "Subject") {
                                this.statusSOAP.push("Subject");
                            }
                        }
                        if (datactpasien[i].o_description === "" || datactpasien[i].o_stroke === "") {
                            this.notificationWarning = true;
                            this.doctorName = datactpasien[i].nama_dok;
                            if (this.statusSOAP.values().next().value !== "Object") {
                                this.statusSOAP.push("Object");
                            }
                        }
                        if (datactpasien[i].a_description === "" || datactpasien[i].a_stroke === "") {
                            this.notificationWarning = true;
                            this.doctorName = datactpasien[i].nama_dok;
                            if (this.statusSOAP.values().next().value !== "Assessment") {
                                this.statusSOAP.push("Assessment");
                            }
                        }
                        if (datactpasien[i].p_obat_description === "" || datactpasien[i].p_obat_stroke === "") {
                            this.notificationWarning = true;
                            this.doctorName = datactpasien[i].nama_dok;
                            if (this.statusSOAP.values().next().value !== "Planning Obat") {
                                this.statusSOAP.push("Planning Obat");
                            }
                        }
                        this.dateArray.push(datactpasien[i]);
                    }
                }

            });
            this.modalStatusIntegratedNote = true;

    }

    filteringByDate() {

        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        let year = this.filterDate.date.year;
        let month = this.filterDate.date.month;
        let day = this.filterDate.date.day;

        if (month < 10) {
            month = '0' + month;
        }
        if (day < 10) {
            day = '0' + day;
        }

        let date = year + "-" + month + "-" + day;

        this.rmService.getIntegratedNoteByDateQuery(this.medrecPatient, date).subscribe(({ data }) => {
            let parseData = this.parseDataJSON(data);
            let datactpasien = parseData.ctByDate;
            if (datactpasien.length > 0) {
                this.dataCT = datactpasien;
                this.statusIntegratedNote = true;
            } else {
                this.statusIntegratedNote = false;
            }
        });
    }

    resetData() {

        this.styluss = [false];
        this.styluso = [false];
        this.stylusa = [false];
        this.styluspo = [false];
        this.statusIntegratedNote = true;

        this.dataCT = [];

        this.rmService.getPatientById(this.medrecPatient).subscribe(({ data }) => {

            let parseData = this.parseDataJSON(data);
            let pasiendetail = parseData.patientByNorm;
            this.detailDataPatient = [pasiendetail];
            this.mappingArray(pasiendetail.tahun, pasiendetail.agama);

            this.rmService.getDataIntegratedNote(pasiendetail.no_medrec).subscribe(({ data }) => {
                if (data) {
                    let parseData = this.parseDataJSON(data);
                    let datactpasien = parseData.ctByMedrec;
                    this.dataCT = datactpasien;
                }

            });
            this.modalStatusIntegratedNote = true;

        });
    }

    drawcanvas(index: any, strokedata: any, status: any) {
        // this.components = [];
        if (status === "s") {
            this.styluss[index] = !this.styluss[index];
        } else if (status === "o") {
            this.styluso[index] = !this.styluso[index];
        } else if (status === "a") {
            this.stylusa[index] = !this.stylusa[index];
        } else if (status === "po") {
            this.styluspo[index] = !this.styluspo[index];
        }
        let x: number[] = [];
        let y: number[] = [];
        let t: number[] = [];

        if (strokedata !== null) {
            let datastroke = JSON.parse(strokedata);

            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);

            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);

            this.element = document.getElementById(index);
            let imageRenderingCanvas = <HTMLCanvasElement>this.element;
            let ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            // this.inkGrabber = new InkGrabber(ctx);

            // this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (let i = 1; i < x.length - 1; i++) {

                if (x[i] == null) {
                    // this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);

                    // let stroke = this.inkGrabber.getStroke();

                    // this.components.push(stroke);

                    // this.inkGrabber = new InkGrabber(ctx);

                    // this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                } else {
                    // this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            // this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);

            // let stroke = this.inkGrabber.getStroke();
            // this.components.push(stroke);

            // this.inkGrabberFinal = new InkGrabber(ctx);

            // for (let i = 0; i < this.components.length; i++) {
            //     this.inkGrabberFinal.drawComponent(this.components[i]);
            // }

            let image = new Image();
            image.id = "pic" + index;
            let content = <HTMLCanvasElement>this.element;
            image.src = content.toDataURL();
            this.dataCT[index]["image" + status] = image.src;

        } else {
            this.dataCT[index]["image" + status] = "images/no_image.png";
        }

    }

    // REUSABLE FUNCTION

    generateArray(obj) {
        if (obj) {
            return JSON.parse(obj);
        } else {
            return null;
        }
    }

    private mapToMoment(date: string): string {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
            // return moment(date, "YYYY-MM-DD").format('YYYY');
        }
        return "-";
    }

    filterArray(obj) {
        if (obj.length > 0) {
            return [obj[0]];
        } else {
            return [{ nama_dok: '' }];
        }
    }

    private parseDataJSON(data) {
        let strData = JSON.stringify(data);
        let parsData = JSON.parse(strData);
        return parsData;
    }

    private mappingArray(year, religion) {
        // GET AGE OF PATIENT FROM YEAR BIRTH
        let year_birth = year;
        let date = new Date();
        let year_now = date.getFullYear();
        let year_now_str = year_now.toString();
        let age = Number(year_now_str) - Number(year_birth);
        this.agePatient = age;

        // MAPPING RELIGION FROM ID
        if (religion === "1") {
            this.religionPatient = "Islam";
        } else if (religion === "2") {
            this.religionPatient = "Kristen";
        } else if (religion === "4") {
            this.religionPatient = "Buddha";
        } else if (religion === "0") {
            this.religionPatient = "Hindu";
        } else if (religion === "3") {
            this.religionPatient = "katholik";
        }
    }
    // tslint:disable-next-line:eofline
}