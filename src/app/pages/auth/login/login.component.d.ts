import { AuthService } from './../../../core/providers/auth.service';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core';
export declare class LoginComponent implements OnInit {
    private router;
    private auth;
    registerCredentials: {
        email: string;
        password: string;
    };
    openAlert: boolean;
    messageAlert: string;
    role: any;
    constructor(router: Router, auth: AuthService);
    ngOnInit(): void;
    login(): void;
    isLogin(data: any): Promise<void>;
    checkRole(): void | Promise<boolean>;
    goToForgetPass(): void;
    showError(error: any): void;
    logError(data: any): Promise<void>;
}
