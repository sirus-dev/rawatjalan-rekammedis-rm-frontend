import { AuthService } from './../../../core/providers/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loginpage',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  registerCredentials = { email: '', password: '' };
  openAlert: boolean = false;
  messageAlert: string;
  role: any;

  constructor(private router: Router, private auth: AuthService) { }

  ngOnInit() {

    let session = localStorage.getItem('session');
    let intSession = parseInt(session, 2);
    let date = new Date().getTime();
    if ( date > intSession) {
          localStorage.removeItem('token');
    }

    let token = localStorage.getItem('token');

    if (token == null ) {
      localStorage.removeItem('role');
    } else {
      this.router.navigate(['/home', 'rmpasien']);
    }
  }

  login() {
    this.auth.login(this.registerCredentials)
      .subscribe(
      data => this.isLogin(data),
      error => this.logError(error)
      );

  }

  async isLogin(data) {

    if (data.success) {
      await this.auth.saveToken(data, this.registerCredentials);
      setTimeout(() => this.checkRole(), 500);
    } else {
      return this.showError("Silakan cek kembali email dan password anda");
    }
  }

  checkRole() {
    this.role = localStorage.getItem('role');
    console.log("role:" + this.role);
    if (this.role) {
      return this.router.navigate(['/home', 'rmpasien']);
    } else {
      return this.showError("Anda bukan Staff Rekam Medis");
    }
  }

  goToForgetPass() {
    this.router.navigate(['/forget']);
  }

  showError(error) {
    this.messageAlert = error;
    this.openAlert = true;
  }

  async logError(data) {
    return this.showError("Silakan cek kembali email dan password anda");
  }

}
