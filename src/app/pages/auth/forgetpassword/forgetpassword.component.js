var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AuthService } from '../../../core/providers/auth.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
var ForgetPasswordComponent = /** @class */ (function () {
    function ForgetPasswordComponent(router, auth) {
        this.router = router;
        this.auth = auth;
        this.forgetCredentials = { email: '' };
        this.openAlert = false;
        this.succesSendMail = false;
    }
    ForgetPasswordComponent.prototype.forgetPassword = function () {
        var _this = this;
        this.auth.forgetPassword(this.forgetCredentials)
            .subscribe(function (data) { return _this.isSendEmail(data); }, function (error) { return console.log(error); });
    };
    ForgetPasswordComponent.prototype.isSendEmail = function (data) {
        if (data.success) {
            this.succesSendMail = true;
            return this.alertMessage('Berhasil Kirim Email', 'Mohon untuk cek email anda untuk proses selanjutnya');
        }
        else {
            this.succesSendMail = false;
            return this.alertMessage('Gagal Kirim Email', 'Email yang anda masukkan tidak terdaftar');
        }
    };
    ForgetPasswordComponent.prototype.alertMessage = function (title, message) {
        this.titleAlert = title;
        this.messageAlert = message;
        this.openAlert = true;
    };
    ForgetPasswordComponent.prototype.redirectPage = function () {
        this.router.navigate(['/login']);
    };
    ForgetPasswordComponent = __decorate([
        Component({
            selector: 'app-forgetpassword',
            templateUrl: './forgetpassword.component.html',
            styleUrls: ['./forgetpassword.component.scss']
        }),
        __metadata("design:paramtypes", [Router, AuthService])
    ], ForgetPasswordComponent);
    return ForgetPasswordComponent;
}());
export { ForgetPasswordComponent };
//# sourceMappingURL=forgetpassword.component.js.map