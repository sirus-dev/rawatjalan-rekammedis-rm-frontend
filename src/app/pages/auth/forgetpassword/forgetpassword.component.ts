import { AuthService } from '../../../core/providers/auth.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.scss']
})
export class ForgetPasswordComponent {
  forgetCredentials     = { email: '' };
  openAlert: boolean    = false;
  messageAlert: string;
  titleAlert: string;
  succesSendMail        = false;

  constructor(private router: Router, private auth: AuthService) { }

  forgetPassword() {
    this.auth.forgetPassword(this.forgetCredentials)
    .subscribe(
      data  => this.isSendEmail(data),
      error => console.log(error)
    );
  }

  isSendEmail(data) {
    if (data.success) {
      this.succesSendMail = true;
      return this.alertMessage('Berhasil Kirim Email', 'Mohon untuk cek email anda untuk proses selanjutnya');
    } else {
      this.succesSendMail = false;
      return this.alertMessage('Gagal Kirim Email', 'Email yang anda masukkan tidak terdaftar');
    }
  }

  alertMessage(title, message) {
    this.titleAlert   = title;
    this.messageAlert = message;
    this.openAlert    = true;
  }

  redirectPage() {
    this.router.navigate(['/login']);
  }

}
