import { AuthService } from '../../../core/providers/auth.service';
import { Router } from '@angular/router';
export declare class ForgetPasswordComponent {
    private router;
    private auth;
    forgetCredentials: {
        email: string;
    };
    openAlert: boolean;
    messageAlert: string;
    titleAlert: string;
    succesSendMail: boolean;
    constructor(router: Router, auth: AuthService);
    forgetPassword(): void;
    isSendEmail(data: any): void;
    alertMessage(title: any, message: any): void;
    redirectPage(): void;
}
