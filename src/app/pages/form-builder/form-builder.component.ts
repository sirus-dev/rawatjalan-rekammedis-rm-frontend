import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormsService } from '../../core/providers/forms.service';

interface DataForm {
  key: string;
  type: string;
  templateOptions: Object;
}

interface GroupDataForm {
  key: string;
  type: string;
  templateOptions: Object;
  wrappers: Array<any>;
  fieldGroup: Array<any>;
}

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss']
})
export class FormBuilderComponent implements OnInit {

  formName = 'Untitled Form';
  labelName = 'Untitled Label';
  elementName: any;

  idForm: string;
  maxLength: number;
  rows: number;
  placeholder: string;
  formatText = "text";
  required = "false";
  readonly = "false";

  minRange: number;
  maxRange: number;
  stepRange: number;

  labelItem: string;
  valueItem: string;
  idItem: string;
  placeholderItem: string;

  formArray: Array<any> = [];
  itemRadio: Array<any> = [];
  itemCheck: Array<DataForm> = [];
  itemInput: Array<DataForm> = [];
  itemSelect: Array<any> = [];
  itemRadioYesNo: Array<any> = [];

  tempitemCheck: Array<DataForm> = [];
  tempitemInput: Array<DataForm> = [];

  formGroup: FormGroup;

  btnsave = false;
  btnedit = false;
  showSeconds = false;

  sub: any;

  no_form: any;

  form = new FormGroup({});
  model = {};
  fields: Array<FormlyFieldConfig> = [];
  formField: Array<any> = [];
  tempField: Array<any> = [{ key: '', type: '', templateOptions: {} }];

  statusForm = false;

  updateForm: any;

  selectedElement: any;
  selectedFormField: any;
  hideExpressionKey: any = null;
  hideExpressionKeyEqual: any = null;
  hideExpressionKeyCondition: any = null;

  constructor(
    private formsservice: FormsService,
    public router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    // this.fields = [
    //   {
    //     key: 'name',
    //     type: 'radio',
    //     templateOptions: {
    //       label: 'input',
    //       options: [
    //         { 'value': 'A', 'key': 'aa' },
    //         { 'value': 'B', 'key': 'bb' }
    //       ]
    //     }
    //   },
    //   {
    //     key: 'iLikeTwix',
    //     type: 'checkbox',
    //     templateOptions: {
    //       label: 'I like twix',
    //     },
    //     hideExpression: 'model.name' + '!=' + '"aa"',
    //   },
    // ];
    // this.formField = [
    //   {
    //     key: 'name',
    //     type: 'radio',
    //     templateOptions: {
    //       label: 'input',
    //       options: [
    //         { 'value': 'A', 'key': 'aa' },
    //         { 'value': 'B', 'key': 'bb' }
    //       ]
    //     }
    //   },
    //   {
    //     key: 'iLikeTwix',
    //     type: 'checkbox',
    //     templateOptions: {
    //       label: 'I like twix',
    //     },
    //     hideExpression: 'model.name' + '!=' + '"aa"',
    //   },
    // ];
    this.itemRadioYesNo.push({ 'value': 'Tidak', 'key': 'tidak' }, { 'value': 'Ya', 'key': 'ya' });
    if (this.formField.length === 0) {
      this.statusForm = true;
    } else {
      this.statusForm = false;
    }
  }

  randomID() {
    const id = this.labelName.toLowerCase();
    const words = id.split(" ");
    let word = "";
    let len = words.length;
    if (len > 4) {
      len = 4;
    }
    for (let i = 0; i < len; i++) {
      if (i === (len - 1)) {
        word += words[i];
      } else {
        word += words[i] + " ";
      }
    }
    this.idForm = word.replace(/ /g, "_");
  }

  selectAllContent($event) {
    $event.target.select();
  }

  onSelectFormFieldChange($event) {
    this.selectedFormField = ($event.target.value).toString();
    console.log(this.selectedFormField);
  }

  onSelectElementNameChange($event) {
    if ($event.target.value === "multiinput") {
      this.itemInput = [];
      this.tempitemInput = [];
    } else if ($event.target.value === "checkboxgroup") {
      this.itemCheck = [];
      this.tempitemCheck = [];
    } else if ($event.target.value === "label") {
      this.formatText = "normal";
    } else if ($event.target.value === "input") {
      this.formatText = "text";
    }
    console.log($event.target.value);
  }

  showElement() {
    this.fields = this.formField;
  }

  saveElement() {
    if (this.maxLength == null) {
      this.maxLength = 90;
    }

    switch (this.elementName) {
      case 'label':
        if (this.formatText === "header") {
          this.formField.push(
            {
              key: this.idForm,
              type: 'header-text',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        } else if (this.formatText === "bold") {
          this.formField.push(
            {
              key: this.idForm,
              type: 'label-bold',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        } else {
          this.formField.push(
            {
              key: this.idForm,
              type: 'label',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        }
        this.saveFields("element", null);
        break;
      case 'input':
        this.formField.push(
          {
            key: this.idForm,
            type: 'input',
            templateOptions: {
              type: this.formatText,
              label: this.labelName,
              placeholder: this.placeholder,
              required: JSON.parse(this.required),
              readonly: JSON.parse(this.readonly)
            }
          }
        );
        this.saveFields("element", null);
        break;
      case 'multiinput':
        this.formField.push({
          key: this.idForm,
          wrappers: ['multiinput'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.itemInput
        });
        this.saveFields("group", this.tempitemInput);
        break;
      case 'checkbox':
        this.formField.push(
          {
            key: this.idForm,
            type: 'checkbox',
            templateOptions: {
              label: this.labelName
            }
          }
        );
        this.saveFields("element", null);
        break;
      case 'checkboxgroup':
        this.formField.push({
          key: this.idForm,
          wrappers: ['multicheckbox'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.itemCheck
        });
        this.saveFields("group", this.tempitemCheck);
        break;
      case 'radio':
        this.formField.push({
          key: this.idForm,
          type: 'radio',
          templateOptions: {
            label: this.labelName,
            options: this.itemRadio
          }
        });
        this.saveFields("element", null);
        break;
      case 'textarea':
        this.formField.push(
          {
            key: this.idForm,
            type: 'textarea',
            templateOptions: {
              label: this.labelName,
              rows: this.rows,
              placeholder: this.placeholder,
              required: JSON.parse(this.required)
            }
          }
        );
        this.saveFields("element", null);
        break;
      case 'select':
        this.formField.push({
          key: this.idForm,
          type: 'select',
          templateOptions: {
            label: this.labelName,
            options: this.itemSelect
          }
        });
        this.saveFields("element", null);
        break;
      case 'timepicker':
        this.formField.push({
          key: this.idForm,
          type: 'timepicker',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.saveFields("element", null);
        break;
      case 'datepicker':
        this.formField.push({
          key: this.idForm,
          type: 'datetime',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.saveFields("element", null);
        break;
      case 'range':
        this.formField.push(
          {
            key: this.idForm,
            type: 'range',
            templateOptions: {
              label: this.labelName,
              min: this.minRange,
              max: this.maxRange,
              step: this.stepRange
            }
          }
        );
        this.saveFields("element", null);
        break;
    }
    this.labelName = 'Untitled Label';
    this.maxLength = null;
    this.placeholder = '';
    this.idForm = '';
    this.elementName = null;
    this.formatText = '';
  }

  cancelElement() {

  }

  saveFields(type, item) {
    this.itemInput = [];
    this.itemCheck = [];
    this.itemRadio = [];
    this.itemSelect = [];
    this.fields = [];
    this.tempField = [];
    if (this.btnedit) {
      this.btnsave = false;
    } else {
      this.btnsave = true;
    }
    if (type === "group") {
      for (let i = 0; i < this.formField.length; i++) {
        this.fields.push(this.formField[i]);
        this.tempField.push({
          key: this.formField[i].key,
          wrappers: this.formField[i].wrappers,
          templateOptions: this.formField[i].templateOptions,
          fieldGroup: item
        });
      }
    } else {
      for (let i = 0; i < this.formField.length; i++) {
        this.fields.push(this.formField[i]);
        this.tempField.push({
          key: this.formField[i].key,
          type: this.formField[i].type,
          templateOptions: this.formField[i].templateOptions,
        });
      }
    }
    this.statusForm = false;
  }

  deleteItem(i, type) {
    switch (type) {
      case 'RADIO':
        this.itemRadio.splice(i, 1);
        break;
      case 'SELECT':
        this.itemSelect.splice(i, 1);
        break;
      case 'MULTIINPUT':
        this.itemInput.splice(i, 1);
        break;
      case 'CHECKBOX':
        this.itemCheck.splice(i, 1);
        break;
    }
  }

  addInput() {
    this.tempitemInput.push(
      {
        key: this.idItem,
        type: 'input',
        templateOptions: {
          type: 'text',
          label: this.labelItem,
          placeholder: this.placeholderItem,
          readonly: false
        }
      }
    );
    this.itemInput.push(
      {
        key: this.idItem,
        type: 'input',
        templateOptions: {
          type: 'text',
          label: this.labelItem,
          placeholder: this.placeholderItem,
          readonly: false
        }
      });
    this.idItem = '';
    this.labelItem = '';
    this.placeholderItem = '';
    this.maxLength = null;
  }

  addRadio() {
    this.itemRadio.push({ 'value': this.labelItem, 'key': this.valueItem });
    this.labelItem = '';
    this.valueItem = '';
  }

  addCheck() {
    this.tempitemCheck.push(
      {
        key: this.idItem,
        type: 'checkbox',
        templateOptions: {
          label: this.labelItem,
          value: this.valueItem
        }
      }
    );
    this.itemCheck.push(
      {
        key: this.idItem,
        type: 'checkbox',
        templateOptions: {
          label: this.labelItem,
          value: this.valueItem
        }
      }
    );
    this.idItem = '';
    this.labelItem = '';
    this.valueItem = '';
  }

  addSelect() {
    this.itemSelect.push({ 'label': this.labelItem, 'value': this.valueItem });
    this.labelItem = '';
    this.valueItem = '';
  }

  saveForm() {
    const json = JSON.stringify(this.tempField);
    const id = 'FORM' + Math.floor(Math.random() * 1000);
    this.formsservice.addForm(id, this.formName, json).subscribe(({}) => {
      this.router.navigate(['/home']);
    });
  }

  deleteElement() {
    if (this.formField.length === 1) {
      this.statusForm = true;
    }
    console.log(this.selectedElement);
    this.formField.splice(this.selectedElement, 1);
    this.tempField.splice(this.selectedElement, 1);
    this.fields = [];
    for (let i = 0; i < this.formField.length; i++) {
      this.fields.push(this.formField[i]);
    }
  }

  moveElement(index: any, dir: number) {
    if ((index === 0 && dir === -1) || (index === (this.formField.length - 1) && dir === +1)) {

    } else {
      this.array_move(this.formField, index, index + dir);
      this.array_move(this.tempField, index, index + dir);
      this.saveFields("group", null);
    }
  }

  array_move(arr, old_index, new_index) {
    while (old_index < 0) {
      old_index += arr.length;
    }
    while (new_index < 0) {
      new_index += arr.length;
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
  }

  goToHome() {
    this.router.navigate(['/admin']);
  }

}
