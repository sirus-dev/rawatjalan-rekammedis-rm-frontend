var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Router, ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormsService } from '../../core/providers/forms.service';
var FormBuilderComponent = /** @class */ (function () {
    function FormBuilderComponent(formsservice, router, route) {
        this.formsservice = formsservice;
        this.router = router;
        this.route = route;
        this.formName = 'Untitled Form';
        this.labelName = 'Untitled Label';
        this.formatText = "text";
        this.required = "false";
        this.readonly = "false";
        this.formArray = [];
        this.itemRadio = [];
        this.itemCheck = [];
        this.itemInput = [];
        this.itemSelect = [];
        this.itemRadioYesNo = [];
        this.tempitemCheck = [];
        this.tempitemInput = [];
        this.btnsave = false;
        this.btnedit = false;
        this.showSeconds = false;
        this.form = new FormGroup({});
        this.model = {};
        this.fields = [];
        this.formField = [];
        this.tempField = [{ key: '', type: '', templateOptions: {} }];
        this.statusForm = false;
        this.hideExpressionKey = null;
        this.hideExpressionKeyEqual = null;
        this.hideExpressionKeyCondition = null;
    }
    FormBuilderComponent.prototype.ngOnInit = function () {
        // this.fields = [
        //   {
        //     key: 'name',
        //     type: 'radio',
        //     templateOptions: {
        //       label: 'input',
        //       options: [
        //         { 'value': 'A', 'key': 'aa' },
        //         { 'value': 'B', 'key': 'bb' }
        //       ]
        //     }
        //   },
        //   {
        //     key: 'iLikeTwix',
        //     type: 'checkbox',
        //     templateOptions: {
        //       label: 'I like twix',
        //     },
        //     hideExpression: 'model.name' + '!=' + '"aa"',
        //   },
        // ];
        // this.formField = [
        //   {
        //     key: 'name',
        //     type: 'radio',
        //     templateOptions: {
        //       label: 'input',
        //       options: [
        //         { 'value': 'A', 'key': 'aa' },
        //         { 'value': 'B', 'key': 'bb' }
        //       ]
        //     }
        //   },
        //   {
        //     key: 'iLikeTwix',
        //     type: 'checkbox',
        //     templateOptions: {
        //       label: 'I like twix',
        //     },
        //     hideExpression: 'model.name' + '!=' + '"aa"',
        //   },
        // ];
        this.itemRadioYesNo.push({ 'value': 'Tidak', 'key': 'tidak' }, { 'value': 'Ya', 'key': 'ya' });
        if (this.formField.length === 0) {
            this.statusForm = true;
        }
        else {
            this.statusForm = false;
        }
    };
    FormBuilderComponent.prototype.randomID = function () {
        var id = this.labelName.toLowerCase();
        var words = id.split(" ");
        var word = "";
        var len = words.length;
        if (len > 4) {
            len = 4;
        }
        for (var i = 0; i < len; i++) {
            if (i === (len - 1)) {
                word += words[i];
            }
            else {
                word += words[i] + " ";
            }
        }
        this.idForm = word.replace(/ /g, "_");
    };
    FormBuilderComponent.prototype.selectAllContent = function ($event) {
        $event.target.select();
    };
    FormBuilderComponent.prototype.onSelectFormFieldChange = function ($event) {
        this.selectedFormField = ($event.target.value).toString();
        console.log(this.selectedFormField);
    };
    FormBuilderComponent.prototype.onSelectElementNameChange = function ($event) {
        if ($event.target.value === "multiinput") {
            this.itemInput = [];
            this.tempitemInput = [];
        }
        else if ($event.target.value === "checkboxgroup") {
            this.itemCheck = [];
            this.tempitemCheck = [];
        }
        else if ($event.target.value === "label") {
            this.formatText = "normal";
        }
        else if ($event.target.value === "input") {
            this.formatText = "text";
        }
        console.log($event.target.value);
    };
    FormBuilderComponent.prototype.showElement = function () {
        this.fields = this.formField;
    };
    FormBuilderComponent.prototype.saveElement = function () {
        if (this.maxLength == null) {
            this.maxLength = 90;
        }
        switch (this.elementName) {
            case 'label':
                if (this.formatText === "header") {
                    this.formField.push({
                        key: this.idForm,
                        type: 'header-text',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                else if (this.formatText === "bold") {
                    this.formField.push({
                        key: this.idForm,
                        type: 'label-bold',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                else {
                    this.formField.push({
                        key: this.idForm,
                        type: 'label',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                this.saveFields("element", null);
                break;
            case 'input':
                this.formField.push({
                    key: this.idForm,
                    type: 'input',
                    templateOptions: {
                        type: this.formatText,
                        label: this.labelName,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required),
                        readonly: JSON.parse(this.readonly)
                    }
                });
                this.saveFields("element", null);
                break;
            case 'multiinput':
                this.formField.push({
                    key: this.idForm,
                    wrappers: ['multiinput'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.itemInput
                });
                this.saveFields("group", this.tempitemInput);
                break;
            case 'checkbox':
                this.formField.push({
                    key: this.idForm,
                    type: 'checkbox',
                    templateOptions: {
                        label: this.labelName
                    }
                });
                this.saveFields("element", null);
                break;
            case 'checkboxgroup':
                this.formField.push({
                    key: this.idForm,
                    wrappers: ['multicheckbox'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.itemCheck
                });
                this.saveFields("group", this.tempitemCheck);
                break;
            case 'radio':
                this.formField.push({
                    key: this.idForm,
                    type: 'radio',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemRadio
                    }
                });
                this.saveFields("element", null);
                break;
            case 'textarea':
                this.formField.push({
                    key: this.idForm,
                    type: 'textarea',
                    templateOptions: {
                        label: this.labelName,
                        rows: this.rows,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required)
                    }
                });
                this.saveFields("element", null);
                break;
            case 'select':
                this.formField.push({
                    key: this.idForm,
                    type: 'select',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemSelect
                    }
                });
                this.saveFields("element", null);
                break;
            case 'timepicker':
                this.formField.push({
                    key: this.idForm,
                    type: 'timepicker',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.saveFields("element", null);
                break;
            case 'datepicker':
                this.formField.push({
                    key: this.idForm,
                    type: 'datetime',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.saveFields("element", null);
                break;
            case 'range':
                this.formField.push({
                    key: this.idForm,
                    type: 'range',
                    templateOptions: {
                        label: this.labelName,
                        min: this.minRange,
                        max: this.maxRange,
                        step: this.stepRange
                    }
                });
                this.saveFields("element", null);
                break;
        }
        this.labelName = 'Untitled Label';
        this.maxLength = null;
        this.placeholder = '';
        this.idForm = '';
        this.elementName = null;
        this.formatText = '';
    };
    FormBuilderComponent.prototype.cancelElement = function () {
    };
    FormBuilderComponent.prototype.saveFields = function (type, item) {
        this.itemInput = [];
        this.itemCheck = [];
        this.itemRadio = [];
        this.itemSelect = [];
        this.fields = [];
        this.tempField = [];
        if (this.btnedit) {
            this.btnsave = false;
        }
        else {
            this.btnsave = true;
        }
        if (type === "group") {
            for (var i = 0; i < this.formField.length; i++) {
                this.fields.push(this.formField[i]);
                this.tempField.push({
                    key: this.formField[i].key,
                    wrappers: this.formField[i].wrappers,
                    templateOptions: this.formField[i].templateOptions,
                    fieldGroup: item
                });
            }
        }
        else {
            for (var i = 0; i < this.formField.length; i++) {
                this.fields.push(this.formField[i]);
                this.tempField.push({
                    key: this.formField[i].key,
                    type: this.formField[i].type,
                    templateOptions: this.formField[i].templateOptions,
                });
            }
        }
        this.statusForm = false;
    };
    FormBuilderComponent.prototype.deleteItem = function (i, type) {
        switch (type) {
            case 'RADIO':
                this.itemRadio.splice(i, 1);
                break;
            case 'SELECT':
                this.itemSelect.splice(i, 1);
                break;
            case 'MULTIINPUT':
                this.itemInput.splice(i, 1);
                break;
            case 'CHECKBOX':
                this.itemCheck.splice(i, 1);
                break;
        }
    };
    FormBuilderComponent.prototype.addInput = function () {
        this.tempitemInput.push({
            key: this.idItem,
            type: 'input',
            templateOptions: {
                type: 'text',
                label: this.labelItem,
                placeholder: this.placeholderItem,
                readonly: false
            }
        });
        this.itemInput.push({
            key: this.idItem,
            type: 'input',
            templateOptions: {
                type: 'text',
                label: this.labelItem,
                placeholder: this.placeholderItem,
                readonly: false
            }
        });
        this.idItem = '';
        this.labelItem = '';
        this.placeholderItem = '';
        this.maxLength = null;
    };
    FormBuilderComponent.prototype.addRadio = function () {
        this.itemRadio.push({ 'value': this.labelItem, 'key': this.valueItem });
        this.labelItem = '';
        this.valueItem = '';
    };
    FormBuilderComponent.prototype.addCheck = function () {
        this.tempitemCheck.push({
            key: this.idItem,
            type: 'checkbox',
            templateOptions: {
                label: this.labelItem,
                value: this.valueItem
            }
        });
        this.itemCheck.push({
            key: this.idItem,
            type: 'checkbox',
            templateOptions: {
                label: this.labelItem,
                value: this.valueItem
            }
        });
        this.idItem = '';
        this.labelItem = '';
        this.valueItem = '';
    };
    FormBuilderComponent.prototype.addSelect = function () {
        this.itemSelect.push({ 'label': this.labelItem, 'value': this.valueItem });
        this.labelItem = '';
        this.valueItem = '';
    };
    FormBuilderComponent.prototype.saveForm = function () {
        var _this = this;
        var json = JSON.stringify(this.tempField);
        var id = 'FORM' + Math.floor(Math.random() * 1000);
        this.formsservice.addForm(id, this.formName, json).subscribe(function (_a) {
            _this.router.navigate(['/home']);
        });
    };
    FormBuilderComponent.prototype.deleteElement = function () {
        if (this.formField.length === 1) {
            this.statusForm = true;
        }
        console.log(this.selectedElement);
        this.formField.splice(this.selectedElement, 1);
        this.tempField.splice(this.selectedElement, 1);
        this.fields = [];
        for (var i = 0; i < this.formField.length; i++) {
            this.fields.push(this.formField[i]);
        }
    };
    FormBuilderComponent.prototype.moveElement = function (index, dir) {
        if ((index === 0 && dir === -1) || (index === (this.formField.length - 1) && dir === +1)) {
        }
        else {
            this.array_move(this.formField, index, index + dir);
            this.array_move(this.tempField, index, index + dir);
            this.saveFields("group", null);
        }
    };
    FormBuilderComponent.prototype.array_move = function (arr, old_index, new_index) {
        while (old_index < 0) {
            old_index += arr.length;
        }
        while (new_index < 0) {
            new_index += arr.length;
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr;
    };
    FormBuilderComponent.prototype.goToHome = function () {
        this.router.navigate(['/admin']);
    };
    FormBuilderComponent = __decorate([
        Component({
            selector: 'app-form-builder',
            templateUrl: './form-builder.component.html',
            styleUrls: ['./form-builder.component.scss']
        }),
        __metadata("design:paramtypes", [FormsService,
            Router,
            ActivatedRoute])
    ], FormBuilderComponent);
    return FormBuilderComponent;
}());
export { FormBuilderComponent };
//# sourceMappingURL=form-builder.component.js.map