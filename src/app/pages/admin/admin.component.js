var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormsService } from '../../core/providers/forms.service';
import { Subscription } from "rxjs/Subscription";
var AdminComponent = /** @class */ (function () {
    function AdminComponent(formsservice, router) {
        this.formsservice = formsservice;
        this.router = router;
        this.ngSubscribe = new Subscription();
        this.alertModal = false;
    }
    AdminComponent.prototype.ngOnInit = function () {
        this.loadData();
        this.loadSubscription();
    };
    AdminComponent.prototype.OnDestroy = function () {
        this.ngSubscribe.unsubscribe();
    };
    AdminComponent.prototype.loadSubscription = function () {
        var _this = this;
        setTimeout(function () {
            _this.ngSubscribe.add(_this.formsservice.loadFormSubscription()
                .subscribe(function (_a) {
                _this.loadData();
            }));
        }, 2000);
    };
    AdminComponent.prototype.loadData = function () {
        var _this = this;
        this.formsservice.getFormsAll().subscribe(function (_a) {
            var data = _a.data;
            _this.listDataForms = data.formAll.slice();
        });
    };
    AdminComponent.prototype.goToForm = function () {
        // this.router.navigate(['/forms']);
        this.router.navigate(['home/editform']);
    };
    AdminComponent.prototype.editForm = function (id) {
        this.router.navigate(['home/editform', id]);
    };
    AdminComponent.prototype.deleteForm = function () {
        var _this = this;
        this.formsservice.deleteForm(this.idForm).subscribe(function (_a) {
            _this.loadData();
            _this.closeAlert();
        });
    };
    AdminComponent.prototype.openAlert = function (id) {
        this.idForm = id;
        this.alertModal = true;
    };
    AdminComponent.prototype.closeAlert = function () {
        this.alertModal = false;
    };
    AdminComponent = __decorate([
        Component({
            selector: 'app-admin',
            templateUrl: './admin.component.html',
            styleUrls: ['./admin.component.scss']
        }),
        __metadata("design:paramtypes", [FormsService, Router])
    ], AdminComponent);
    return AdminComponent;
}());
export { AdminComponent };
//# sourceMappingURL=admin.component.js.map