import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsService } from '../../core/providers/forms.service';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  listDataForms: Array<any>;
  searchForm: any;
  ngSubscribe: Subscription = new Subscription();
  alertModal: boolean = false;
  idForm: any;

  constructor(private formsservice: FormsService, public router: Router) { }

  ngOnInit() {
    this.loadData();
    this.loadSubscription();
  }

  OnDestroy() {
    this.ngSubscribe.unsubscribe();
  }

  loadSubscription() {
    setTimeout(() => {
      this.ngSubscribe.add(this.formsservice.loadFormSubscription()
        .subscribe(({}) => {
          this.loadData();
        }));
    }, 2000);
  }

  loadData() {
    this.formsservice.getFormsAll().subscribe(({data}) => {
      this.listDataForms = [...data.formAll];
    });
  }

  goToForm() {
    // this.router.navigate(['/forms']);
    this.router.navigate(['home/editform']);
  }

  editForm(id) {
    this.router.navigate(['home/editform', id]);
  }

  deleteForm() {
    this.formsservice.deleteForm(this.idForm).subscribe(({}) => {
      this.loadData();
      this.closeAlert();
    });
  }

  openAlert(id) {
    this.idForm = id;
    this.alertModal = true;
  }

  closeAlert() {
    this.alertModal = false;
  }
}
