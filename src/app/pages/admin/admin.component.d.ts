import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormsService } from '../../core/providers/forms.service';
import { Subscription } from "rxjs/Subscription";
export declare class AdminComponent implements OnInit {
    private formsservice;
    router: Router;
    listDataForms: Array<any>;
    searchForm: any;
    ngSubscribe: Subscription;
    alertModal: boolean;
    idForm: any;
    constructor(formsservice: FormsService, router: Router);
    ngOnInit(): void;
    OnDestroy(): void;
    loadSubscription(): void;
    loadData(): void;
    goToForm(): void;
    editForm(id: any): void;
    deleteForm(): void;
    openAlert(id: any): void;
    closeAlert(): void;
}
