var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { RmserviceService } from './../../core/providers/rmservice.service';
// import { InkGrabber, AbstractComponent } from '@sirus/stylus';
// import { InkGrabber} from '@sirus/stylus';
import { Component, trigger, transition, style, animate } from '@angular/core';
import * as moment from 'moment';
var CodingComponent = /** @class */ (function () {
    function CodingComponent(rmService) {
        this.rmService = rmService;
        // TYPE OF ARRAY
        this.dataDoctors = [];
        this.dataPatient = [];
        this.dataCT = [];
        this.icdxfix = [];
        this.icd9fix = [];
        this.icd9 = [];
        this.listicd = [];
        this.reviewPatient = [];
        // private components: AbstractComponent[] = [];
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.width = 800;
        this.height = 400;
        this.timeout = 1;
        this.stylus = false;
        this.openAlert = false;
        this.openCoding = false;
        this.autocompleteStatusAction = false;
        this.openPatient = false;
    }
    CodingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rmService.getDataReportBasicNodok(0, 10).subscribe(function (_a) {
            var data = _a.data;
            var datadoctors = data.examineAllByNodok.slice();
            for (var i = 0; i < datadoctors.length; i++) {
                _this.rmService.getDataPatientExamine(datadoctors[i].nodok[0].nodok).subscribe(function (_a) {
                    var data = _a.data;
                    var parseData = _this.parseDataJSON(data);
                    var datadoctor = [parseData.examineByNodok];
                    var counter = datadoctor[0].length;
                    if (counter > 0) {
                        if (counter > 1) {
                            _this.dataDoctors.push({
                                nama_dok: datadoctor[0][0].nama_dok,
                                poli: datadoctor[0][0].poli,
                                nodok: datadoctor[0][0].nodok,
                                badge: counter
                            });
                        }
                        else {
                            _this.dataDoctors.push({
                                nama_dok: datadoctor[0][0].nama_dok,
                                poli: datadoctor[0][0].poli,
                                nodok: datadoctor[0][0].nodok,
                                badge: counter
                            });
                        }
                    }
                });
            }
        });
    };
    CodingComponent.prototype.openModalPatient = function (index, nodok, nama_dok) {
        var _this = this;
        this.openPatient = true;
        this.selectedRow = index;
        this.doctorName = nama_dok;
        this.rmService.getDataPatientExamine(nodok).subscribe(function (_a) {
            var data = _a.data;
            var datapasien = data.examineByNodok.slice();
            if (datapasien) {
                _this.dataPatient = datapasien;
            }
            else {
                _this.dataPatient = [];
            }
        });
    };
    CodingComponent.prototype.editCoding = function (no_medrec, no_periksa) {
        var _this = this;
        this.stylus = false;
        this.medrecPatient = "";
        this.openCoding = true;
        this.rmService.getDataIntegratedNoteExamine(no_periksa).subscribe(function (_a) {
            var data = _a.data;
            var parseData = _this.parseDataJSON(data);
            var integratedNote = parseData.ctByExamine;
            if (integratedNote.length > 0) {
                _this.medrecPatient = integratedNote[0].no_medrec;
                _this.dataCT = [integratedNote[0]];
                _this.icdxfix = JSON.parse(integratedNote[0].a_icdx);
                if (integratedNote[0].a_icd9) {
                    _this.icd9fix = JSON.parse(integratedNote[0].a_icd9);
                }
            }
        });
        this.rmService.getReviewById(no_medrec).subscribe(function (_a) {
            var data = _a.data;
            var parseData = _this.parseDataJSON(data);
            var reviewByMedrec = parseData.reviewByMedrec;
            _this.reviewPatient = [reviewByMedrec];
        });
    };
    CodingComponent.prototype.closePatient = function () {
        this.openPatient = false;
    };
    CodingComponent.prototype.searchICD9 = function (ev) {
        var _this = this;
        var val = ev;
        if (this.searchQueryICD9 !== '') {
            this.autocompleteStatusAction = true;
            this.rmService.getDataICD9(val, 0, 10).subscribe(function (_a) {
                var data = _a.data;
                _this.icd9 = data.icd9Pagination.slice();
            });
            if (val && val.trim() !== '') {
                this.icd9 = this.icd9.filter(function (item) {
                    return (item.deskripsi.toLowerCase().indexOf(val.toLowerCase()) > -1);
                });
            }
        }
        else {
            this.autocompleteStatusAction = false;
        }
    };
    CodingComponent.prototype.addICD9 = function (ev) {
        var val = this.icd9[ev];
        this.searchQueryICD9 = '';
        this.autocompleteStatusAction = false;
        this.listicd.push(val);
    };
    CodingComponent.prototype.removeICD9 = function (ev) {
        this.listicd.splice(ev, 1);
    };
    CodingComponent.prototype.submitCoding = function () {
        var _this = this;
        var icdstr = JSON.stringify(this.listicd);
        this.rmService.updateCTICD9(this.medrecPatient, icdstr).subscribe(function (_a) {
            var data = _a.data;
            if (data) {
                _this.openCoding = false;
                _this.messageAlert = "Berhasil menginput ICD9";
                _this.openAlert = true;
                window.location.reload();
            }
            else {
                _this.messageAlert = "Gagal menginput ICD9";
                _this.openAlert = true;
            }
        });
    };
    CodingComponent.prototype.drawcanvas = function (strokedata, status) {
        // this.components = [];
        this.stylus = !this.stylus;
        var x = [];
        var y = [];
        var t = [];
        if (strokedata !== null) {
            var datastroke = JSON.parse(strokedata);
            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);
            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);
            this.element = document.getElementById("canvas");
            var imageRenderingCanvas = this.element;
            var ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            // this.inkGrabber = new InkGrabber(ctx);
            // this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (var i = 1; i < x.length - 1; i++) {
                if (x[i] == null) {
                    // this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
                    // let stroke = this.inkGrabber.getStroke();
                    // this.components.push(stroke);
                    // this.inkGrabber = new InkGrabber(ctx);
                    // this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                }
                else {
                    // this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            // this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
            // let stroke = this.inkGrabber.getStroke();
            // this.components.push(stroke);
            // this.inkGrabberFinal = new InkGrabber(ctx);
            // for (let i = 0; i < this.components.length; i++ ) {
            //   this.inkGrabberFinal.drawComponent(this.components[i]);
            // }
            var image = new Image();
            image.id = "pic";
            var content = this.element;
            image.src = content.toDataURL();
            this.dataCT[0]["image" + status] = image.src;
        }
        else {
            this.dataCT[0]["image" + status] = "../../images/no_image.png";
        }
    };
    CodingComponent.prototype.parseDataJSON = function (data) {
        var strData = JSON.stringify(data);
        var parsData = JSON.parse(strData);
        return parsData;
    };
    CodingComponent.prototype.mapToMoment = function (date) {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    };
    CodingComponent.prototype.generateArray = function (obj) {
        if (obj) {
            return JSON.parse(obj);
        }
        else {
            return null;
        }
    };
    CodingComponent = __decorate([
        Component({
            styleUrls: ['./coding.component.scss'],
            templateUrl: './coding.component.html',
            animations: [
                trigger('fadeInOut', [
                    transition(':enter', [
                        style({ width: 0 }),
                        animate(300, style({ width: 600 }))
                    ]),
                    transition(':leave', [
                        animate(300, style({ width: 0 }))
                    ]),
                ])
            ]
        }),
        __metadata("design:paramtypes", [RmserviceService])
    ], CodingComponent);
    return CodingComponent;
}());
export { CodingComponent };
//# sourceMappingURL=coding.component.js.map