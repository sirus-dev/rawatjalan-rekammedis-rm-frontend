import { RmserviceService } from './../../core/providers/rmservice.service';
// import { InkGrabber, AbstractComponent } from '@sirus/stylus';
// import { InkGrabber} from '@sirus/stylus';
import { Component, trigger, transition, style, animate, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
    styleUrls: ['./coding.component.scss'],
    templateUrl: './coding.component.html',
    animations: [
    trigger('fadeInOut', [
        transition(':enter', [
        style({width: 0}),
        animate(300, style({width: 600}))
        ]),
        transition(':leave', [
        animate(300, style({width: 0}))
        ]),
    ])
    ]
})
export class CodingComponent implements OnInit {

    searchdoctor: any;
    searchpatient: any;
    searchQueryICD9: any;
    medrecPatient: string;
    messageAlert: any;
    selectedRow: string;
    doctorName: string;

    // TYPE OF ARRAY
    dataDoctors: Array<any>   = [];
    dataPatient: Array<any>   = [];
    dataCT: Array<any>        = [];
    icdxfix: Array<any>       = [];
    icd9fix: Array<any>       = [];
    icd9: Array<any>          = [];
    listicd: Array<any>       = [];
    reviewPatient: Array<any> = [];

    // STYLUS CONFIGURATION
    // private inkGrabber: InkGrabber;
    // private inkGrabberFinal: InkGrabber;
    private element: HTMLElement;
    // private components: AbstractComponent[] = [];
    arrayX: number[] = [];
    arrayY: number[] = [];
    arrayT: number[] = [];

    width: number   = 800;
    height: number  = 400;
    timeout: number = 1;
    imgSource: any;

    stylus: boolean             = false;
    openAlert: boolean          = false;
    openCoding                  = false;
    autocompleteStatusAction    = false;
    openPatient: boolean        = false;

    constructor(private rmService: RmserviceService) {}

    ngOnInit() {

        this.rmService.getDataReportBasicNodok(0, 10).subscribe(({ data }) => {
            let datadoctors = [...data.examineAllByNodok];
            for (let i = 0; i < datadoctors.length; i++) {
                this.rmService.getDataPatientExamine(datadoctors[i].nodok[0].nodok).subscribe(({ data }) => {
                    let parseData   = this.parseDataJSON(data);
                    let datadoctor  = [parseData.examineByNodok];
                    let counter     = datadoctor[0].length;
                    if (counter > 0) {
                        if (counter > 1) {
                            this.dataDoctors.push({
                            nama_dok: datadoctor[0][0].nama_dok,
                            poli: datadoctor[0][0].poli,
                            nodok: datadoctor[0][0].nodok,
                            badge: counter });
                        } else {
                            this.dataDoctors.push({
                            nama_dok: datadoctor[0][0].nama_dok,
                            poli: datadoctor[0][0].poli,
                            nodok: datadoctor[0][0].nodok,
                            badge: counter });
                        }
                    }
                });
            }
        });
    }

    openModalPatient(index: string, nodok: string, nama_dok: string) {
        this.openPatient    = true;
        this.selectedRow    = index;
        this.doctorName     = nama_dok;
        this.rmService.getDataPatientExamine(nodok).subscribe(({data}) => {
            let datapasien = [...data.examineByNodok];
            if (datapasien) {
                this.dataPatient = datapasien;
            } else {
                this.dataPatient = [];
            }
        });
    }

    editCoding(no_medrec: string, no_periksa: string) {
        this.stylus         = false;
        this.medrecPatient  = "";
        this.openCoding     = true;
        this.rmService.getDataIntegratedNoteExamine(no_periksa).subscribe(({data}) => {
            let parseData = this.parseDataJSON(data);
            let integratedNote = parseData.ctByExamine;
            if (integratedNote.length > 0) {
                this.medrecPatient  = integratedNote[0].no_medrec;
                this.dataCT         = [integratedNote[0]];
                this.icdxfix        = JSON.parse(integratedNote[0].a_icdx);
                if (integratedNote[0].a_icd9) {
                    this.icd9fix    = JSON.parse(integratedNote[0].a_icd9);
                }
              }
        });

        this.rmService.getReviewById(no_medrec).subscribe(({data}) => {
            let parseData       = this.parseDataJSON(data);
            let reviewByMedrec  = parseData.reviewByMedrec;
            this.reviewPatient  = [reviewByMedrec];
        });
    }

    closePatient() {
        this.openPatient = false;
    }

    searchICD9(ev) {
        let val = ev;
        if (this.searchQueryICD9 !== '') {
            this.autocompleteStatusAction = true;
            this.rmService.getDataICD9(val, 0, 10).subscribe(({data}) => {
                this.icd9 = [...data.icd9Pagination];
            });
            if (val && val.trim() !== '') {
                this.icd9 = this.icd9.filter((item) => {
                return (item.deskripsi.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
            }
        } else {
            this.autocompleteStatusAction = false;
        }
    }

    addICD9(ev) {
        let val                       = this.icd9[ev];
        this.searchQueryICD9          = '';
        this.autocompleteStatusAction = false;
        this.listicd.push(val);
    }

    removeICD9(ev: any) {
        this.listicd.splice(ev, 1);
    }

    submitCoding() {
        let icdstr = JSON.stringify(this.listicd);
        this.rmService.updateCTICD9(this.medrecPatient, icdstr).subscribe(({ data }) => {
            if (data) {
                this.openCoding     = false;
                this.messageAlert   = "Berhasil menginput ICD9";
                this.openAlert      = true;
                window.location.reload();
            } else {
                this.messageAlert   = "Gagal menginput ICD9";
                this.openAlert      = true;
            }
        });
    }

    drawcanvas(strokedata: any, status: any) {
        // this.components = [];
        this.stylus     = !this.stylus;
        let x: number[] = [];
        let y: number[] = [];
        let t: number[] = [];

        if (strokedata !== null) {
          let datastroke = JSON.parse(strokedata);

            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);

            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);

            this.element = document.getElementById("canvas");
            let imageRenderingCanvas = <HTMLCanvasElement>this.element;
            let ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            // this.inkGrabber = new InkGrabber(ctx);

            // this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (let i = 1; i < x.length - 1; i++) {

              if (x[i] == null) {
                // this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);

                // let stroke = this.inkGrabber.getStroke();

                // this.components.push(stroke);

                // this.inkGrabber = new InkGrabber(ctx);

                // this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
              } else {
                // this.inkGrabber.continueCapture(x[i], y[i], t[i]);
              }
            }
            // this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);

            // let stroke = this.inkGrabber.getStroke();
            // this.components.push(stroke);

            // this.inkGrabberFinal = new InkGrabber(ctx);

            // for (let i = 0; i < this.components.length; i++ ) {
            //   this.inkGrabberFinal.drawComponent(this.components[i]);
            // }

            let image                           = new Image();
            image.id                            = "pic";
            let content                         = <HTMLCanvasElement>this.element;
            image.src                           = content.toDataURL();
            this.dataCT[0]["image" + status]    = image.src;

        } else {
            this.dataCT[0]["image" + status] = "../../images/no_image.png";
        }

    }

    private parseDataJSON(data) {
        let strData   = JSON.stringify(data);
        let parsData  = JSON.parse(strData);
        return parsData;
    }

    mapToMoment(date: string): string {
        if (moment(date).isValid()) {
            return moment(date, "YYYY-MM-DD").format('DD/MM/YYYY');
        }
        return "-";
    }

    generateArray(obj) {
        if (obj) {
            return JSON.parse(obj);
        } else {
            return null;
        }
    }

}
