import { AuthService } from './../../core/providers/auth.service';
import { Router } from '@angular/router';
import { OnInit } from '@angular/core';
export declare class HomeComponent implements OnInit {
    private router;
    private auth;
    constructor(router: Router, auth: AuthService);
    logout(): void;
    ngOnInit(): void;
}
