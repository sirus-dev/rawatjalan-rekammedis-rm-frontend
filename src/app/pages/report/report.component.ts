import { RmserviceService } from './../../core/providers/rmservice.service';
import { Component } from '@angular/core';
import { IMyDpOptions } from 'mydatepicker';
// import { InkGrabber } from '@sirus/stylus';
// import { AbstractComponent } from '@sirus/stylus';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as XLSXStyle from 'xlsx';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
// pdfMake.vfs = pdfFonts.pdfMake.vfs;

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
    styleUrls: ['./report.component.scss'],
    selector: 'report-page',
    templateUrl: 'report.component.html'
})
export class ReportComponent implements OnInit {
    dateArray: any[];
    docDefinition: any;
    docFormat = 'pdf';
    reportFormat = 'rekap';

    // TYPE OF ARRAY
    dataReport: Array<any> = [];
    dataReportExport: Array<any> = [];
    detailPatient: Array<any> = [];
    listpembayaran: any = [];
    listdoctor: any;
    listbatal: any;
    listrujuk: any;
    listpenyakit: any;

    // STYLUS CONFIGURATION
    // private inkGrabber: InkGrabber;
    // private inkGrabberFinal: InkGrabber;
    private element: HTMLElement;
    // private components: AbstractComponent[] = [];
    arrayX: number[] = [];
    arrayY: number[] = [];
    arrayT: number[] = [];
    width: number = 800;
    height: number = 400;
    timeout: number = 1;
    imgSource: any;

    // DATE PICKER CONFIGURATION
    myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'dd/mm/yyyy',
        todayBtnTxt: 'Today',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        inline: false,
        disableUntil: { year: 2016, month: 8, day: 10 },
    };

    // TYPE OF STRING
    startdate;
    enddate;
    placeholder: string = 'Select a date';
    show: string = 'all';
    dateStart: string;
    dateEnd: string;
    nameOfDocument: string = '';
    nameOfDocumentDetailTime: string = '';
    nameOfDocumentDetailPatient: string = '';

    // TYPE OF BOOLEAN
    open: boolean = false;
    stylus: boolean[] = [false];
    chart: boolean = false;

    // CHART
    chartOptions: any;
    chartData: any[];
    chartLabels: any[];
    BPJS: any;
    Pribadi: any;
    Asuransi: any;

    constructor(private rmService: RmserviceService) { pdfMake.vfs = pdfFonts.pdfMake.vfs; }

    ngOnInit() {
        let d: Date = new Date();
        this.dateStart = "0-00-00";

        let yearend = d.getFullYear();
        let monthend = d.getMonth() + 1;
        let dayend = d.getDate();
        let monthend0;
        let dayend0;

        if (monthend < 10) {
            monthend0 = '0' + monthend;
        } else {
            monthend0 = monthend;
        }
        if (dayend < 10) {
            dayend0 = '0' + dayend;
        } else {
            dayend0 = dayend;
        }

        this.dateEnd = yearend + "-" + monthend0 + "-" + dayend0;
        this.loadData();
    }

    loadData() {
        this.rmService.getDataReportByDateRange(this.dateStart, this.dateEnd).subscribe(({ data }) => {

            this.dataReport = [];
            let datareport = [...data.examineJoinByDateRange];
            let reportdata = this.parseDataJSON(datareport);
            for (let i = 0; i < reportdata.length; i++) {
                reportdata[i]["index"] = i;
                if (reportdata[i]["diagnosa"] !== "") {
                    reportdata[i]["showDiagnosa"] = "...";
                } else {
                    if (reportdata[i]["diagnosa_stroke"] != null) {
                        reportdata[i]["showDiagnosa"] = "(Stylus)";
                    } else {
                        reportdata[i]["showDiagnosa"] = " ";
                    }
                }
                this.dataReport.push(reportdata[i]);
            }

            this.dataReportExport = [];
            let datareportexport = [...data.examineJoinByDateRange];
            let reportdataexport = this.parseDataJSON(datareportexport);
            for (let i = 0; i < reportdataexport.length; i++) {
                if (reportdataexport[i]["diagnosa"] !== "") {
                    reportdataexport[i]["showDiagnosa"] = "...";
                } else {
                    if (reportdataexport[i]["diagnosa_stroke"] != null) {
                        reportdataexport[i]["showDiagnosa"] = "(Stylus)";
                    } else {
                        reportdataexport[i]["showDiagnosa"] = " ";
                    }
                }
                this.dataReportExport.push({
                    No: i + 1,
                    Tanggal: reportdataexport[i].tgl,
                    Nama_Pasien: reportdataexport[i].nama_pasien,
                    No_RM: reportdataexport[i].no_medrec[0].no_medrec,
                    Jenis_Kelamin: reportdataexport[i].kelamin,
                    Umur: reportdataexport[i].umur,
                    Dokter: reportdataexport[i].nama_dok,
                    Spesialis: reportdataexport[i].nodok[0].specialist,
                    Pembayaran: reportdataexport[i].jaminan,
                    Diagnosa: reportdataexport[i].showDiagnosa,
                    Kasus: reportdataexport[i].kasus,
                    Kunjungan: reportdataexport[i].kunjungan,
                    Alamat: reportdataexport[i].no_medrec[0].address
                });
            }
            this.nameOfDocument = 'Rekap Laporan Pasien';
        });
    }

    formatReport(event) {
        let value = event.target.value;
        this.reportFormat = value;
        // let totalasuransi = 0;
        // let totalpribadi = 0;
        // let totalbpjs = 0;
        this.Asuransi = 0;
        this.Pribadi = 0;
        this.BPJS = 0;
        if (value === 'rekap') {
            this.show = 'all';
            this.loadData();
        } else if (value === 'pembayaran') {
            this.show = 'pembayaran';

            this.rmService.getDataReportBasicNodok(0, 10).subscribe(({ data }) => {
                this.dataReportExport = [];
                this.nameOfDocument = 'Laporan Pasien Pembayaran';
                this.nameOfDocumentDetailTime = '';
                this.nameOfDocumentDetailPatient = '';
                this.listpembayaran = [];
                let datareport = [...data.examineAllByNodok];
                let reportdata = this.parseDataJSON(datareport);

                for (let i = 0; i < reportdata.length; i++) {
                    this.rmService.getDataPatientExamine(reportdata[i].nodok[0].nodok).subscribe(({ data }) => {
                        let parsperiksa = this.parseDataJSON(data);
                        let datadoctor = [parsperiksa.examineByNodok];
                        let counterbpjs = 0;
                        let counterasuransi = 0;
                        let counterpribadi = 0;
                        let counter = datadoctor[0].length;
                        if (counter > 0) {
                            for (let j = 0; j < counter; j++) {
                                if (datadoctor[0][j].jaminan === "BPJS") {
                                    if (counterbpjs > 0) {
                                        counterbpjs++;
                                    } else {
                                        counterbpjs = 1;
                                    }
                                }
                                if (datadoctor[0][j].jaminan === "Asuransi") {
                                    if (counterasuransi > 0) {
                                        counterasuransi++;
                                    } else {
                                        counterasuransi = 1;
                                    }
                                }
                                if (datadoctor[0][j].jaminan === "Pribadi") {
                                    if (counterpribadi > 0) {
                                        counterpribadi++;
                                    } else {
                                        counterpribadi = 1;
                                    }
                                }
                            }

                            this.Asuransi += counterasuransi;
                            this.Pribadi += counterpribadi;
                            this.BPJS += counterbpjs;

                            if (i === reportdata.length - 1) {
                                this.chart = false;
                                this.showChart();
                            }

                            this.listpembayaran.push({
                                id: i,
                                nama_dok: reportdata[i].nama_dok,
                                pribadi: counterpribadi,
                                asuransi: counterasuransi,
                                bpjs: counterbpjs
                            });


                            this.dataReportExport.push({
                                No: i + 1,
                                Nama_Dokter: reportdata[i].nama_dok,
                                Pribadi: counterpribadi,
                                Asuransi: counterasuransi,
                                BPJS: counterbpjs
                            });

                        }
                    });
                }
            });
        } else if (value === 'dokter') {
            this.show = 'dokter';
            this.dataReportExport = [];
            this.nameOfDocument = 'Laporan Pasien Per Dokter';
            this.nameOfDocumentDetailTime = '';
            this.nameOfDocumentDetailPatient = '';
            this.listdoctor = [
                { dokter: 'Anasry dr SpA', spesialis: 'Anak', januari: '27', februari: '29', maret: '12', april: '0', mei: '0', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '68' },
                { dokter: 'Haris dr SpA', spesialis: 'Anak', januari: '346', februari: '277', maret: '379', april: '341', mei: '345', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '1688' },
                { dokter: 'Rusdi dr SpB', spesialis: 'Bedah', januari: '130', februari: '116', maret: '264', april: '95', mei: '88', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '693' },
                { dokter: 'Jansu dr SpB', spesialis: 'Bedah', januari: '275', februari: '248', maret: '148', april: '276', mei: '300', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '1247' },
                { dokter: 'Irman dr SpPD', spesialis: 'Dalam', januari: '36', februari: '20', maret: '41', april: '56', mei: '14', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '167' },
                { dokter: 'Rusli dr SpPD', spesialis: 'Dalam', januari: '1021', februari: '955', maret: '1075', april: '816', mei: '862', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '4729' },
                { dokter: 'Ande AmF', spesialis: 'Fisioterapi', januari: '66', februari: '54', maret: '60', april: '46', mei: '65', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '291' },
                { dokter: 'Rusti AmF', spesialis: 'Fisioterapi', januari: '37', februari: '14', maret: '22', april: '14', mei: '28', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '115' },
                { dokter: 'Hanifah drg', spesialis: 'Gizi', januari: '', februari: '', maret: '11', april: '12', mei: '9', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '32' },
            ];

            let reportdataexport = this.listdoctor;
            for (let i = 0; i < reportdataexport.length; i++) {
                this.dataReportExport.push({
                    No: i + 1,
                    Nama_Dokter: reportdataexport[i].dokter,
                    Spesialis: reportdataexport[i].spesialis,
                    Januari: reportdataexport[i].januari,
                    Februari: reportdataexport[i].februari,
                    Maret: reportdataexport[i].maret,
                    April: reportdataexport[i].april,
                    Mei: reportdataexport[i].mei,
                    Juni: reportdataexport[i].juni,
                    Juli: reportdataexport[i].juli,
                    Agustus: reportdataexport[i].agustus,
                    September: reportdataexport[i].september,
                    Oktober: reportdataexport[i].oktober,
                    November: reportdataexport[i].november,
                    Desember: reportdataexport[i].desember,
                    Jumlah: reportdataexport[i].jumlah
                });
            }

        } else if (value === 'batal') {
            this.show = 'batal';
            this.dataReportExport = [];
            this.nameOfDocument = 'Laporan Jumlah Pasien Batal';
            this.nameOfDocumentDetailTime = '';
            this.nameOfDocumentDetailPatient = '';
            this.listbatal = [
                { tanggal: '12/07/2017', name: 'Jajang Jaenudin', norm: '324663', dokter: 'Dr. Asep Surasep', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Raka Maheka', norm: '234332', dokter: 'Dr. Koesman', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Asep Soejali', norm: '234253', dokter: 'Dr. Maman S', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Saepudin Koesnandar', norm: '565466', dokter: 'Dr. Soekismo Widjaya', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Winaldi Saputra', norm: '324663', dokter: 'Dr. Rozak Muzakir', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Yusuf Ramadhan', norm: '234332', dokter: 'Dr. Abdul Syarief', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Enjang Kosasih', norm: '234253', dokter: 'Dr. Jaenudin', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Ety Supriati', norm: '565466', dokter: 'Dr. Yoseph', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' }
            ];

            let reportdataexport = this.listbatal;
            for (let i = 0; i < reportdataexport.length; i++) {
                this.dataReportExport.push({
                    No: i + 1,
                    Tanggal: reportdataexport[i].tanggal,
                    Nama: reportdataexport[i].name,
                    No_RM: reportdataexport[i].norm,
                    Nama_Dokter: reportdataexport[i].dokter,
                    Spesialis: reportdataexport[i].spesialis,
                    Pembayaran: reportdataexport[i].pemabayaran
                });
            }

        } else if (value === 'rujuk') {
            this.show = 'rujuk';
            this.dataReportExport = [];
            this.nameOfDocument = 'Laporan Pasien Dirujuk';
            this.nameOfDocumentDetailTime = '';
            this.nameOfDocumentDetailPatient = '';
            this.listrujuk = [
                { tanggal: '12/07/2017', name: 'Jajang Jaenudin', norm: '324663', dokter: 'Dr. Asep Surasep', spesialis: 'Ahli Bedah', diagnosa: 'Sakit Perut', rujuk: 'RSCM', rawatinap: 'Rawat' },
                { tanggal: '12/07/2017', name: 'Raka Maheka', norm: '234332', dokter: 'Dr. Koesman', spesialis: 'Ahli Bedah', diagnosa: 'Usus', rujuk: 'RSCM', rawatinap: 'Rawat' },
                { tanggal: '12/07/2017', name: 'Asep Soejali', norm: '234253', dokter: 'Dr. Maman S', spesialis: 'Ahli Bedah', diagnosa: 'Jantung', rujuk: 'RSCM', rawatinap: 'Rawat' },
                { tanggal: '12/07/2017', name: 'Saepudin Koesnandar', norm: '565466', dokter: 'Dr. Soekismo Widjaya', spesialis: 'Ahli Bedah', diagnosa: 'Sakit Perut', rujuk: 'RSCM', rawatinap: 'Rawat' }
            ];

            let reportdataexport = this.listrujuk;
            for (let i = 0; i < reportdataexport.length; i++) {
                this.dataReportExport.push({
                    No: i + 1,
                    Tanggal: reportdataexport[i].tanggal,
                    Nama: reportdataexport[i].name,
                    No_RM: reportdataexport[i].norm,
                    Nama_Dokter: reportdataexport[i].dokter,
                    Spesialis: reportdataexport[i].spesialis,
                    Diagnosa: reportdataexport[i].diagnosa,
                    Rujuk: reportdataexport[i].rujuk,
                    Rawat_Inap: reportdataexport[i].rawatinap
                });
            }

        } else if (value === 'penyakit') {
            this.show = 'penyakit';
            this.dataReportExport = [];
            this.nameOfDocument = 'Laporan 10 Besar Penyakit';
            this.nameOfDocumentDetailTime = '';
            this.nameOfDocumentDetailPatient = '';
            this.listpenyakit = [
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '1', icd10: 'J06.9', desc: 'ispa', kasuslk: '9', kasuspr: '5', jumlahkasus: '14', jumlahkunjungan: '128' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '2', icd10: 'I99', desc: 'cva', kasuslk: '2', kasuspr: '0', jumlahkasus: '2', jumlahkunjungan: '135' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '3', icd10: 'H26.9', desc: 'kat imatur', kasuslk: '3', kasuspr: '5', jumlahkasus: '8', jumlahkunjungan: '135' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '4', icd10: 'M19.9', desc: 'oa', kasuslk: '2', kasuspr: '3', jumlahkasus: '5', jumlahkunjungan: '155' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '5', icd10: 'M54.5', desc: 'lbp', kasuslk: '1', kasuspr: '6', jumlahkasus: '7', jumlahkunjungan: '162' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '6', icd10: 'K30', desc: 'dyspepsia', kasuslk: '1', kasuspr: '6', jumlahkasus: '7', jumlahkunjungan: '164' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '7', icd10: 'A15.1', desc: 'kp', kasuslk: '0', kasuspr: '1', jumlahkasus: '1', jumlahkunjungan: '178' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '8', icd10: 'I11.9', desc: 'hhd', kasuslk: '2', kasuspr: '1', jumlahkasus: '3', jumlahkunjungan: '186' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '9', icd10: 'I10', desc: 'ht', kasuslk: '1', kasuspr: '1', jumlahkasus: '2', jumlahkunjungan: '238' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '10', icd10: 'A16.2', desc: 'tb paru', kasuslk: '4', kasuspr: '2', jumlahkasus: '6', jumlahkunjungan: '455' }
            ];

            let reportdataexport = this.listpenyakit;
            for (let i = 0; i < reportdataexport.length; i++) {
                this.dataReportExport.push({
                    No: i + 1,
                    Nama_RS: reportdataexport[i].namars,
                    Bulan: reportdataexport[i].bulan,
                    Tahun: reportdataexport[i].tahun,
                    No_Urut: reportdataexport[i].nourut,
                    Kode_ICD_10: reportdataexport[i].icd10,
                    Deskripsi: reportdataexport[i].desc,
                    Kasus_Baru_Lakilaki: reportdataexport[i].kasuslk,
                    Kasus_Baru_Perempuan: reportdataexport[i].kasuspr,
                    Jumlah_Kasus_Baru: reportdataexport[i].jumlahkasus,
                    Jumlah_Kunjungan: reportdataexport[i].jumlahkunjungan
                });
            }

        }
    }

    openModal(val: any) {
        this.stylus = [false];
        this.open = true;
        this.detailPatient = [val];

        // let nomedrec = this.detailPatient[0].no_medrec[0].no_medrec;
    }

    showChart() {
        this.chart = !this.chart;
        this.chartOptions = { responsive: true };
        this.chartData = [{ data: [this.Asuransi, this.Pribadi, this.BPJS] }];
        this.chartLabels = ['Pribadi', 'Asuransi', 'BPJS'];
    }

    onDateStartChanged(event) {
        if (event.date.year === 0 && event.date.day === 0 && event.date.month === 0) {
            this.rmService.getDataReportByDateRange(this.dateStart, this.dateEnd).subscribe(({}) => {
                // this.dataReport = [];
                // let datareport = [...data.examineJoinByDateRange];
                // let reportdata = JSON.stringify(datareport);
                // let pars = JSON.parse(reportdata);
                // for (let i = 0; i < pars.length; i++) {
                //     pars[i]["index"] = i;

                //     this.dataReport.push(pars[i]);
                // }
                if (this.reportFormat === 'rekap') {
                    this.loadData();
                }
            });
        }
    }
    onDateEndChanged(event) {
        if (event.date.year === 0 && event.date.day === 0 && event.date.month === 0) {
            this.rmService.getDataReportByDateRange(this.dateStart, this.dateEnd).subscribe(({ data }) => {
                this.nameOfDocumentDetailTime = '';
                this.dataReport = [];
                let datareport = [...data.examineJoinByDateRange];
                let reportdata = JSON.stringify(datareport);
                let pars = JSON.parse(reportdata);
                for (let i = 0; i < pars.length; i++) {
                    pars[i]["index"] = i;

                    this.dataReport.push(pars[i]);
                }
            });
        }
    }

    dateRange() {
        if (this.startdate !== '' && this.enddate !== '') {
            let yearstart = this.startdate.date.year;
            let monthstart = this.startdate.date.month;
            let daystart = this.startdate.date.day;
            if (monthstart < 10) {
                monthstart = '0' + monthstart;
            }
            if (daystart < 10) {
                daystart = '0' + daystart;
            }

            let datestart = yearstart + "-" + monthstart + "-" + daystart;

            let yearend = this.enddate.date.year;
            let monthend = this.enddate.date.month;
            let dayend = this.enddate.date.day;
            if (monthend < 10) {
                monthend = '0' + monthend;
            }
            if (dayend < 10) {
                dayend = '0' + dayend;
            }

            let dateend = yearend + "-" + monthend + "-" + dayend;

            this.rmService.getDataReportByDateRange(datestart, dateend).subscribe(({ data }) => {
                this.dataReport = [];
                let datareport = [...data.examineJoinByDateRange];
                let reportdata = JSON.stringify(datareport);
                let pars = JSON.parse(reportdata);
                for (let i = 0; i < pars.length; i++) {
                    pars[i]["index"] = i;
                    this.dataReport.push(pars[i]);
                }
                this.dataReportExport = [];
                let datareportexport = [...data.examineJoinByDateRange];
                let reportdataexport = this.parseDataJSON(datareportexport);
                for (let i = 0; i < reportdataexport.length; i++) {
                    if (reportdataexport[i]["diagnosa"] !== "") {
                        reportdataexport[i]["showDiagnosa"] = "...";
                    } else {
                        if (reportdataexport[i]["diagnosa_stroke"] != null) {
                            reportdataexport[i]["showDiagnosa"] = "(Stylus)";
                        } else {
                            reportdataexport[i]["showDiagnosa"] = " ";
                        }
                    }
                    this.dataReportExport.push({
                        No: i + 1,
                        Tanggal: reportdataexport[i].tgl,
                        Nama_Pasien: reportdataexport[i].nama_pasien,
                        No_RM: reportdataexport[i].no_medrec[0].no_medrec,
                        Jenis_Kelamin: reportdataexport[i].kelamin,
                        Umur: reportdataexport[i].umur,
                        Dokter: reportdataexport[i].nama_dok,
                        Spesialis: reportdataexport[i].nodok[0].specialist,
                        Pembayaran: reportdataexport[i].jaminan,
                        Diagnosa: reportdataexport[i].showDiagnosa,
                        Kasus: reportdataexport[i].kasus,
                        Kunjungan: reportdataexport[i].kunjungan,
                        Alamat: reportdataexport[i].no_medrec[0].address
                    });
                }
                this.nameOfDocument = 'Rekap Laporan Pasien';
                this.nameOfDocumentDetailTime = datestart + ' - ' + dateend;
            });
        } else {

        }
    }

    patientStatus($event) {
        let kunjungan = $event.target.value;
        if (kunjungan === "0") {
            this.loadData();
        } else {
            this.rmService.getDataReportByVisit(kunjungan, 0, 10).subscribe(({ data }) => {
                this.dataReport = [];
                let datareport = [...data.examineJoinByVisit];
                let reportdata = this.parseDataJSON(datareport);
                for (let i = 0; i < reportdata.length; i++) {
                    reportdata[i]["index"] = i;
                    if (reportdata[i]["diagnosa"] !== "") {
                        reportdata[i]["showDiagnosa"] = "...";
                    } else {
                        if (reportdata[i]["diagnosa_stroke"] != null) {
                            reportdata[i]["showDiagnosa"] = "(Stylus)";
                        } else {
                            reportdata[i]["showDiagnosa"] = " ";
                        }
                    }
                    this.dataReport.push(reportdata[i]);
                }
                this.dataReportExport = [];
                let datareportexport = [...data.examineJoinByVisit];
                let reportdataexport = this.parseDataJSON(datareportexport);
                for (let i = 0; i < reportdataexport.length; i++) {
                    if (reportdataexport[i]["diagnosa"] !== "") {
                        reportdataexport[i]["showDiagnosa"] = "...";
                    } else {
                        if (reportdataexport[i]["diagnosa_stroke"] != null) {
                            reportdataexport[i]["showDiagnosa"] = "(Stylus)";
                        } else {
                            reportdataexport[i]["showDiagnosa"] = " ";
                        }
                    }
                    this.dataReportExport.push({
                        No: i + 1,
                        Tanggal: reportdataexport[i].tgl,
                        Nama_Pasien: reportdataexport[i].nama_pasien,
                        No_RM: reportdataexport[i].no_medrec[0].no_medrec,
                        Jenis_Kelamin: reportdataexport[i].kelamin,
                        Umur: reportdataexport[i].umur,
                        Dokter: reportdataexport[i].nama_dok,
                        Spesialis: reportdataexport[i].nodok[0].specialist,
                        Pembayaran: reportdataexport[i].jaminan,
                        Diagnosa: reportdataexport[i].showDiagnosa,
                        Kasus: reportdataexport[i].kasus,
                        Kunjungan: reportdataexport[i].kunjungan,
                        Alamat: reportdataexport[i].no_medrec[0].address
                    });
                }
                this.nameOfDocument = 'Rekap Laporan Pasien';
                this.nameOfDocumentDetailPatient = kunjungan;
            });
        }
    }

    private parseDataJSON(data) {
        let strData = JSON.stringify(data);
        let parsData = JSON.parse(strData);
        return parsData;
    }

    // Export to Excel
    exportAsExcelFile(excelFileName: string): void {

        const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.dataReportExport);
        this.wrapAndCenterCell(worksheet.B2);
        const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };

        const excelBuffer: any = XLSXStyle.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    }

    wrapAndCenterCell(cell: XLSX.CellObject) {
        const wrapAndCenterCellStyle = { alignment: { wrapText: true, vertical: 'center', horizontal: 'center' } };
        this.setCellStyle(cell, wrapAndCenterCellStyle);
    }

    setCellStyle(cell: XLSX.CellObject, style: {}) {
        cell.s = style;
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '[' + new Date().toLocaleString() + ']' + EXCEL_EXTENSION);
    }

    exportExcel() {
        this.exportAsExcelFile(this.nameOfDocument);
    }
    // End of Export to Excel

    // Export to PDF
    buildTableBody(data, columns, headers) {
        let body = [];
        body.push(headers);
        data.forEach(row => {
            let dataRow = [];
            columns.forEach(element => {
                if (element) {
                    dataRow.push(row[element]);
                }
            });
            body.push(dataRow);
        });

        return body;
    }

    table(data, columns, headers, width) {
        return {
            table: {
                headerRows: 1,
                widths: width,
                body: this.buildTableBody(data, columns, headers)
            }
        };
    }

    encodeLogoImage(): string {
        let element = document.getElementById('logo');
        let image = new Image();
        image = <HTMLImageElement>element;
        let canvas = document.createElement("canvas");
        canvas.width = image.width;
        canvas.height = image.height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0, image.width, image.height);
        let dataURL = canvas.toDataURL();
        return dataURL;
    }

    exportPDF(value) {
        let rowHeader = [];
        let rowHeaderPDF = [];
        let width = [];

        if (value === 'rekap') {
            rowHeader = [
                'No',
                'Tanggal',
                'Nama_Pasien',
                'No_RM',
                'Jenis_Kelamin',
                'Umur',
                'Dokter',
                'Spesialis',
                'Pembayaran',
                'Diagnosa',
                'Kasus',
                'Kunjungan',
                'Alamat'];
            rowHeaderPDF = [
                'No',
                'Tanggal',
                'Nama Pasien',
                'No RM',
                'Jenis Kelamin',
                'Umur',
                'Dokter',
                'Spesialis',
                'Pembayaran',
                'Diagnosa',
                'Kasus',
                'Kunjungan',
                'Alamat'];
        } else if (value === 'pembayaran') {
            rowHeader = [
                'No',
                'Nama_Dokter',
                'Pribadi',
                'Asuransi',
                'BPJS'];
            rowHeaderPDF = [
                'No',
                'Nama Dokter',
                'Pribadi',
                'Asuransi',
                'BPJS'];
        } else if (value === 'dokter') {
            rowHeader = [
                'No',
                'Nama_Dokter',
                'Spesialis',
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
                'Jumlah'];
            rowHeaderPDF = [
                'No',
                'Nama Dokter',
                'Spesialis',
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
                'Jumlah'];
        } else if (value === 'batal') {
            rowHeader = [
                'No',
                'Tanggal',
                'Nama',
                'No_RM',
                'Nama_Dokter',
                'Spesialis',
                'Pembayaran'];
            rowHeaderPDF = [
                'No',
                'Tanggal',
                'Nama',
                'No. RM',
                'Nama Dokter',
                'Spesialis',
                'Pembayaran'];
        } else if (value === 'rujuk') {
            rowHeader = [
                'No',
                'Tanggal',
                'Nama',
                'No_RM',
                'Nama_Dokter',
                'Spesialis',
                'Diagnosa',
                'Rujuk',
                'Rawat_Inap'];
            rowHeaderPDF = [
                'No',
                'Tanggal',
                'Nama',
                'No. RM',
                'Nama Dokter',
                'Spesialis',
                'Diagnosa',
                'Rujuk',
                'Rawat Inap'];
        } else if (value === 'penyakit') {
            rowHeader = [
                'No',
                'Nama_RS',
                'Bulan',
                'Tahun',
                'No_Urut',
                'Kode_ICD_10',
                'Deskripsi',
                'Kasus_Baru_Lakilaki',
                'Kasus_Baru_Perempuan',
                'Jumlah_Kasus_Baru',
                'Jumlah_Kunjungan'];
            rowHeaderPDF = [
                'No',
                'Nama RS',
                'Bulan',
                'Tahun',
                'No. Urut',
                'Kode ICD 10',
                'Deskripsi',
                'Kasus Baru (Laki-laki)',
                'Kasus Baru (Perempuan)',
                'Jumlah Kasus Baru',
                'Jumlah Kunjungan'];
        } else {

        }
        for (let i = 0; i < rowHeader.length; i++) {
            if (i === 1) {
                width.push('*');
            } else { width.push('auto'); }
        }

        this.docDefinition = {
            pageSize: 'A4',
            pageOrientation: 'landscape',
            pageMargins: [20, 20, 20, 20],
            content: [
                { image: this.encodeLogoImage(), alignment: 'center' },
                { text: "RUMAH SAKIT", style: 'header', alignment: 'center', fontSize: 22 },
                { text: "MARY CILEUNGSI", style: 'header', alignment: 'center', fontSize: 22 },
                { text: "Jl. Raya Narogong KM 21, Cileungsi, Kabupaten - Bogor, 16820", style: 'header', alignment: 'center' },
                { text: "Telp: (021) 8249222", style: 'header', alignment: 'center' },
                { text: " ", style: 'header', alignment: 'center', fontSize: 22 },
                {
                    columns: [
                        {
                            width: 100,
                            text: 'Subjek'
                        },
                        {
                            text: ": " + this.nameOfDocument
                        },
                    ],
                    columnGap: 10
                },
                {
                    columns: [

                        {
                            width: 100,
                            text: 'Tanggal'
                        },
                        {
                            text: ": " + this.nameOfDocumentDetailTime
                        },
                    ],
                    columnGap: 10
                },
                {
                    columns: [
                        {
                            width: 100,
                            text: 'Status Pasien'
                        },
                        {
                            text: ": " + this.nameOfDocumentDetailPatient
                        },
                    ],
                    columnGap: 10
                },
                { text: " ", style: 'header', alignment: 'center', fontSize: 15 },
                this.table(this.dataReportExport, rowHeader, rowHeaderPDF, width),
                { text: " ", style: 'header', alignment: 'center', fontSize: 15 },
                { text: new Date().toLocaleString(), alignment: 'right' },
            ]
        };
        pdfMake.createPdf(this.docDefinition).download(this.nameOfDocument);
    }
    // End of Export to Excel

    formatDoc(event) {
        this.docFormat = event.target.value;
    }

    exportDoc() {
        if (this.docFormat === 'pdf') {
            this.exportPDF(this.reportFormat);
        } else if (this.docFormat === 'xlsx') {
            this.exportExcel();
        }

    }


    drawcanvas(index: any, strokedata: any, status: any) {
        // this.components = [];
        this.stylus[index] = !this.stylus[index];

        let x: number[] = [];
        let y: number[] = [];
        let t: number[] = [];

        if (strokedata !== null) {
            let datastroke = JSON.parse(strokedata);

            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);

            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);

            this.element = document.getElementById(index);
            let imageRenderingCanvas = <HTMLCanvasElement>this.element;
            let ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            // this.inkGrabber = new InkGrabber(ctx);

            // this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (let i = 1; i < x.length - 1; i++) {

                if (x[i] == null) {
                    // this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);

                    // let stroke = this.inkGrabber.getStroke();

                    // this.components.push(stroke);

                    // this.inkGrabber = new InkGrabber(ctx);

                    // this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                } else {
                    // this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            // this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);

            // let stroke = this.inkGrabber.getStroke();
            // this.components.push(stroke);

            // this.inkGrabberFinal = new InkGrabber(ctx);

            // for (let i = 0; i < this.components.length; i++) {
            //     this.inkGrabberFinal.drawComponent(this.components[i]);
            // }

            let image = new Image();
            image.id = "pic" + index;
            let content = <HTMLCanvasElement>this.element;
            image.src = content.toDataURL();
            this.detailPatient[index]["image"] = image.src;
        } else {
            this.detailPatient[index]["image"] = "images/no_image.png";
        }

    }
    // tslint:disable-next-line:eofline
}