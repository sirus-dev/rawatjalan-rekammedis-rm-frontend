var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { RmserviceService } from './../../core/providers/rmservice.service';
import { Component } from '@angular/core';
// import { InkGrabber } from '@sirus/stylus';
// import { AbstractComponent } from '@sirus/stylus';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as XLSXStyle from 'xlsx';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
// pdfMake.vfs = pdfFonts.pdfMake.vfs;
var EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
var EXCEL_EXTENSION = '.xlsx';
var ReportComponent = /** @class */ (function () {
    function ReportComponent(rmService) {
        this.rmService = rmService;
        this.docFormat = 'pdf';
        this.reportFormat = 'rekap';
        // TYPE OF ARRAY
        this.dataReport = [];
        this.dataReportExport = [];
        this.detailPatient = [];
        this.listpembayaran = [];
        // private components: AbstractComponent[] = [];
        this.arrayX = [];
        this.arrayY = [];
        this.arrayT = [];
        this.width = 800;
        this.height = 400;
        this.timeout = 1;
        // DATE PICKER CONFIGURATION
        this.myDatePickerOptions = {
            dateFormat: 'dd/mm/yyyy',
            todayBtnTxt: 'Today',
            firstDayOfWeek: 'mo',
            sunHighlight: true,
            inline: false,
            disableUntil: { year: 2016, month: 8, day: 10 },
        };
        this.placeholder = 'Select a date';
        this.show = 'all';
        this.nameOfDocument = '';
        this.nameOfDocumentDetailTime = '';
        this.nameOfDocumentDetailPatient = '';
        // TYPE OF BOOLEAN
        this.open = false;
        this.stylus = [false];
        this.chart = false;
        pdfMake.vfs = pdfFonts.pdfMake.vfs;
    }
    ReportComponent.prototype.ngOnInit = function () {
        var d = new Date();
        this.dateStart = "0-00-00";
        var yearend = d.getFullYear();
        var monthend = d.getMonth() + 1;
        var dayend = d.getDate();
        var monthend0;
        var dayend0;
        if (monthend < 10) {
            monthend0 = '0' + monthend;
        }
        else {
            monthend0 = monthend;
        }
        if (dayend < 10) {
            dayend0 = '0' + dayend;
        }
        else {
            dayend0 = dayend;
        }
        this.dateEnd = yearend + "-" + monthend0 + "-" + dayend0;
        this.loadData();
    };
    ReportComponent.prototype.loadData = function () {
        var _this = this;
        this.rmService.getDataReportByDateRange(this.dateStart, this.dateEnd).subscribe(function (_a) {
            var data = _a.data;
            _this.dataReport = [];
            var datareport = data.examineJoinByDateRange.slice();
            var reportdata = _this.parseDataJSON(datareport);
            for (var i = 0; i < reportdata.length; i++) {
                reportdata[i]["index"] = i;
                if (reportdata[i]["diagnosa"] !== "") {
                    reportdata[i]["showDiagnosa"] = "...";
                }
                else {
                    if (reportdata[i]["diagnosa_stroke"] != null) {
                        reportdata[i]["showDiagnosa"] = "(Stylus)";
                    }
                    else {
                        reportdata[i]["showDiagnosa"] = " ";
                    }
                }
                _this.dataReport.push(reportdata[i]);
            }
            _this.dataReportExport = [];
            var datareportexport = data.examineJoinByDateRange.slice();
            var reportdataexport = _this.parseDataJSON(datareportexport);
            for (var i = 0; i < reportdataexport.length; i++) {
                if (reportdataexport[i]["diagnosa"] !== "") {
                    reportdataexport[i]["showDiagnosa"] = "...";
                }
                else {
                    if (reportdataexport[i]["diagnosa_stroke"] != null) {
                        reportdataexport[i]["showDiagnosa"] = "(Stylus)";
                    }
                    else {
                        reportdataexport[i]["showDiagnosa"] = " ";
                    }
                }
                _this.dataReportExport.push({
                    No: i + 1,
                    Tanggal: reportdataexport[i].tgl,
                    Nama_Pasien: reportdataexport[i].nama_pasien,
                    No_RM: reportdataexport[i].no_medrec[0].no_medrec,
                    Jenis_Kelamin: reportdataexport[i].kelamin,
                    Umur: reportdataexport[i].umur,
                    Dokter: reportdataexport[i].nama_dok,
                    Spesialis: reportdataexport[i].nodok[0].specialist,
                    Pembayaran: reportdataexport[i].jaminan,
                    Diagnosa: reportdataexport[i].showDiagnosa,
                    Kasus: reportdataexport[i].kasus,
                    Kunjungan: reportdataexport[i].kunjungan,
                    Alamat: reportdataexport[i].no_medrec[0].address
                });
            }
            _this.nameOfDocument = 'Rekap Laporan Pasien';
        });
    };
    ReportComponent.prototype.formatReport = function (event) {
        var _this = this;
        var value = event.target.value;
        this.reportFormat = value;
        // let totalasuransi = 0;
        // let totalpribadi = 0;
        // let totalbpjs = 0;
        this.Asuransi = 0;
        this.Pribadi = 0;
        this.BPJS = 0;
        if (value === 'rekap') {
            this.show = 'all';
            this.loadData();
        }
        else if (value === 'pembayaran') {
            this.show = 'pembayaran';
            this.rmService.getDataReportBasicNodok(0, 10).subscribe(function (_a) {
                var data = _a.data;
                _this.dataReportExport = [];
                _this.nameOfDocument = 'Laporan Pasien Pembayaran';
                _this.nameOfDocumentDetailTime = '';
                _this.nameOfDocumentDetailPatient = '';
                _this.listpembayaran = [];
                var datareport = data.examineAllByNodok.slice();
                var reportdata = _this.parseDataJSON(datareport);
                var _loop_1 = function (i) {
                    _this.rmService.getDataPatientExamine(reportdata[i].nodok[0].nodok).subscribe(function (_a) {
                        var data = _a.data;
                        var parsperiksa = _this.parseDataJSON(data);
                        var datadoctor = [parsperiksa.examineByNodok];
                        var counterbpjs = 0;
                        var counterasuransi = 0;
                        var counterpribadi = 0;
                        var counter = datadoctor[0].length;
                        if (counter > 0) {
                            for (var j = 0; j < counter; j++) {
                                if (datadoctor[0][j].jaminan === "BPJS") {
                                    if (counterbpjs > 0) {
                                        counterbpjs++;
                                    }
                                    else {
                                        counterbpjs = 1;
                                    }
                                }
                                if (datadoctor[0][j].jaminan === "Asuransi") {
                                    if (counterasuransi > 0) {
                                        counterasuransi++;
                                    }
                                    else {
                                        counterasuransi = 1;
                                    }
                                }
                                if (datadoctor[0][j].jaminan === "Pribadi") {
                                    if (counterpribadi > 0) {
                                        counterpribadi++;
                                    }
                                    else {
                                        counterpribadi = 1;
                                    }
                                }
                            }
                            _this.Asuransi += counterasuransi;
                            _this.Pribadi += counterpribadi;
                            _this.BPJS += counterbpjs;
                            if (i === reportdata.length - 1) {
                                _this.chart = false;
                                _this.showChart();
                            }
                            _this.listpembayaran.push({
                                id: i,
                                nama_dok: reportdata[i].nama_dok,
                                pribadi: counterpribadi,
                                asuransi: counterasuransi,
                                bpjs: counterbpjs
                            });
                            _this.dataReportExport.push({
                                No: i + 1,
                                Nama_Dokter: reportdata[i].nama_dok,
                                Pribadi: counterpribadi,
                                Asuransi: counterasuransi,
                                BPJS: counterbpjs
                            });
                        }
                    });
                };
                for (var i = 0; i < reportdata.length; i++) {
                    _loop_1(i);
                }
            });
        }
        else if (value === 'dokter') {
            this.show = 'dokter';
            this.dataReportExport = [];
            this.nameOfDocument = 'Laporan Pasien Per Dokter';
            this.nameOfDocumentDetailTime = '';
            this.nameOfDocumentDetailPatient = '';
            this.listdoctor = [
                { dokter: 'Anasry dr SpA', spesialis: 'Anak', januari: '27', februari: '29', maret: '12', april: '0', mei: '0', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '68' },
                { dokter: 'Haris dr SpA', spesialis: 'Anak', januari: '346', februari: '277', maret: '379', april: '341', mei: '345', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '1688' },
                { dokter: 'Rusdi dr SpB', spesialis: 'Bedah', januari: '130', februari: '116', maret: '264', april: '95', mei: '88', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '693' },
                { dokter: 'Jansu dr SpB', spesialis: 'Bedah', januari: '275', februari: '248', maret: '148', april: '276', mei: '300', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '1247' },
                { dokter: 'Irman dr SpPD', spesialis: 'Dalam', januari: '36', februari: '20', maret: '41', april: '56', mei: '14', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '167' },
                { dokter: 'Rusli dr SpPD', spesialis: 'Dalam', januari: '1021', februari: '955', maret: '1075', april: '816', mei: '862', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '4729' },
                { dokter: 'Ande AmF', spesialis: 'Fisioterapi', januari: '66', februari: '54', maret: '60', april: '46', mei: '65', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '291' },
                { dokter: 'Rusti AmF', spesialis: 'Fisioterapi', januari: '37', februari: '14', maret: '22', april: '14', mei: '28', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '115' },
                { dokter: 'Hanifah drg', spesialis: 'Gizi', januari: '', februari: '', maret: '11', april: '12', mei: '9', juni: '', juli: '', agustus: '', september: '', oktober: '', november: '', desember: '', jumlah: '32' },
            ];
            var reportdataexport = this.listdoctor;
            for (var i = 0; i < reportdataexport.length; i++) {
                this.dataReportExport.push({
                    No: i + 1,
                    Nama_Dokter: reportdataexport[i].dokter,
                    Spesialis: reportdataexport[i].spesialis,
                    Januari: reportdataexport[i].januari,
                    Februari: reportdataexport[i].februari,
                    Maret: reportdataexport[i].maret,
                    April: reportdataexport[i].april,
                    Mei: reportdataexport[i].mei,
                    Juni: reportdataexport[i].juni,
                    Juli: reportdataexport[i].juli,
                    Agustus: reportdataexport[i].agustus,
                    September: reportdataexport[i].september,
                    Oktober: reportdataexport[i].oktober,
                    November: reportdataexport[i].november,
                    Desember: reportdataexport[i].desember,
                    Jumlah: reportdataexport[i].jumlah
                });
            }
        }
        else if (value === 'batal') {
            this.show = 'batal';
            this.dataReportExport = [];
            this.nameOfDocument = 'Laporan Jumlah Pasien Batal';
            this.nameOfDocumentDetailTime = '';
            this.nameOfDocumentDetailPatient = '';
            this.listbatal = [
                { tanggal: '12/07/2017', name: 'Jajang Jaenudin', norm: '324663', dokter: 'Dr. Asep Surasep', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Raka Maheka', norm: '234332', dokter: 'Dr. Koesman', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Asep Soejali', norm: '234253', dokter: 'Dr. Maman S', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Saepudin Koesnandar', norm: '565466', dokter: 'Dr. Soekismo Widjaya', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Winaldi Saputra', norm: '324663', dokter: 'Dr. Rozak Muzakir', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Yusuf Ramadhan', norm: '234332', dokter: 'Dr. Abdul Syarief', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Enjang Kosasih', norm: '234253', dokter: 'Dr. Jaenudin', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' },
                { tanggal: '12/07/2017', name: 'Ety Supriati', norm: '565466', dokter: 'Dr. Yoseph', spesialis: 'Ahli Bedah', pemabayaran: 'BPJS' }
            ];
            var reportdataexport = this.listbatal;
            for (var i = 0; i < reportdataexport.length; i++) {
                this.dataReportExport.push({
                    No: i + 1,
                    Tanggal: reportdataexport[i].tanggal,
                    Nama: reportdataexport[i].name,
                    No_RM: reportdataexport[i].norm,
                    Nama_Dokter: reportdataexport[i].dokter,
                    Spesialis: reportdataexport[i].spesialis,
                    Pembayaran: reportdataexport[i].pemabayaran
                });
            }
        }
        else if (value === 'rujuk') {
            this.show = 'rujuk';
            this.dataReportExport = [];
            this.nameOfDocument = 'Laporan Pasien Dirujuk';
            this.nameOfDocumentDetailTime = '';
            this.nameOfDocumentDetailPatient = '';
            this.listrujuk = [
                { tanggal: '12/07/2017', name: 'Jajang Jaenudin', norm: '324663', dokter: 'Dr. Asep Surasep', spesialis: 'Ahli Bedah', diagnosa: 'Sakit Perut', rujuk: 'RSCM', rawatinap: 'Rawat' },
                { tanggal: '12/07/2017', name: 'Raka Maheka', norm: '234332', dokter: 'Dr. Koesman', spesialis: 'Ahli Bedah', diagnosa: 'Usus', rujuk: 'RSCM', rawatinap: 'Rawat' },
                { tanggal: '12/07/2017', name: 'Asep Soejali', norm: '234253', dokter: 'Dr. Maman S', spesialis: 'Ahli Bedah', diagnosa: 'Jantung', rujuk: 'RSCM', rawatinap: 'Rawat' },
                { tanggal: '12/07/2017', name: 'Saepudin Koesnandar', norm: '565466', dokter: 'Dr. Soekismo Widjaya', spesialis: 'Ahli Bedah', diagnosa: 'Sakit Perut', rujuk: 'RSCM', rawatinap: 'Rawat' }
            ];
            var reportdataexport = this.listrujuk;
            for (var i = 0; i < reportdataexport.length; i++) {
                this.dataReportExport.push({
                    No: i + 1,
                    Tanggal: reportdataexport[i].tanggal,
                    Nama: reportdataexport[i].name,
                    No_RM: reportdataexport[i].norm,
                    Nama_Dokter: reportdataexport[i].dokter,
                    Spesialis: reportdataexport[i].spesialis,
                    Diagnosa: reportdataexport[i].diagnosa,
                    Rujuk: reportdataexport[i].rujuk,
                    Rawat_Inap: reportdataexport[i].rawatinap
                });
            }
        }
        else if (value === 'penyakit') {
            this.show = 'penyakit';
            this.dataReportExport = [];
            this.nameOfDocument = 'Laporan 10 Besar Penyakit';
            this.nameOfDocumentDetailTime = '';
            this.nameOfDocumentDetailPatient = '';
            this.listpenyakit = [
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '1', icd10: 'J06.9', desc: 'ispa', kasuslk: '9', kasuspr: '5', jumlahkasus: '14', jumlahkunjungan: '128' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '2', icd10: 'I99', desc: 'cva', kasuslk: '2', kasuspr: '0', jumlahkasus: '2', jumlahkunjungan: '135' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '3', icd10: 'H26.9', desc: 'kat imatur', kasuslk: '3', kasuspr: '5', jumlahkasus: '8', jumlahkunjungan: '135' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '4', icd10: 'M19.9', desc: 'oa', kasuslk: '2', kasuspr: '3', jumlahkasus: '5', jumlahkunjungan: '155' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '5', icd10: 'M54.5', desc: 'lbp', kasuslk: '1', kasuspr: '6', jumlahkasus: '7', jumlahkunjungan: '162' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '6', icd10: 'K30', desc: 'dyspepsia', kasuslk: '1', kasuspr: '6', jumlahkasus: '7', jumlahkunjungan: '164' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '7', icd10: 'A15.1', desc: 'kp', kasuslk: '0', kasuspr: '1', jumlahkasus: '1', jumlahkunjungan: '178' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '8', icd10: 'I11.9', desc: 'hhd', kasuslk: '2', kasuspr: '1', jumlahkasus: '3', jumlahkunjungan: '186' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '9', icd10: 'I10', desc: 'ht', kasuslk: '1', kasuspr: '1', jumlahkasus: '2', jumlahkunjungan: '238' },
                { namars: 'RSU MARY CH', bulan: 'MEI', tahun: '2017', nourut: '10', icd10: 'A16.2', desc: 'tb paru', kasuslk: '4', kasuspr: '2', jumlahkasus: '6', jumlahkunjungan: '455' }
            ];
            var reportdataexport = this.listpenyakit;
            for (var i = 0; i < reportdataexport.length; i++) {
                this.dataReportExport.push({
                    No: i + 1,
                    Nama_RS: reportdataexport[i].namars,
                    Bulan: reportdataexport[i].bulan,
                    Tahun: reportdataexport[i].tahun,
                    No_Urut: reportdataexport[i].nourut,
                    Kode_ICD_10: reportdataexport[i].icd10,
                    Deskripsi: reportdataexport[i].desc,
                    Kasus_Baru_Lakilaki: reportdataexport[i].kasuslk,
                    Kasus_Baru_Perempuan: reportdataexport[i].kasuspr,
                    Jumlah_Kasus_Baru: reportdataexport[i].jumlahkasus,
                    Jumlah_Kunjungan: reportdataexport[i].jumlahkunjungan
                });
            }
        }
    };
    ReportComponent.prototype.openModal = function (val) {
        this.stylus = [false];
        this.open = true;
        this.detailPatient = [val];
        // let nomedrec = this.detailPatient[0].no_medrec[0].no_medrec;
    };
    ReportComponent.prototype.showChart = function () {
        this.chart = !this.chart;
        this.chartOptions = { responsive: true };
        this.chartData = [{ data: [this.Asuransi, this.Pribadi, this.BPJS] }];
        this.chartLabels = ['Pribadi', 'Asuransi', 'BPJS'];
    };
    ReportComponent.prototype.onDateStartChanged = function (event) {
        var _this = this;
        if (event.date.year === 0 && event.date.day === 0 && event.date.month === 0) {
            this.rmService.getDataReportByDateRange(this.dateStart, this.dateEnd).subscribe(function (_a) {
                // this.dataReport = [];
                // let datareport = [...data.examineJoinByDateRange];
                // let reportdata = JSON.stringify(datareport);
                // let pars = JSON.parse(reportdata);
                // for (let i = 0; i < pars.length; i++) {
                //     pars[i]["index"] = i;
                //     this.dataReport.push(pars[i]);
                // }
                if (_this.reportFormat === 'rekap') {
                    _this.loadData();
                }
            });
        }
    };
    ReportComponent.prototype.onDateEndChanged = function (event) {
        var _this = this;
        if (event.date.year === 0 && event.date.day === 0 && event.date.month === 0) {
            this.rmService.getDataReportByDateRange(this.dateStart, this.dateEnd).subscribe(function (_a) {
                var data = _a.data;
                _this.nameOfDocumentDetailTime = '';
                _this.dataReport = [];
                var datareport = data.examineJoinByDateRange.slice();
                var reportdata = JSON.stringify(datareport);
                var pars = JSON.parse(reportdata);
                for (var i = 0; i < pars.length; i++) {
                    pars[i]["index"] = i;
                    _this.dataReport.push(pars[i]);
                }
            });
        }
    };
    ReportComponent.prototype.dateRange = function () {
        var _this = this;
        if (this.startdate !== '' && this.enddate !== '') {
            var yearstart = this.startdate.date.year;
            var monthstart = this.startdate.date.month;
            var daystart = this.startdate.date.day;
            if (monthstart < 10) {
                monthstart = '0' + monthstart;
            }
            if (daystart < 10) {
                daystart = '0' + daystart;
            }
            var datestart_1 = yearstart + "-" + monthstart + "-" + daystart;
            var yearend = this.enddate.date.year;
            var monthend = this.enddate.date.month;
            var dayend = this.enddate.date.day;
            if (monthend < 10) {
                monthend = '0' + monthend;
            }
            if (dayend < 10) {
                dayend = '0' + dayend;
            }
            var dateend_1 = yearend + "-" + monthend + "-" + dayend;
            this.rmService.getDataReportByDateRange(datestart_1, dateend_1).subscribe(function (_a) {
                var data = _a.data;
                _this.dataReport = [];
                var datareport = data.examineJoinByDateRange.slice();
                var reportdata = JSON.stringify(datareport);
                var pars = JSON.parse(reportdata);
                for (var i = 0; i < pars.length; i++) {
                    pars[i]["index"] = i;
                    _this.dataReport.push(pars[i]);
                }
                _this.dataReportExport = [];
                var datareportexport = data.examineJoinByDateRange.slice();
                var reportdataexport = _this.parseDataJSON(datareportexport);
                for (var i = 0; i < reportdataexport.length; i++) {
                    if (reportdataexport[i]["diagnosa"] !== "") {
                        reportdataexport[i]["showDiagnosa"] = "...";
                    }
                    else {
                        if (reportdataexport[i]["diagnosa_stroke"] != null) {
                            reportdataexport[i]["showDiagnosa"] = "(Stylus)";
                        }
                        else {
                            reportdataexport[i]["showDiagnosa"] = " ";
                        }
                    }
                    _this.dataReportExport.push({
                        No: i + 1,
                        Tanggal: reportdataexport[i].tgl,
                        Nama_Pasien: reportdataexport[i].nama_pasien,
                        No_RM: reportdataexport[i].no_medrec[0].no_medrec,
                        Jenis_Kelamin: reportdataexport[i].kelamin,
                        Umur: reportdataexport[i].umur,
                        Dokter: reportdataexport[i].nama_dok,
                        Spesialis: reportdataexport[i].nodok[0].specialist,
                        Pembayaran: reportdataexport[i].jaminan,
                        Diagnosa: reportdataexport[i].showDiagnosa,
                        Kasus: reportdataexport[i].kasus,
                        Kunjungan: reportdataexport[i].kunjungan,
                        Alamat: reportdataexport[i].no_medrec[0].address
                    });
                }
                _this.nameOfDocument = 'Rekap Laporan Pasien';
                _this.nameOfDocumentDetailTime = datestart_1 + ' - ' + dateend_1;
            });
        }
        else {
        }
    };
    ReportComponent.prototype.patientStatus = function ($event) {
        var _this = this;
        var kunjungan = $event.target.value;
        if (kunjungan === "0") {
            this.loadData();
        }
        else {
            this.rmService.getDataReportByVisit(kunjungan, 0, 10).subscribe(function (_a) {
                var data = _a.data;
                _this.dataReport = [];
                var datareport = data.examineJoinByVisit.slice();
                var reportdata = _this.parseDataJSON(datareport);
                for (var i = 0; i < reportdata.length; i++) {
                    reportdata[i]["index"] = i;
                    if (reportdata[i]["diagnosa"] !== "") {
                        reportdata[i]["showDiagnosa"] = "...";
                    }
                    else {
                        if (reportdata[i]["diagnosa_stroke"] != null) {
                            reportdata[i]["showDiagnosa"] = "(Stylus)";
                        }
                        else {
                            reportdata[i]["showDiagnosa"] = " ";
                        }
                    }
                    _this.dataReport.push(reportdata[i]);
                }
                _this.dataReportExport = [];
                var datareportexport = data.examineJoinByVisit.slice();
                var reportdataexport = _this.parseDataJSON(datareportexport);
                for (var i = 0; i < reportdataexport.length; i++) {
                    if (reportdataexport[i]["diagnosa"] !== "") {
                        reportdataexport[i]["showDiagnosa"] = "...";
                    }
                    else {
                        if (reportdataexport[i]["diagnosa_stroke"] != null) {
                            reportdataexport[i]["showDiagnosa"] = "(Stylus)";
                        }
                        else {
                            reportdataexport[i]["showDiagnosa"] = " ";
                        }
                    }
                    _this.dataReportExport.push({
                        No: i + 1,
                        Tanggal: reportdataexport[i].tgl,
                        Nama_Pasien: reportdataexport[i].nama_pasien,
                        No_RM: reportdataexport[i].no_medrec[0].no_medrec,
                        Jenis_Kelamin: reportdataexport[i].kelamin,
                        Umur: reportdataexport[i].umur,
                        Dokter: reportdataexport[i].nama_dok,
                        Spesialis: reportdataexport[i].nodok[0].specialist,
                        Pembayaran: reportdataexport[i].jaminan,
                        Diagnosa: reportdataexport[i].showDiagnosa,
                        Kasus: reportdataexport[i].kasus,
                        Kunjungan: reportdataexport[i].kunjungan,
                        Alamat: reportdataexport[i].no_medrec[0].address
                    });
                }
                _this.nameOfDocument = 'Rekap Laporan Pasien';
                _this.nameOfDocumentDetailPatient = kunjungan;
            });
        }
    };
    ReportComponent.prototype.parseDataJSON = function (data) {
        var strData = JSON.stringify(data);
        var parsData = JSON.parse(strData);
        return parsData;
    };
    // Export to Excel
    ReportComponent.prototype.exportAsExcelFile = function (excelFileName) {
        var worksheet = XLSX.utils.json_to_sheet(this.dataReportExport);
        this.wrapAndCenterCell(worksheet.B2);
        var workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
        var excelBuffer = XLSXStyle.write(workbook, { bookType: 'xlsx', type: 'buffer' });
        this.saveAsExcelFile(excelBuffer, excelFileName);
    };
    ReportComponent.prototype.wrapAndCenterCell = function (cell) {
        var wrapAndCenterCellStyle = { alignment: { wrapText: true, vertical: 'center', horizontal: 'center' } };
        this.setCellStyle(cell, wrapAndCenterCellStyle);
    };
    ReportComponent.prototype.setCellStyle = function (cell, style) {
        cell.s = style;
    };
    ReportComponent.prototype.saveAsExcelFile = function (buffer, fileName) {
        var data = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        FileSaver.saveAs(data, fileName + '[' + new Date().toLocaleString() + ']' + EXCEL_EXTENSION);
    };
    ReportComponent.prototype.exportExcel = function () {
        this.exportAsExcelFile(this.nameOfDocument);
    };
    // End of Export to Excel
    // Export to PDF
    ReportComponent.prototype.buildTableBody = function (data, columns, headers) {
        var body = [];
        body.push(headers);
        data.forEach(function (row) {
            var dataRow = [];
            columns.forEach(function (element) {
                if (element) {
                    dataRow.push(row[element]);
                }
            });
            body.push(dataRow);
        });
        return body;
    };
    ReportComponent.prototype.table = function (data, columns, headers, width) {
        return {
            table: {
                headerRows: 1,
                widths: width,
                body: this.buildTableBody(data, columns, headers)
            }
        };
    };
    ReportComponent.prototype.encodeLogoImage = function () {
        var element = document.getElementById('logo');
        var image = new Image();
        image = element;
        var canvas = document.createElement("canvas");
        canvas.width = image.width;
        canvas.height = image.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(image, 0, 0, image.width, image.height);
        var dataURL = canvas.toDataURL();
        return dataURL;
    };
    ReportComponent.prototype.exportPDF = function (value) {
        var rowHeader = [];
        var rowHeaderPDF = [];
        var width = [];
        if (value === 'rekap') {
            rowHeader = [
                'No',
                'Tanggal',
                'Nama_Pasien',
                'No_RM',
                'Jenis_Kelamin',
                'Umur',
                'Dokter',
                'Spesialis',
                'Pembayaran',
                'Diagnosa',
                'Kasus',
                'Kunjungan',
                'Alamat'
            ];
            rowHeaderPDF = [
                'No',
                'Tanggal',
                'Nama Pasien',
                'No RM',
                'Jenis Kelamin',
                'Umur',
                'Dokter',
                'Spesialis',
                'Pembayaran',
                'Diagnosa',
                'Kasus',
                'Kunjungan',
                'Alamat'
            ];
        }
        else if (value === 'pembayaran') {
            rowHeader = [
                'No',
                'Nama_Dokter',
                'Pribadi',
                'Asuransi',
                'BPJS'
            ];
            rowHeaderPDF = [
                'No',
                'Nama Dokter',
                'Pribadi',
                'Asuransi',
                'BPJS'
            ];
        }
        else if (value === 'dokter') {
            rowHeader = [
                'No',
                'Nama_Dokter',
                'Spesialis',
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
                'Jumlah'
            ];
            rowHeaderPDF = [
                'No',
                'Nama Dokter',
                'Spesialis',
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
                'Jumlah'
            ];
        }
        else if (value === 'batal') {
            rowHeader = [
                'No',
                'Tanggal',
                'Nama',
                'No_RM',
                'Nama_Dokter',
                'Spesialis',
                'Pembayaran'
            ];
            rowHeaderPDF = [
                'No',
                'Tanggal',
                'Nama',
                'No. RM',
                'Nama Dokter',
                'Spesialis',
                'Pembayaran'
            ];
        }
        else if (value === 'rujuk') {
            rowHeader = [
                'No',
                'Tanggal',
                'Nama',
                'No_RM',
                'Nama_Dokter',
                'Spesialis',
                'Diagnosa',
                'Rujuk',
                'Rawat_Inap'
            ];
            rowHeaderPDF = [
                'No',
                'Tanggal',
                'Nama',
                'No. RM',
                'Nama Dokter',
                'Spesialis',
                'Diagnosa',
                'Rujuk',
                'Rawat Inap'
            ];
        }
        else if (value === 'penyakit') {
            rowHeader = [
                'No',
                'Nama_RS',
                'Bulan',
                'Tahun',
                'No_Urut',
                'Kode_ICD_10',
                'Deskripsi',
                'Kasus_Baru_Lakilaki',
                'Kasus_Baru_Perempuan',
                'Jumlah_Kasus_Baru',
                'Jumlah_Kunjungan'
            ];
            rowHeaderPDF = [
                'No',
                'Nama RS',
                'Bulan',
                'Tahun',
                'No. Urut',
                'Kode ICD 10',
                'Deskripsi',
                'Kasus Baru (Laki-laki)',
                'Kasus Baru (Perempuan)',
                'Jumlah Kasus Baru',
                'Jumlah Kunjungan'
            ];
        }
        else {
        }
        for (var i = 0; i < rowHeader.length; i++) {
            if (i === 1) {
                width.push('*');
            }
            else {
                width.push('auto');
            }
        }
        this.docDefinition = {
            pageSize: 'A4',
            pageOrientation: 'landscape',
            pageMargins: [20, 20, 20, 20],
            content: [
                { image: this.encodeLogoImage(), alignment: 'center' },
                { text: "RUMAH SAKIT", style: 'header', alignment: 'center', fontSize: 22 },
                { text: "MARY CILEUNGSI", style: 'header', alignment: 'center', fontSize: 22 },
                { text: "Jl. Raya Narogong KM 21, Cileungsi, Kabupaten - Bogor, 16820", style: 'header', alignment: 'center' },
                { text: "Telp: (021) 8249222", style: 'header', alignment: 'center' },
                { text: " ", style: 'header', alignment: 'center', fontSize: 22 },
                {
                    columns: [
                        {
                            width: 100,
                            text: 'Subjek'
                        },
                        {
                            text: ": " + this.nameOfDocument
                        },
                    ],
                    columnGap: 10
                },
                {
                    columns: [
                        {
                            width: 100,
                            text: 'Tanggal'
                        },
                        {
                            text: ": " + this.nameOfDocumentDetailTime
                        },
                    ],
                    columnGap: 10
                },
                {
                    columns: [
                        {
                            width: 100,
                            text: 'Status Pasien'
                        },
                        {
                            text: ": " + this.nameOfDocumentDetailPatient
                        },
                    ],
                    columnGap: 10
                },
                { text: " ", style: 'header', alignment: 'center', fontSize: 15 },
                this.table(this.dataReportExport, rowHeader, rowHeaderPDF, width),
                { text: " ", style: 'header', alignment: 'center', fontSize: 15 },
                { text: new Date().toLocaleString(), alignment: 'right' },
            ]
        };
        pdfMake.createPdf(this.docDefinition).download(this.nameOfDocument);
    };
    // End of Export to Excel
    ReportComponent.prototype.formatDoc = function (event) {
        this.docFormat = event.target.value;
    };
    ReportComponent.prototype.exportDoc = function () {
        if (this.docFormat === 'pdf') {
            this.exportPDF(this.reportFormat);
        }
        else if (this.docFormat === 'xlsx') {
            this.exportExcel();
        }
    };
    ReportComponent.prototype.drawcanvas = function (index, strokedata, status) {
        // this.components = [];
        this.stylus[index] = !this.stylus[index];
        var x = [];
        var y = [];
        var t = [];
        if (strokedata !== null) {
            var datastroke = JSON.parse(strokedata);
            x = x.concat(datastroke[0]);
            y = y.concat(datastroke[1]);
            t = t.concat(datastroke[2]);
            x = x.concat(null);
            y = y.concat(null);
            t = t.concat(null);
            this.element = document.getElementById(index);
            var imageRenderingCanvas = this.element;
            var ctx = imageRenderingCanvas.getContext('2d');
            ctx.clearRect(0, 0, this.width, this.height);
            // this.inkGrabber = new InkGrabber(ctx);
            // this.inkGrabber.startCapture(x[0], y[0], t[0]);
            for (var i = 1; i < x.length - 1; i++) {
                if (x[i] == null) {
                    // this.inkGrabber.endCapture(x[i - 1], y[i - 1], t[i - 1]);
                    // let stroke = this.inkGrabber.getStroke();
                    // this.components.push(stroke);
                    // this.inkGrabber = new InkGrabber(ctx);
                    // this.inkGrabber.startCapture(x[i + 1], y[i + 1], t[i + 1]);
                }
                else {
                    // this.inkGrabber.continueCapture(x[i], y[i], t[i]);
                }
            }
            // this.inkGrabber.endCapture(x[x.length - 2], y[x.length - 2], t[x.length - 2]);
            // let stroke = this.inkGrabber.getStroke();
            // this.components.push(stroke);
            // this.inkGrabberFinal = new InkGrabber(ctx);
            // for (let i = 0; i < this.components.length; i++) {
            //     this.inkGrabberFinal.drawComponent(this.components[i]);
            // }
            var image = new Image();
            image.id = "pic" + index;
            var content = this.element;
            image.src = content.toDataURL();
            this.detailPatient[index]["image"] = image.src;
        }
        else {
            this.detailPatient[index]["image"] = "images/no_image.png";
        }
    };
    ReportComponent = __decorate([
        Component({
            styleUrls: ['./report.component.scss'],
            selector: 'report-page',
            templateUrl: 'report.component.html'
        }),
        __metadata("design:paramtypes", [RmserviceService])
    ], ReportComponent);
    return ReportComponent;
}());
export { ReportComponent };
//# sourceMappingURL=report.component.js.map