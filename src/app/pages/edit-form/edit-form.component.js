var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Router, ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormsService } from '../../core/providers/forms.service';
var EditFormComponent = /** @class */ (function () {
    function EditFormComponent(formsservice, router, route) {
        this.formsservice = formsservice;
        this.router = router;
        this.route = route;
        this.formName = 'Untitled Form';
        this.labelName = 'Untitled Label';
        this.formatText = "text";
        this.required = "false";
        this.readonly = "false";
        this.formArray = [];
        this.itemRadio = [];
        this.itemCheck = [];
        this.itemInput = [];
        this.itemSelect = [];
        this.tempitemCheck = [];
        this.tempitemInput = [];
        this.tempdataitemCheck = [];
        this.tempdataitemInput = [];
        this.showSeconds = false;
        this.no_form = '';
        this.form = new FormGroup({});
        this.model = {};
        this.fields = [];
        this.formField = [];
        this.tempField = [{ key: '', type: '', templateOptions: {} }];
        this.statusForm = false;
        this.onEditedElement = false;
        this.enableEditedElement = false;
        this.editCheckbox = false;
        this.editMultiInput = false;
        this.editRadioGroup = false;
        this.editSelectElement = false;
    }
    EditFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.tempField = [];
        if (this.formField.length === 0) {
            this.statusForm = true;
        }
        else {
            this.statusForm = false;
        }
        this.sub = this.route
            .params
            .subscribe(function (params) {
            if (params) {
                _this.no_form = params['id'];
                if (_this.no_form) {
                    _this.updateForm = _this.formsservice.getFormById(_this.no_form).subscribe(function (_a) {
                        var data = _a.data;
                        _this.formField = JSON.parse(data.formById.json_form);
                        _this.tempField = JSON.parse(data.formById.json_form);
                        _this.fields = JSON.parse(data.formById.json_form);
                        _this.formName = data.formById.name_form;
                        for (var i = 0; i < _this.formField.length; i++) {
                            if (_this.formField[i].fieldGroup) {
                                _this.tempitemInput = [];
                                _this.tempdataitemInput = [];
                                _this.tempitemCheck = [];
                                _this.tempdataitemCheck = [];
                                for (var j = 0; j < _this.formField[i].fieldGroup.length; j++) {
                                    if (_this.formField[i].wrappers[0] === "multiinput") {
                                        _this.tempitemInput.push({
                                            key: _this.formField[i].fieldGroup[j].key,
                                            type: 'input',
                                            templateOptions: {
                                                type: _this.formField[i].fieldGroup[j].templateOptions.type,
                                                label: _this.formField[i].fieldGroup[j].templateOptions.label,
                                                placeholder: _this.formField[i].fieldGroup[j].templateOptions.placeholder,
                                                readonly: _this.formField[i].fieldGroup[j].templateOptions.readonly
                                            }
                                        });
                                        _this.tempdataitemInput.push({
                                            key: _this.formField[i].fieldGroup[j].key,
                                            type: 'input',
                                            templateOptions: {
                                                type: _this.formField[i].fieldGroup[j].templateOptions.type,
                                                label: _this.formField[i].fieldGroup[j].templateOptions.label,
                                                placeholder: _this.formField[i].fieldGroup[j].templateOptions.placeholder,
                                                readonly: _this.formField[i].fieldGroup[j].templateOptions.readonly
                                            }
                                        });
                                    }
                                    else if (_this.formField[i].wrappers[0] === "multicheckbox") {
                                        _this.tempitemCheck.push({
                                            key: _this.formField[i].fieldGroup[j].key,
                                            type: 'checkbox',
                                            templateOptions: {
                                                label: _this.formField[i].fieldGroup[j].templateOptions.label,
                                                value: _this.formField[i].fieldGroup[j].templateOptions.value
                                            }
                                        });
                                        _this.tempdataitemCheck.push({
                                            key: _this.formField[i].fieldGroup[j].key,
                                            type: 'checkbox',
                                            templateOptions: {
                                                label: _this.formField[i].fieldGroup[j].templateOptions.label,
                                                value: _this.formField[i].fieldGroup[j].templateOptions.value
                                            }
                                        });
                                    }
                                }
                                // if (this.formField[i].wrappers[0] === "multiinput") {
                                //   this.tempField.push({
                                //     key: this.formField[i].key,
                                //     type: this.formField[i].type,
                                //     wrappers: this.formField[i].wrappers,
                                //     templateOptions: this.formField[i].templateOptions,
                                //     fieldGroup: this.tempdataitemInput
                                //   });
                                // } else if (this.formField[i].wrappers[0] === "multicheckbox") {
                                //   this.tempField.push({
                                //     key: this.formField[i].key,
                                //     type: this.formField[i].type,
                                //     wrappers: this.formField[i].wrappers,
                                //     templateOptions: this.formField[i].templateOptions,
                                //     fieldGroup: this.tempdataitemCheck
                                //   });
                                // }
                            }
                        }
                        console.log(_this.tempitemInput);
                        console.log(_this.tempdataitemInput);
                        console.log(_this.tempitemCheck);
                        console.log(_this.tempdataitemCheck);
                    });
                }
            }
            else {
            }
        });
    };
    EditFormComponent.prototype.randomID = function () {
        var id = this.labelName.toLowerCase();
        var words = id.split(" ");
        var word = "";
        var len = words.length;
        if (len > 4) {
            len = 4;
        }
        for (var i = 0; i < len; i++) {
            if (i === (len - 1)) {
                word += words[i];
            }
            else {
                word += words[i] + " ";
            }
        }
        this.idForm = word.replace(/ /g, "_");
    };
    EditFormComponent.prototype.generateTemplate = function (option) {
        if (option = "Yes-No") {
            this.itemRadio.push({ 'value': 'Tidak', 'key': 'tidak' }, { 'value': 'Ya', 'key': 'ya' });
        }
    };
    EditFormComponent.prototype.generateValue = function () {
        this.valueItem = this.labelItem.toLowerCase();
    };
    EditFormComponent.prototype.selectAllContent = function ($event) {
        $event.target.select();
    };
    EditFormComponent.prototype.onSelectElementNameChange = function ($event) {
        if (!this.onEditedElement) {
            if ($event.target.value === "multiinput") {
                this.itemInput = [];
                this.tempitemInput = [];
                this.tempdataitemInput = [];
            }
            else if ($event.target.value === "checkboxgroup") {
                this.itemCheck = [];
                this.tempitemCheck = [];
                this.tempdataitemCheck = [];
            }
            else if ($event.target.value === "label") {
                this.formatText = "normal";
            }
            else if ($event.target.value === "input") {
                this.formatText = "text";
            }
            console.log($event.target.value);
        }
    };
    EditFormComponent.prototype.showElement = function () {
        this.fields = this.formField;
    };
    EditFormComponent.prototype.saveElement = function () {
        if (this.maxLength == null) {
            this.maxLength = 90;
        }
        if (this.idForm == null) {
            this.idForm = Math.random().toString(36).substring(7);
        }
        switch (this.elementName) {
            case 'label':
                if (this.formatText === "header") {
                    this.formField.push({
                        key: Math.random().toString(36).substring(7),
                        type: 'header-text',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                    this.tempField.push({
                        key: Math.random().toString(36).substring(7),
                        type: 'header-text',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                else if (this.formatText === "bold") {
                    this.formField.push({
                        key: Math.random().toString(36).substring(7),
                        type: 'label-bold',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                    this.tempField.push({
                        key: Math.random().toString(36).substring(7),
                        type: 'label-bold',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                else {
                    this.formField.push({
                        key: Math.random().toString(36).substring(7),
                        type: 'label',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                    this.tempField.push({
                        key: Math.random().toString(36).substring(7),
                        type: 'label',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                this.saveFields();
                break;
            case 'input':
                this.formField.push({
                    key: this.idForm,
                    type: 'input',
                    templateOptions: {
                        type: this.formatText,
                        label: this.labelName,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required),
                        readonly: JSON.parse(this.readonly)
                    }
                });
                this.tempField.push({
                    key: this.idForm,
                    type: 'input',
                    templateOptions: {
                        type: this.formatText,
                        label: this.labelName,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required),
                        readonly: JSON.parse(this.readonly)
                    }
                });
                this.saveFields();
                break;
            case 'multiinput':
                this.formField.push({
                    key: this.idForm,
                    wrappers: ['multiinput'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.itemInput
                });
                this.tempField.push({
                    key: this.idForm,
                    wrappers: ['multiinput'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.tempitemInput
                });
                this.saveFields();
                break;
            case 'checkbox':
                this.formField.push({
                    key: this.idForm,
                    type: 'checkbox',
                    templateOptions: {
                        label: this.labelName
                    }
                });
                this.tempField.push({
                    key: this.idForm,
                    type: 'checkbox',
                    templateOptions: {
                        label: this.labelName
                    }
                });
                this.saveFields();
                break;
            case 'checkboxgroup':
                this.formField.push({
                    key: this.idForm,
                    wrappers: ['multicheckbox'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.itemCheck
                });
                this.tempField.push({
                    key: this.idForm,
                    wrappers: ['multicheckbox'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.tempdataitemCheck
                });
                this.saveFields();
                break;
            case 'radio':
                this.formField.push({
                    key: this.idForm,
                    type: 'radio',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemRadio
                    }
                });
                this.tempField.push({
                    key: this.idForm,
                    type: 'radio',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemRadio
                    }
                });
                this.saveFields();
                break;
            case 'textarea':
                this.formField.push({
                    key: this.idForm,
                    type: 'textarea',
                    templateOptions: {
                        label: this.labelName,
                        rows: this.rows,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required)
                    }
                });
                this.tempField.push({
                    key: this.idForm,
                    type: 'textarea',
                    templateOptions: {
                        label: this.labelName,
                        rows: this.rows,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required)
                    }
                });
                this.saveFields();
                break;
            case 'select':
                this.formField.push({
                    key: this.idForm,
                    type: 'select',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemSelect
                    }
                });
                this.tempField.push({
                    key: this.idForm,
                    type: 'select',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemSelect
                    }
                });
                this.saveFields();
                break;
            case 'timepicker':
                this.formField.push({
                    key: this.idForm,
                    type: 'timepicker',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.tempField.push({
                    key: this.idForm,
                    type: 'timepicker',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.saveFields();
                break;
            case 'datepicker':
                this.formField.push({
                    key: this.idForm,
                    type: 'datetime',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.tempField.push({
                    key: this.idForm,
                    type: 'datetime',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.saveFields();
                break;
            case 'range':
                this.formField.push({
                    key: this.idForm,
                    type: 'range',
                    templateOptions: {
                        label: this.labelName,
                        rangeClass: "calm",
                        min: this.minRange,
                        max: this.maxRange,
                        step: this.stepRange,
                        value: this.minRange,
                    }
                });
                this.tempField.push({
                    key: this.idForm,
                    type: 'range',
                    templateOptions: {
                        label: this.labelName,
                        rangeClass: "calm",
                        min: this.minRange,
                        max: this.maxRange,
                        step: this.stepRange,
                        value: this.minRange,
                    }
                });
                this.saveFields();
                break;
        }
        this.labelName = 'Untitled Label';
        this.maxLength = null;
        this.placeholder = '';
        this.idForm = '';
        this.elementName = null;
        this.minRange = null;
        this.maxRange = null;
        this.stepRange = null;
    };
    EditFormComponent.prototype.saveEditedElement = function () {
        if (this.maxLength == null) {
            this.maxLength = 90;
        }
        if (this.idForm == null) {
            this.idForm = Math.random().toString(36).substring(7);
        }
        this.tempdataitemInput = [];
        for (var i = 0; i < this.itemInput.length; i++) {
            this.tempdataitemInput.push({
                key: this.itemInput[i].key,
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: this.itemInput[i].templateOptions['label'],
                    placeholder: this.itemInput[i].templateOptions['placeholder'],
                    readonly: false
                }
            });
        }
        this.tempdataitemCheck = [];
        for (var i = 0; i < this.itemCheck.length; i++) {
            this.tempdataitemCheck.push({
                key: this.itemCheck[i].key,
                type: 'checkbox',
                templateOptions: {
                    label: this.itemCheck[i].templateOptions['label'],
                    value: this.itemCheck[i].templateOptions['value']
                }
            });
        }
        switch (this.elementName) {
            case 'label':
                if (this.formatText === "header") {
                    this.formField.splice(this.selectedElement, 1, {
                        key: Math.random().toString(36).substring(7),
                        type: 'header-text',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                    this.tempField.splice(this.selectedElement, 1, {
                        key: Math.random().toString(36).substring(7),
                        type: 'header-text',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                else if (this.formatText === "bold") {
                    this.formField.splice(this.selectedElement, 1, {
                        key: Math.random().toString(36).substring(7),
                        type: 'label-bold',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                    this.tempField.splice(this.selectedElement, 1, {
                        key: Math.random().toString(36).substring(7),
                        type: 'label-bold',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                else {
                    this.formField.splice(this.selectedElement, 1, {
                        key: Math.random().toString(36).substring(7),
                        type: 'label',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                    this.tempField.splice(this.selectedElement, 1, {
                        key: Math.random().toString(36).substring(7),
                        type: 'label',
                        templateOptions: {
                            label: this.labelName
                        }
                    });
                }
                this.saveFields();
                break;
            case 'input':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'input',
                    templateOptions: {
                        type: this.formatText,
                        label: this.labelName,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required),
                        readonly: JSON.parse(this.readonly)
                    }
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'input',
                    templateOptions: {
                        type: this.formatText,
                        label: this.labelName,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required),
                        readonly: JSON.parse(this.readonly)
                    }
                });
                this.saveFields();
                break;
            case 'multiinput':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    wrappers: ['multiinput'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.itemInput
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    wrappers: ['multiinput'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.tempdataitemInput
                });
                this.saveFields();
                break;
            case 'checkbox':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'checkbox',
                    templateOptions: {
                        label: this.labelName
                    }
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'checkbox',
                    templateOptions: {
                        label: this.labelName
                    }
                });
                this.saveFields();
                break;
            case 'checkboxgroup':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    wrappers: ['multicheckbox'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.itemCheck
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    wrappers: ['multicheckbox'],
                    templateOptions: { label: this.labelName },
                    fieldGroup: this.tempdataitemCheck
                });
                this.saveFields();
                break;
            case 'radio':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'radio',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemRadio
                    }
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'radio',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemRadio
                    }
                });
                this.saveFields();
                break;
            case 'textarea':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'textarea',
                    templateOptions: {
                        label: this.labelName,
                        rows: this.rows,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required)
                    }
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'textarea',
                    templateOptions: {
                        label: this.labelName,
                        rows: this.rows,
                        placeholder: this.placeholder,
                        required: JSON.parse(this.required)
                    }
                });
                this.saveFields();
                break;
            case 'select':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'select',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemSelect
                    }
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'select',
                    templateOptions: {
                        label: this.labelName,
                        options: this.itemSelect
                    }
                });
                this.saveFields();
                break;
            case 'timepicker':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'timepicker',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'timepicker',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.saveFields();
                break;
            case 'datepicker':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'datetime',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'datetime',
                    templateOptions: {
                        label: this.labelName,
                    }
                });
                this.saveFields();
                break;
            case 'range':
                this.formField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'range',
                    templateOptions: {
                        label: this.labelName,
                        rangeClass: "calm",
                        min: this.minRange,
                        max: this.maxRange,
                        step: this.stepRange,
                        value: this.minRange,
                    }
                });
                this.tempField.splice(this.selectedElement, 1, {
                    key: this.idForm,
                    type: 'range',
                    templateOptions: {
                        label: this.labelName,
                        rangeClass: "calm",
                        min: this.minRange,
                        max: this.maxRange,
                        step: this.stepRange,
                        value: this.minRange,
                    }
                });
                this.saveFields();
                break;
        }
        this.labelName = 'Untitled Label';
        this.maxLength = null;
        this.placeholder = '';
        this.idForm = '';
        this.elementName = null;
        this.minRange = null;
        this.maxRange = null;
        this.stepRange = null;
        this.onEditedElement = false;
    };
    EditFormComponent.prototype.cancelElement = function () {
        this.onEditedElement = false;
        this.labelName = 'Untitled Label';
        this.maxLength = null;
        this.placeholder = '';
        this.idForm = '';
        this.elementName = null;
        this.editMultiInput = false;
        this.editCheckbox = false;
        this.editMultiInput = false;
        this.editRadioGroup = false;
        this.itemInput = [];
        this.itemCheck = [];
        this.itemRadio = [];
        this.itemSelect = [];
        this.tempitemCheck = [];
        this.tempitemInput = [];
    };
    EditFormComponent.prototype.reorderElement = function () {
        this.enableEditedElement = true;
    };
    EditFormComponent.prototype.saveFields = function () {
        this.itemInput = [];
        this.itemCheck = [];
        this.itemRadio = [];
        this.itemSelect = [];
        this.tempitemCheck = [];
        this.tempitemInput = [];
        this.fields = [];
        for (var i = 0; i < this.formField.length; i++) {
            this.fields.push(this.formField[i]);
        }
        this.statusForm = false;
    };
    EditFormComponent.prototype.deleteItem = function (i, type) {
        switch (type) {
            case 'RADIO':
                this.itemRadio.splice(i, 1);
                break;
            case 'SELECT':
                this.itemSelect.splice(i, 1);
                break;
            case 'MULTIINPUT':
                this.itemInput.splice(i, 1);
                this.tempitemInput.splice(i, 1);
                this.tempdataitemInput.splice(i, 1);
                break;
            case 'CHECKBOX':
                this.itemCheck.splice(i, 1);
                this.tempitemCheck.splice(i, 1);
                this.tempdataitemCheck.splice(i, 1);
                break;
        }
    };
    EditFormComponent.prototype.editItem = function (i, type) {
        switch (type) {
            case 'RADIO':
                this.editRadioGroup = true;
                this.labelItem = this.itemRadio[i].value;
                this.valueItem = this.itemRadio[i].key;
                this.index = i;
                break;
            case 'SELECT':
                this.editSelectElement = true;
                this.labelItem = this.itemSelect[i].label;
                this.valueItem = this.itemSelect[i].value;
                this.index = i;
                break;
            case 'MULTIINPUT':
                this.editMultiInput = true;
                this.idItem = this.itemInput[i].key;
                this.labelItem = this.itemInput[i].templateOptions["label"];
                this.placeholderItem = this.itemInput[i].templateOptions["placeholder"];
                this.index = i;
                break;
            case 'CHECKBOX':
                this.editCheckbox = true;
                this.idItem = this.itemCheck[i].key;
                this.labelItem = this.itemCheck[i].templateOptions["label"];
                this.valueItem = this.itemCheck[i].templateOptions["value"];
                this.index = i;
                break;
        }
    };
    EditFormComponent.prototype.addInput = function () {
        this.itemInput.push({
            key: this.idItem,
            type: 'input',
            templateOptions: {
                type: 'text',
                label: this.labelItem,
                placeholder: this.placeholderItem,
                readonly: false
            }
        });
        this.tempitemInput.push({
            key: this.idItem,
            type: 'input',
            templateOptions: {
                type: 'text',
                label: this.labelItem,
                placeholder: this.placeholderItem,
                readonly: false
            }
        });
        this.tempdataitemInput.push({
            key: this.idItem,
            type: 'input',
            templateOptions: {
                type: 'text',
                label: this.labelItem,
                placeholder: this.placeholderItem,
                readonly: false
            }
        });
        this.idItem = '';
        this.labelItem = '';
        this.placeholderItem = '';
        this.maxLength = null;
    };
    EditFormComponent.prototype.editInput = function () {
        this.itemInput.splice(this.index, 1, {
            key: this.idItem,
            type: 'input',
            templateOptions: {
                type: 'text',
                label: this.labelItem,
                placeholder: this.placeholderItem,
                readonly: false
            }
        });
        this.tempitemInput.splice(this.index, 1, {
            key: this.idItem,
            type: 'input',
            templateOptions: {
                type: 'text',
                label: this.labelItem,
                placeholder: this.placeholderItem,
                readonly: false
            }
        });
        this.tempdataitemInput.splice(this.index, 1, {
            key: this.idItem,
            type: 'input',
            templateOptions: {
                type: 'text',
                label: this.labelItem,
                placeholder: this.placeholderItem,
                readonly: false
            }
        });
        this.idItem = '';
        this.labelItem = '';
        this.placeholderItem = '';
        this.maxLength = null;
        this.editMultiInput = false;
    };
    EditFormComponent.prototype.editRadio = function () {
        this.itemRadio.splice(this.index, 1, {
            'value': this.labelItem, 'key': this.valueItem
        });
        this.labelItem = '';
        this.valueItem = '';
        this.editRadioGroup = false;
    };
    EditFormComponent.prototype.addRadio = function () {
        this.itemRadio.push({ 'value': this.labelItem, 'key': this.valueItem });
        this.labelItem = '';
        this.valueItem = '';
    };
    EditFormComponent.prototype.addCheck = function () {
        this.itemCheck.push({
            key: this.idItem,
            type: 'checkbox',
            templateOptions: {
                label: this.labelItem
            }
        });
        this.tempitemCheck.push({
            key: this.idItem,
            type: 'checkbox',
            templateOptions: {
                label: this.labelItem
            }
        });
        this.tempdataitemCheck.push({
            key: this.idItem,
            type: 'checkbox',
            templateOptions: {
                label: this.labelItem
            }
        });
        this.idItem = '';
        this.labelItem = '';
        this.valueItem = '';
    };
    EditFormComponent.prototype.editCheck = function () {
        this.itemCheck.splice(this.index, 1, {
            key: this.idItem,
            type: 'checkbox',
            templateOptions: {
                label: this.labelItem
            }
        });
        this.tempitemCheck.splice(this.index, 1, {
            key: this.idItem,
            type: 'checkbox',
            templateOptions: {
                label: this.labelItem
            }
        });
        this.tempdataitemCheck.splice(this.index, 1, {
            key: this.idItem,
            type: 'checkbox',
            templateOptions: {
                label: this.labelItem
            }
        });
        this.idItem = '';
        this.labelItem = '';
        this.valueItem = '';
        this.editCheckbox = false;
    };
    EditFormComponent.prototype.editSelect = function () {
        this.itemSelect.splice(this.index, 1, {
            'label': this.labelItem, 'value': this.valueItem
        });
        this.labelItem = '';
        this.valueItem = '';
        this.editSelectElement = false;
    };
    EditFormComponent.prototype.addSelect = function () {
        this.itemSelect.push({ 'label': this.labelItem, 'value': this.valueItem });
        this.labelItem = '';
        this.valueItem = '';
    };
    EditFormComponent.prototype.editElement = function (index) {
        this.selectedElement = index;
        console.log(this.formField[index]);
        var element = this.formField[index];
        this.onEditedElement = true;
        if (element.type === 'header-text') {
            this.elementName = 'label';
            this.formatText = 'header';
            this.labelName = element.templateOptions.label;
        }
        else if (element.type === 'label-bold') {
            this.elementName = 'label';
            this.formatText = 'bold';
            this.labelName = element.templateOptions.label;
        }
        else if (element.type === 'label') {
            this.elementName = 'label';
            this.formatText = 'normal';
            this.labelName = element.templateOptions.label;
        }
        else if (element.type === 'input') {
            this.elementName = 'input';
            this.labelName = element.templateOptions.label;
            this.idForm = element.key;
            this.placeholder = element.templateOptions.placeholder;
            this.formatText = element.templateOptions.type;
            this.required = element.templateOptions.required;
            this.readonly = element.templateOptions.readonly;
        }
        else if (element.type === 'textarea') {
            this.elementName = 'textarea';
            this.labelName = element.templateOptions.label;
            this.idForm = element.key;
            this.rows = element.templateOptions.rows;
            this.placeholder = element.templateOptions.placeholder;
        }
        else if (element.type === 'checkbox') {
            this.elementName = 'checkbox';
            this.labelName = element.templateOptions.label;
            this.idForm = element.key;
        }
        else if (element.type === 'radio') {
            this.elementName = 'radio';
            this.labelName = element.templateOptions.label;
            this.idForm = element.key;
            this.itemRadio = element.templateOptions.options;
        }
        else if (element.type === 'select') {
            this.elementName = 'select';
            this.labelName = element.templateOptions.label;
            this.idForm = element.key;
            this.itemSelect = element.templateOptions.options;
        }
        else if (element.type === 'datetime') {
            this.elementName = 'datepicker';
            this.labelName = element.templateOptions.label;
            this.idForm = element.key;
        }
        else if (element.type === 'timepicker') {
            this.elementName = 'timepicker';
            this.labelName = element.templateOptions.label;
            this.idForm = element.key;
        }
        else if (element.type === 'range') {
            this.elementName = 'range';
            this.labelName = element.templateOptions.label;
            this.idForm = element.key;
            this.minRange = element.templateOptions.min;
            this.maxRange = element.templateOptions.max;
            this.stepRange = element.templateOptions.step;
        }
        else {
            if (element.wrappers[0] === "multiinput") {
                this.elementName = 'multiinput';
                this.labelName = element.templateOptions.label;
                this.idForm = element.key;
                this.itemInput = [];
                this.tempitemInput = [];
                for (var i = 0; i < element.fieldGroup.length; i++) {
                    this.itemInput.push(element.fieldGroup[i]);
                    this.tempitemInput.push(element.fieldGroup[i]);
                }
            }
            else if (element.wrappers[0] === "multicheckbox") {
                this.elementName = 'checkboxgroup';
                this.labelName = element.templateOptions.label;
                this.idForm = element.key;
                this.itemCheck = [];
                this.tempitemCheck = [];
                this.tempdataitemCheck = [];
                for (var i = 0; i < element.fieldGroup.length; i++) {
                    this.itemCheck.push(element.fieldGroup[i]);
                    this.tempitemCheck.push(element.fieldGroup[i]);
                    this.tempdataitemCheck.push(element.fieldGroup[i]);
                }
                console.log(this.itemCheck);
                console.log(this.tempitemCheck);
                console.log(this.tempdataitemCheck);
            }
        }
    };
    EditFormComponent.prototype.deleteElement = function (index) {
        console.log(index);
        console.log(this.formField);
        console.log(this.tempdataitemInput);
        this.formField.splice(index, 1);
        this.tempField.splice(index, 1);
        this.fields = [];
        console.log(this.formField);
        console.log(this.tempField);
        for (var i = 0; i < this.formField.length; i++) {
            this.fields.push(this.formField[i]);
        }
    };
    EditFormComponent.prototype.moveElement = function (index, dir) {
        if ((index === 0 && dir === -1) || (index === (this.formField.length - 1) && dir === +1)) {
        }
        else {
            this.array_move(this.formField, index, index + dir);
            this.array_move(this.tempField, index, index + dir);
            this.saveFields();
        }
    };
    EditFormComponent.prototype.array_move = function (arr, old_index, new_index) {
        while (old_index < 0) {
            old_index += arr.length;
        }
        while (new_index < 0) {
            new_index += arr.length;
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr;
    };
    EditFormComponent.prototype.editForm = function () {
        var _this = this;
        console.log(this.tempField);
        var json = JSON.stringify(this.tempField);
        console.log(json);
        this.formsservice.updateForm(this.no_form, this.formName, json).subscribe(function (_a) {
            _this.alertEditModal = false;
            _this.router.navigate(['/home/admin']);
        });
    };
    EditFormComponent.prototype.saveForm = function () {
        var _this = this;
        var json = JSON.stringify(this.tempField);
        var id = 'FORM' + Math.floor(Math.random() * 1000);
        this.formsservice.addForm(id, this.formName, json).subscribe(function (_a) {
            _this.alertSaveModal = false;
            _this.router.navigate(['/home/admin']);
        });
    };
    EditFormComponent.prototype.goToHome = function () {
        this.router.navigate(['/home/admin']);
    };
    EditFormComponent = __decorate([
        Component({
            selector: 'app-edit-form',
            templateUrl: './edit-form.component.html',
            styleUrls: ['./edit-form.component.scss']
        }),
        __metadata("design:paramtypes", [FormsService,
            Router,
            ActivatedRoute])
    ], EditFormComponent);
    return EditFormComponent;
}());
export { EditFormComponent };
//# sourceMappingURL=edit-form.component.js.map