import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormsService } from '../../core/providers/forms.service';

interface DataForm {
  key: string;
  type: string;
  templateOptions: Object;
}

interface GroupDataForm {
  key: string;
  type: string;
  templateOptions: Object;
  wrappers: Array<any>;
  fieldGroup: Array<any>;
}

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {

  formName = 'Untitled Form';
  labelName = 'Untitled Label';
  elementName: any;

  index: any;

  idForm: string;
  maxLength: number;
  rows: number;
  placeholder: string;
  formatText = "text";
  required = "false";
  readonly = "false";

  minRange: number;
  maxRange: number;
  stepRange: number;

  labelItem: string;
  valueItem: string;
  idItem: string;
  placeholderItem: string;

  selectedElement: any;

  formArray: Array<any> = [];
  itemRadio: Array<any> = [];
  itemCheck: Array<DataForm> = [];
  itemInput: Array<DataForm> = [];
  itemSelect: Array<any> = [];

  tempitemCheck: Array<DataForm> = [];
  tempitemInput: Array<DataForm> = [];

  tempdataitemCheck: Array<DataForm> = [];
  tempdataitemInput: Array<DataForm> = [];

  formGroup: FormGroup;

  showSeconds = false;

  sub: any;

  no_form: any = '';

  form = new FormGroup({});
  model = {};
  fields: Array<FormlyFieldConfig> = [];
  formField: Array<any> = [];
  tempField: Array<any> = [{ key: '', type: '', templateOptions: {} }];

  statusForm = false;

  updateForm: any;

  onEditedElement = false;
  enableEditedElement = false;
  editCheckbox = false;
  editMultiInput = false;
  editRadioGroup = false;
  editSelectElement = false;
  alertEditModal: boolean;
  alertSaveModal: boolean;

  constructor(
    private formsservice: FormsService,
    public router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    this.tempField = [];
    if (this.formField.length === 0) {
      this.statusForm = true;
    } else {
      this.statusForm = false;
    }
    this.sub = this.route
      .params
      .subscribe(params => {
        if (params) {
          this.no_form = params['id'];
          if (this.no_form) {
            this.updateForm = this.formsservice.getFormById(this.no_form).subscribe(({ data }) => {
              this.formField = JSON.parse(data.formById.json_form);
              this.tempField = JSON.parse(data.formById.json_form);
              this.fields = JSON.parse(data.formById.json_form);
              this.formName = data.formById.name_form;
              for (let i = 0; i < this.formField.length; i++) {
                if (this.formField[i].fieldGroup) {
                  this.tempitemInput = [];
                  this.tempdataitemInput = [];
                  this.tempitemCheck = [];
                  this.tempdataitemCheck = [];
                  for (let j = 0; j < this.formField[i].fieldGroup.length; j++) {
                    if (this.formField[i].wrappers[0] === "multiinput") {
                      this.tempitemInput.push(
                        {
                          key: this.formField[i].fieldGroup[j].key,
                          type: 'input',
                          templateOptions: {
                            type: this.formField[i].fieldGroup[j].templateOptions.type,
                            label: this.formField[i].fieldGroup[j].templateOptions.label,
                            placeholder: this.formField[i].fieldGroup[j].templateOptions.placeholder,
                            readonly: this.formField[i].fieldGroup[j].templateOptions.readonly
                          }
                        }
                      );
                      this.tempdataitemInput.push(
                        {
                          key: this.formField[i].fieldGroup[j].key,
                          type: 'input',
                          templateOptions: {
                            type: this.formField[i].fieldGroup[j].templateOptions.type,
                            label: this.formField[i].fieldGroup[j].templateOptions.label,
                            placeholder: this.formField[i].fieldGroup[j].templateOptions.placeholder,
                            readonly: this.formField[i].fieldGroup[j].templateOptions.readonly
                          }
                        }
                      );
                    } else if (this.formField[i].wrappers[0] === "multicheckbox") {
                      this.tempitemCheck.push(
                        {
                          key: this.formField[i].fieldGroup[j].key,
                          type: 'checkbox',
                          templateOptions: {
                            label: this.formField[i].fieldGroup[j].templateOptions.label,
                            value: this.formField[i].fieldGroup[j].templateOptions.value
                          }
                        }
                      );
                      this.tempdataitemCheck.push(
                        {
                          key: this.formField[i].fieldGroup[j].key,
                          type: 'checkbox',
                          templateOptions: {
                            label: this.formField[i].fieldGroup[j].templateOptions.label,
                            value: this.formField[i].fieldGroup[j].templateOptions.value
                          }
                        }
                      );
                    }
                  }
                  // if (this.formField[i].wrappers[0] === "multiinput") {
                  //   this.tempField.push({
                  //     key: this.formField[i].key,
                  //     type: this.formField[i].type,
                  //     wrappers: this.formField[i].wrappers,
                  //     templateOptions: this.formField[i].templateOptions,
                  //     fieldGroup: this.tempdataitemInput
                  //   });
                  // } else if (this.formField[i].wrappers[0] === "multicheckbox") {
                  //   this.tempField.push({
                  //     key: this.formField[i].key,
                  //     type: this.formField[i].type,
                  //     wrappers: this.formField[i].wrappers,
                  //     templateOptions: this.formField[i].templateOptions,
                  //     fieldGroup: this.tempdataitemCheck
                  //   });
                  // }
                }
              }
              console.log(this.tempitemInput);
              console.log(this.tempdataitemInput);
              console.log(this.tempitemCheck);
              console.log(this.tempdataitemCheck);
            });
          }
        } else {

        }
      });

  }

  randomID() {
    const id = this.labelName.toLowerCase();
    const words = id.split(" ");
    let word = "";
    let len = words.length;
    if (len > 4) {
      len = 4;
    }
    for (let i = 0; i < len; i++) {
      if (i === (len - 1)) {
        word += words[i];
      } else {
        word += words[i] + " ";
      }
    }
    this.idForm = word.replace(/ /g, "_");
  }

  generateTemplate(option) {
    if (option = "Yes-No") {
      this.itemRadio.push({ 'value': 'Tidak', 'key': 'tidak' }, { 'value': 'Ya', 'key': 'ya' });
    }
  }

  generateValue() {
    this.valueItem = this.labelItem.toLowerCase();
  }

  selectAllContent($event) {
    $event.target.select();
  }

  onSelectElementNameChange($event) {
    if (!this.onEditedElement) {
      if ($event.target.value === "multiinput") {
        this.itemInput = [];
        this.tempitemInput = [];
        this.tempdataitemInput = [];
      } else if ($event.target.value === "checkboxgroup") {
        this.itemCheck = [];
        this.tempitemCheck = [];
        this.tempdataitemCheck = [];
      } else if ($event.target.value === "label") {
        this.formatText = "normal";
      } else if ($event.target.value === "input") {
        this.formatText = "text";
      }
      console.log($event.target.value);
    }
  }

  showElement() {
    this.fields = this.formField;
  }

  saveElement() {
    if (this.maxLength == null) {
      this.maxLength = 90;
    }
    if (this.idForm == null) {
      this.idForm = Math.random().toString(36).substring(7);
    }

    switch (this.elementName) {
      case 'label':
        if (this.formatText === "header") {
          this.formField.push(
            {
              key: Math.random().toString(36).substring(7),
              type: 'header-text',
              templateOptions: {
                label: this.labelName
              }
            }
          );
          this.tempField.push(
            {
              key: Math.random().toString(36).substring(7),
              type: 'header-text',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        } else if (this.formatText === "bold") {
          this.formField.push(
            {
              key: Math.random().toString(36).substring(7),
              type: 'label-bold',
              templateOptions: {
                label: this.labelName
              }
            }
          );
          this.tempField.push(
            {
              key: Math.random().toString(36).substring(7),
              type: 'label-bold',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        } else {
          this.formField.push(
            {
              key: Math.random().toString(36).substring(7),
              type: 'label',
              templateOptions: {
                label: this.labelName
              }
            }
          );
          this.tempField.push(
            {
              key: Math.random().toString(36).substring(7),
              type: 'label',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        }
        this.saveFields();
        break;
      case 'input':
        this.formField.push(
          {
            key: this.idForm,
            type: 'input',
            templateOptions: {
              type: this.formatText,
              label: this.labelName,
              placeholder: this.placeholder,
              required: JSON.parse(this.required),
              readonly: JSON.parse(this.readonly)
            }
          }
        );
        this.tempField.push(
          {
            key: this.idForm,
            type: 'input',
            templateOptions: {
              type: this.formatText,
              label: this.labelName,
              placeholder: this.placeholder,
              required: JSON.parse(this.required),
              readonly: JSON.parse(this.readonly)
            }
          }
        );
        this.saveFields();
        break;
      case 'multiinput':
        this.formField.push({
          key: this.idForm,
          wrappers: ['multiinput'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.itemInput
        });
        this.tempField.push({
          key: this.idForm,
          wrappers: ['multiinput'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.tempitemInput
        });
        this.saveFields();
        break;
      case 'checkbox':
        this.formField.push(
          {
            key: this.idForm,
            type: 'checkbox',
            templateOptions: {
              label: this.labelName
            }
          }
        );
        this.tempField.push(
          {
            key: this.idForm,
            type: 'checkbox',
            templateOptions: {
              label: this.labelName
            }
          }
        );
        this.saveFields();
        break;
      case 'checkboxgroup':
        this.formField.push({
          key: this.idForm,
          wrappers: ['multicheckbox'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.itemCheck
        });
        this.tempField.push({
          key: this.idForm,
          wrappers: ['multicheckbox'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.tempdataitemCheck
        });
        this.saveFields();
        break;
      case 'radio':
        this.formField.push({
          key: this.idForm,
          type: 'radio',
          templateOptions: {
            label: this.labelName,
            options: this.itemRadio
          }
        });
        this.tempField.push({
          key: this.idForm,
          type: 'radio',
          templateOptions: {
            label: this.labelName,
            options: this.itemRadio
          }
        });
        this.saveFields();
        break;
      case 'textarea':
        this.formField.push(
          {
            key: this.idForm,
            type: 'textarea',
            templateOptions: {
              label: this.labelName,
              rows: this.rows,
              placeholder: this.placeholder,
              required: JSON.parse(this.required)
            }
          }
        );
        this.tempField.push(
          {
            key: this.idForm,
            type: 'textarea',
            templateOptions: {
              label: this.labelName,
              rows: this.rows,
              placeholder: this.placeholder,
              required: JSON.parse(this.required)
            }
          }
        );
        this.saveFields();
        break;
      case 'select':
        this.formField.push({
          key: this.idForm,
          type: 'select',
          templateOptions: {
            label: this.labelName,
            options: this.itemSelect
          }
        });
        this.tempField.push({
          key: this.idForm,
          type: 'select',
          templateOptions: {
            label: this.labelName,
            options: this.itemSelect
          }
        });
        this.saveFields();
        break;
      case 'timepicker':
        this.formField.push({
          key: this.idForm,
          type: 'timepicker',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.tempField.push({
          key: this.idForm,
          type: 'timepicker',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.saveFields();
        break;
      case 'datepicker':
        this.formField.push({
          key: this.idForm,
          type: 'datetime',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.tempField.push({
          key: this.idForm,
          type: 'datetime',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.saveFields();
        break;
      case 'range':
        this.formField.push(
          {
            key: this.idForm,
            type: 'range',
            templateOptions: {
              label: this.labelName,
              rangeClass: "calm",
              min: this.minRange,
              max: this.maxRange,
              step: this.stepRange,
              value: this.minRange,
            }
          }
        );
        this.tempField.push(
          {
            key: this.idForm,
            type: 'range',
            templateOptions: {
              label: this.labelName,
              rangeClass: "calm",
              min: this.minRange,
              max: this.maxRange,
              step: this.stepRange,
              value: this.minRange,
            }
          }
        );
        this.saveFields();
        break;
    }
    this.labelName = 'Untitled Label';
    this.maxLength = null;
    this.placeholder = '';
    this.idForm = '';
    this.elementName = null;
    this.minRange = null;
    this.maxRange = null;
    this.stepRange = null;
  }

  saveEditedElement() {
    if (this.maxLength == null) {
      this.maxLength = 90;
    }
    if (this.idForm == null) {
      this.idForm = Math.random().toString(36).substring(7);
    }

    this.tempdataitemInput = [];
    for (let i = 0; i < this.itemInput.length; i++) {
      this.tempdataitemInput.push(
        {
          key: this.itemInput[i].key,
          type: 'input',
          templateOptions: {
            type: 'text',
            label: this.itemInput[i].templateOptions['label'],
            placeholder: this.itemInput[i].templateOptions['placeholder'],
            readonly: false
          }
        }
      );
    }

    this.tempdataitemCheck = [];
    for (let i = 0; i < this.itemCheck.length; i++) {
      this.tempdataitemCheck.push(
        {
          key: this.itemCheck[i].key,
          type: 'checkbox',
          templateOptions: {
            label: this.itemCheck[i].templateOptions['label'],
            value: this.itemCheck[i].templateOptions['value']
          }
        }
      );
    }

    switch (this.elementName) {
      case 'label':
        if (this.formatText === "header") {
          this.formField.splice(this.selectedElement, 1,
            {
              key: Math.random().toString(36).substring(7),
              type: 'header-text',
              templateOptions: {
                label: this.labelName
              }
            }
          );
          this.tempField.splice(this.selectedElement, 1,
            {
              key: Math.random().toString(36).substring(7),
              type: 'header-text',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        } else if (this.formatText === "bold") {
          this.formField.splice(this.selectedElement, 1,
            {
              key: Math.random().toString(36).substring(7),
              type: 'label-bold',
              templateOptions: {
                label: this.labelName
              }
            }
          );
          this.tempField.splice(this.selectedElement, 1,
            {
              key: Math.random().toString(36).substring(7),
              type: 'label-bold',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        } else {
          this.formField.splice(this.selectedElement, 1,
            {
              key: Math.random().toString(36).substring(7),
              type: 'label',
              templateOptions: {
                label: this.labelName
              }
            }
          );
          this.tempField.splice(this.selectedElement, 1,
            {
              key: Math.random().toString(36).substring(7),
              type: 'label',
              templateOptions: {
                label: this.labelName
              }
            }
          );
        }
        this.saveFields();
        break;
      case 'input':
        this.formField.splice(this.selectedElement, 1,
          {
            key: this.idForm,
            type: 'input',
            templateOptions: {
              type: this.formatText,
              label: this.labelName,
              placeholder: this.placeholder,
              required: JSON.parse(this.required),
              readonly: JSON.parse(this.readonly)
            }
          }
        );
        this.tempField.splice(this.selectedElement, 1,
          {
            key: this.idForm,
            type: 'input',
            templateOptions: {
              type: this.formatText,
              label: this.labelName,
              placeholder: this.placeholder,
              required: JSON.parse(this.required),
              readonly: JSON.parse(this.readonly)
            }
          }
        );
        this.saveFields();
        break;
      case 'multiinput':
        this.formField.splice(this.selectedElement, 1, {
          key: this.idForm,
          wrappers: ['multiinput'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.itemInput
        });
        this.tempField.splice(this.selectedElement, 1, {
          key: this.idForm,
          wrappers: ['multiinput'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.tempdataitemInput
        });
        this.saveFields();
        break;
      case 'checkbox':
        this.formField.splice(this.selectedElement, 1,
          {
            key: this.idForm,
            type: 'checkbox',
            templateOptions: {
              label: this.labelName
            }
          }
        );
        this.tempField.splice(this.selectedElement, 1,
          {
            key: this.idForm,
            type: 'checkbox',
            templateOptions: {
              label: this.labelName
            }
          }
        );
        this.saveFields();
        break;
      case 'checkboxgroup':
        this.formField.splice(this.selectedElement, 1, {
          key: this.idForm,
          wrappers: ['multicheckbox'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.itemCheck
        });
        this.tempField.splice(this.selectedElement, 1, {
          key: this.idForm,
          wrappers: ['multicheckbox'],
          templateOptions: { label: this.labelName },
          fieldGroup: this.tempdataitemCheck
        });
        this.saveFields();
        break;
      case 'radio':
        this.formField.splice(this.selectedElement, 1, {
          key: this.idForm,
          type: 'radio',
          templateOptions: {
            label: this.labelName,
            options: this.itemRadio
          }
        });
        this.tempField.splice(this.selectedElement, 1, {
          key: this.idForm,
          type: 'radio',
          templateOptions: {
            label: this.labelName,
            options: this.itemRadio
          }
        });
        this.saveFields();
        break;
      case 'textarea':
        this.formField.splice(this.selectedElement, 1,
          {
            key: this.idForm,
            type: 'textarea',
            templateOptions: {
              label: this.labelName,
              rows: this.rows,
              placeholder: this.placeholder,
              required: JSON.parse(this.required)
            }
          }
        );
        this.tempField.splice(this.selectedElement, 1,
          {
            key: this.idForm,
            type: 'textarea',
            templateOptions: {
              label: this.labelName,
              rows: this.rows,
              placeholder: this.placeholder,
              required: JSON.parse(this.required)
            }
          }
        );
        this.saveFields();
        break;
      case 'select':
        this.formField.splice(this.selectedElement, 1, {
          key: this.idForm,
          type: 'select',
          templateOptions: {
            label: this.labelName,
            options: this.itemSelect
          }
        });
        this.tempField.splice(this.selectedElement, 1, {
          key: this.idForm,
          type: 'select',
          templateOptions: {
            label: this.labelName,
            options: this.itemSelect
          }
        });
        this.saveFields();
        break;
      case 'timepicker':
        this.formField.splice(this.selectedElement, 1, {
          key: this.idForm,
          type: 'timepicker',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.tempField.splice(this.selectedElement, 1, {
          key: this.idForm,
          type: 'timepicker',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.saveFields();
        break;
      case 'datepicker':
        this.formField.splice(this.selectedElement, 1, {
          key: this.idForm,
          type: 'datetime',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.tempField.splice(this.selectedElement, 1, {
          key: this.idForm,
          type: 'datetime',
          templateOptions: {
            label: this.labelName,
          }
        });
        this.saveFields();
        break;
      case 'range':
        this.formField.splice(this.selectedElement, 1,
          {
            key: this.idForm,
            type: 'range',
            templateOptions: {
              label: this.labelName,
              rangeClass: "calm",
              min: this.minRange,
              max: this.maxRange,
              step: this.stepRange,
              value: this.minRange,
            }
          }
        );
        this.tempField.splice(this.selectedElement, 1,
          {
            key: this.idForm,
            type: 'range',
            templateOptions: {
              label: this.labelName,
              rangeClass: "calm",
              min: this.minRange,
              max: this.maxRange,
              step: this.stepRange,
              value: this.minRange,
            }
          }
        );
        this.saveFields();
        break;
    }
    this.labelName = 'Untitled Label';
    this.maxLength = null;
    this.placeholder = '';
    this.idForm = '';
    this.elementName = null;
    this.minRange = null;
    this.maxRange = null;
    this.stepRange = null;
    this.onEditedElement = false;
  }

  cancelElement() {
    this.onEditedElement = false;
    this.labelName = 'Untitled Label';
    this.maxLength = null;
    this.placeholder = '';
    this.idForm = '';
    this.elementName = null;
    this.editMultiInput = false;
    this.editCheckbox = false;
    this.editMultiInput = false;
    this.editRadioGroup = false;
    this.itemInput = [];
    this.itemCheck = [];
    this.itemRadio = [];
    this.itemSelect = [];
    this.tempitemCheck = [];
    this.tempitemInput = [];
  }

  reorderElement() {
    this.enableEditedElement = true;
  }

  saveFields() {
    this.itemInput = [];
    this.itemCheck = [];
    this.itemRadio = [];
    this.itemSelect = [];
    this.tempitemCheck = [];
    this.tempitemInput = [];
    this.fields = [];
    for (let i = 0; i < this.formField.length; i++) {
      this.fields.push(this.formField[i]);
    }
    this.statusForm = false;
  }

  deleteItem(i, type) {
    switch (type) {
      case 'RADIO':
        this.itemRadio.splice(i, 1);
        break;
      case 'SELECT':
        this.itemSelect.splice(i, 1);
        break;
      case 'MULTIINPUT':
        this.itemInput.splice(i, 1);
        this.tempitemInput.splice(i, 1);
        this.tempdataitemInput.splice(i, 1);
        break;
      case 'CHECKBOX':
        this.itemCheck.splice(i, 1);
        this.tempitemCheck.splice(i, 1);
        this.tempdataitemCheck.splice(i, 1);
        break;
    }
  }

  editItem(i, type) {
    switch (type) {
      case 'RADIO':
        this.editRadioGroup = true;
        this.labelItem = this.itemRadio[i].value;
        this.valueItem = this.itemRadio[i].key;
        this.index = i;
        break;
      case 'SELECT':
        this.editSelectElement = true;
        this.labelItem = this.itemSelect[i].label;
        this.valueItem = this.itemSelect[i].value;
        this.index = i;
        break;
      case 'MULTIINPUT':
        this.editMultiInput = true;
        this.idItem = this.itemInput[i].key;
        this.labelItem = this.itemInput[i].templateOptions["label"];
        this.placeholderItem = this.itemInput[i].templateOptions["placeholder"];
        this.index = i;
        break;
      case 'CHECKBOX':
        this.editCheckbox = true;
        this.idItem = this.itemCheck[i].key;
        this.labelItem = this.itemCheck[i].templateOptions["label"];
        this.valueItem = this.itemCheck[i].templateOptions["value"];
        this.index = i;
        break;
    }
  }

  addInput() {
    this.itemInput.push(
      {
        key: this.idItem,
        type: 'input',
        templateOptions: {
          type: 'text',
          label: this.labelItem,
          placeholder: this.placeholderItem,
          readonly: false
        }
      });
    this.tempitemInput.push(
      {
        key: this.idItem,
        type: 'input',
        templateOptions: {
          type: 'text',
          label: this.labelItem,
          placeholder: this.placeholderItem,
          readonly: false
        }
      }
    );
    this.tempdataitemInput.push(
      {
        key: this.idItem,
        type: 'input',
        templateOptions: {
          type: 'text',
          label: this.labelItem,
          placeholder: this.placeholderItem,
          readonly: false
        }
      }
    );
    this.idItem = '';
    this.labelItem = '';
    this.placeholderItem = '';
    this.maxLength = null;
  }

  editInput() {
    this.itemInput.splice(this.index, 1, {
      key: this.idItem,
      type: 'input',
      templateOptions: {
        type: 'text',
        label: this.labelItem,
        placeholder: this.placeholderItem,
        readonly: false
      }
    });
    this.tempitemInput.splice(this.index, 1, {
      key: this.idItem,
      type: 'input',
      templateOptions: {
        type: 'text',
        label: this.labelItem,
        placeholder: this.placeholderItem,
        readonly: false
      }
    });
    this.tempdataitemInput.splice(this.index, 1, {
      key: this.idItem,
      type: 'input',
      templateOptions: {
        type: 'text',
        label: this.labelItem,
        placeholder: this.placeholderItem,
        readonly: false
      }
    });
    this.idItem = '';
    this.labelItem = '';
    this.placeholderItem = '';
    this.maxLength = null;
    this.editMultiInput = false;
  }

  editRadio() {
    this.itemRadio.splice(this.index, 1, {
      'value': this.labelItem, 'key': this.valueItem
    });
    this.labelItem = '';
    this.valueItem = '';
    this.editRadioGroup = false;
  }

  addRadio() {
    this.itemRadio.push({ 'value': this.labelItem, 'key': this.valueItem });
    this.labelItem = '';
    this.valueItem = '';
  }

  addCheck() {
    this.itemCheck.push(
      {
        key: this.idItem,
        type: 'checkbox',
        templateOptions: {
          label: this.labelItem
        }
      }
    );
    this.tempitemCheck.push(
      {
        key: this.idItem,
        type: 'checkbox',
        templateOptions: {
          label: this.labelItem
        }
      }
    );
    this.tempdataitemCheck.push(
      {
        key: this.idItem,
        type: 'checkbox',
        templateOptions: {
          label: this.labelItem
        }
      }
    );
    this.idItem = '';
    this.labelItem = '';
    this.valueItem = '';
  }

  editCheck() {
    this.itemCheck.splice(this.index, 1, {
      key: this.idItem,
      type: 'checkbox',
      templateOptions: {
        label: this.labelItem
      }
    });
    this.tempitemCheck.splice(this.index, 1,
      {
        key: this.idItem,
        type: 'checkbox',
        templateOptions: {
          label: this.labelItem
        }
      }
    );
    this.tempdataitemCheck.splice(this.index, 1,
      {
        key: this.idItem,
        type: 'checkbox',
        templateOptions: {
          label: this.labelItem
        }
      }
    );
    this.idItem = '';
    this.labelItem = '';
    this.valueItem = '';
    this.editCheckbox = false;
  }

  editSelect() {
    this.itemSelect.splice(this.index, 1, {
      'label': this.labelItem, 'value': this.valueItem
    });
    this.labelItem = '';
    this.valueItem = '';
    this.editSelectElement = false;
  }

  addSelect() {
    this.itemSelect.push({ 'label': this.labelItem, 'value': this.valueItem });
    this.labelItem = '';
    this.valueItem = '';
  }

  editElement(index: any) {
    this.selectedElement = index;
    console.log(this.formField[index]);
    const element = this.formField[index];
    this.onEditedElement = true;
    if (element.type === 'header-text') {
      this.elementName = 'label';
      this.formatText = 'header';
      this.labelName = element.templateOptions.label;
    } else if (element.type === 'label-bold') {
      this.elementName = 'label';
      this.formatText = 'bold';
      this.labelName = element.templateOptions.label;
    } else if (element.type === 'label') {
      this.elementName = 'label';
      this.formatText = 'normal';
      this.labelName = element.templateOptions.label;
    } else if (element.type === 'input') {
      this.elementName = 'input';
      this.labelName = element.templateOptions.label;
      this.idForm = element.key;
      this.placeholder = element.templateOptions.placeholder;
      this.formatText = element.templateOptions.type;
      this.required = element.templateOptions.required;
      this.readonly = element.templateOptions.readonly;
    } else if (element.type === 'textarea') {
      this.elementName = 'textarea';
      this.labelName = element.templateOptions.label;
      this.idForm = element.key;
      this.rows = element.templateOptions.rows;
      this.placeholder = element.templateOptions.placeholder;
    } else if (element.type === 'checkbox') {
      this.elementName = 'checkbox';
      this.labelName = element.templateOptions.label;
      this.idForm = element.key;
    } else if (element.type === 'radio') {
      this.elementName = 'radio';
      this.labelName = element.templateOptions.label;
      this.idForm = element.key;
      this.itemRadio = element.templateOptions.options;
    } else if (element.type === 'select') {
      this.elementName = 'select';
      this.labelName = element.templateOptions.label;
      this.idForm = element.key;
      this.itemSelect = element.templateOptions.options;
    } else if (element.type === 'datetime') {
      this.elementName = 'datepicker';
      this.labelName = element.templateOptions.label;
      this.idForm = element.key;
    } else if (element.type === 'timepicker') {
      this.elementName = 'timepicker';
      this.labelName = element.templateOptions.label;
      this.idForm = element.key;
    } else if (element.type === 'range') {
      this.elementName = 'range';
      this.labelName = element.templateOptions.label;
      this.idForm = element.key;
      this.minRange = element.templateOptions.min;
      this.maxRange = element.templateOptions.max;
      this.stepRange = element.templateOptions.step;
    } else {
      if (element.wrappers[0] === "multiinput") {
        this.elementName = 'multiinput';
        this.labelName = element.templateOptions.label;
        this.idForm = element.key;
        this.itemInput = [];
        this.tempitemInput = [];
        for (let i = 0; i < element.fieldGroup.length; i++) {
          this.itemInput.push(element.fieldGroup[i]);
          this.tempitemInput.push(element.fieldGroup[i]);
        }
      } else if (element.wrappers[0] === "multicheckbox") {
        this.elementName = 'checkboxgroup';
        this.labelName = element.templateOptions.label;
        this.idForm = element.key;
        this.itemCheck = [];
        this.tempitemCheck = [];
        this.tempdataitemCheck = [];
        for (let i = 0; i < element.fieldGroup.length; i++) {
          this.itemCheck.push(element.fieldGroup[i]);
          this.tempitemCheck.push(element.fieldGroup[i]);
          this.tempdataitemCheck.push(element.fieldGroup[i]);
        }
        console.log(this.itemCheck);
        console.log(this.tempitemCheck);
        console.log(this.tempdataitemCheck);
      }
    }
  }

  deleteElement(index: any) {
    console.log(index);
    console.log(this.formField);
    console.log(this.tempdataitemInput);
    this.formField.splice(index, 1);
    this.tempField.splice(index, 1);
    this.fields = [];
    console.log(this.formField);
    console.log(this.tempField);
    for (let i = 0; i < this.formField.length; i++) {
      this.fields.push(this.formField[i]);
    }
  }

  moveElement(index: any, dir: number) {
    if ((index === 0 && dir === -1) || (index === (this.formField.length - 1) && dir === +1)) {

    } else {
      this.array_move(this.formField, index, index + dir);
      this.array_move(this.tempField, index, index + dir);
      this.saveFields();
    }
  }

  array_move(arr, old_index, new_index) {
    while (old_index < 0) {
      old_index += arr.length;
    }
    while (new_index < 0) {
      new_index += arr.length;
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
  }

  editForm() {
    console.log(this.tempField);
    const json = JSON.stringify(this.tempField);
    console.log(json);
    this.formsservice.updateForm(this.no_form, this.formName, json).subscribe(({}) => {
      this.alertEditModal = false;
      this.router.navigate(['/home/admin']);
    });
  }

  saveForm() {
    const json = JSON.stringify(this.tempField);
    const id = 'FORM' + Math.floor(Math.random() * 1000);
    this.formsservice.addForm(id, this.formName, json).subscribe(({}) => {
      this.alertSaveModal = false;
      this.router.navigate(['/home/admin']);
    });
  }

  goToHome() {
    this.router.navigate(['/home/admin']);
  }

}
