var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { RmserviceService } from './../../core/providers/rmservice.service';
import { FormBuilder } from '@angular/forms';
import { Component, trigger, transition, style, animate } from '@angular/core';
var RequestEditComponent = /** @class */ (function () {
    function RequestEditComponent(rmService, formBuilder) {
        this.rmService = rmService;
        this.formBuilder = formBuilder;
        this.dataDoctors = [];
        this.dataPasien = [];
        this.dataCT = [];
        this.icdfix = [];
        this.icd9 = [];
        this.listicd = [];
        this.reviewPasien = [];
        this.detailpasien = [];
        this.openpasien = false;
        this.openRequest = false;
        this.requestDone = false;
        this.autocompleteStatusTindakan = false;
        this.NoMedrec = "";
        this.openAlert = false;
        this.openLoading = false;
        this.placeholder = 'Select a date';
        this.myDatePickerOptions = {
            dateFormat: 'yyyy/mm/dd',
            todayBtnTxt: 'Today',
            firstDayOfWeek: 'mo',
            sunHighlight: true,
            inline: false,
            disableUntil: { year: 2016, month: 8, day: 10 },
        };
        this.datapasiendetail = [];
        this.formEmail = this.formBuilder.group({
            'no': [''],
            'alasan': ['']
        });
    }
    RequestEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.rmService.getDataReportBasicNodok(0, 10).subscribe(function (_a) {
            var data = _a.data;
            var datadoctors = data.examineAllByNodok.slice();
            for (var i = 0; i < datadoctors.length; i++) {
                // console.log("DOKTER : " + datadoctors[i].nodok[0].nodok);
                _this.rmService.getDataPatientExamine(datadoctors[i].nodok[0].nodok).subscribe(function (_a) {
                    var data = _a.data;
                    var str = JSON.stringify(data);
                    var pars = JSON.parse(str);
                    var datadoctor = [pars.examineByNodok];
                    var counter = datadoctor[0].length;
                    if (counter > 0) {
                        if (counter > 1) {
                            _this.dataDoctors.push({
                                nama_dok: datadoctor[0][0].nama_dok,
                                poli: datadoctor[0][0].poli,
                                nodok: datadoctor[0][0].nodok,
                                badge: counter
                            });
                        }
                        else {
                            _this.dataDoctors.push({
                                nama_dok: datadoctor[0][0].nama_dok,
                                poli: datadoctor[0][0].poli,
                                nodok: datadoctor[0][0].nodok,
                                badge: counter
                            });
                        }
                    }
                });
            }
            console.log(_this.dataDoctors);
        });
    };
    RequestEditComponent.prototype.openPasien = function (index, nodok, nama_dok) {
        var _this = this;
        this.openpasien = true;
        this.selectedRow = index;
        this.DoctorName = nama_dok;
        this.rmService.getDataPatientExamine(nodok).subscribe(function (_a) {
            var data = _a.data;
            var datapasien = data.examineByNodok.slice();
            if (datapasien) {
                _this.dataPasien = datapasien;
            }
            else {
                _this.dataPasien = [];
            }
        });
    };
    RequestEditComponent.prototype.editRequest = function (no_medrec) {
        var _this = this;
        this.NoMedrec = "";
        this.openRequest = true;
        this.detailpasien = [];
        this.rmService.getDataReportBasicById(no_medrec).subscribe(function (_a) {
            var data = _a.data;
            var str = JSON.stringify(data);
            var pars = JSON.parse(str);
            var detailpasien = pars.examineJoinById;
            _this.datapasiendetail = [detailpasien];
            _this.noperiksa = detailpasien.no_periksa;
        });
    };
    RequestEditComponent.prototype.closePasien = function () {
        this.openpasien = false;
    };
    RequestEditComponent.prototype.emailRequest = function () {
        return __awaiter(this, void 0, void 0, function () {
            var yearstart, monthstart, daystart, datestart;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(this.mydate && this.alasan)) return [3 /*break*/, 3];
                        this.openLoading = true;
                        yearstart = this.mydate.date.year;
                        monthstart = this.mydate.date.month;
                        daystart = this.mydate.date.day;
                        if (monthstart < 10) {
                            monthstart = '0' + monthstart;
                        }
                        if (daystart < 10) {
                            daystart = '0' + daystart;
                        }
                        datestart = yearstart + "-" + monthstart + "-" + daystart;
                        if (!this.alasan) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.rmService.emailrequest(this.noperiksa, this.alasan, datestart).subscribe(function (data) {
                                _this.openLoading = false;
                                _this.messageAlert = "Request Edit Berhasil Terkirim";
                                _this.openAlert = true;
                                _this.requestDone = true;
                                _this.mydate = null;
                                _this.alasan = null;
                            }, function (error) { console.log(error); _this.messageAlert = "Request Edit Gagal Terkirim"; _this.openAlert = true; })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        this.messageAlert = "Isi Tanggal dan Alasan terlebih dahulu";
                        this.openAlert = true;
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    RequestEditComponent.prototype.closeNotif = function () {
        this.openAlert = false;
        if (this.requestDone) {
            this.openRequest = false;
        }
        this.requestDone = false;
    };
    RequestEditComponent = __decorate([
        Component({
            styleUrls: ['./requestedit.component.scss'],
            templateUrl: './requestedit.component.html',
            animations: [
                trigger('fadeInOut', [
                    transition(':enter', [
                        style({ width: 0 }),
                        animate(300, style({ width: 600 }))
                    ]),
                    transition(':leave', [
                        animate(300, style({ width: 0 }))
                    ]),
                ])
            ]
        }),
        __metadata("design:paramtypes", [RmserviceService, FormBuilder])
    ], RequestEditComponent);
    return RequestEditComponent;
}());
export { RequestEditComponent };
//# sourceMappingURL=requestedit.component.js.map