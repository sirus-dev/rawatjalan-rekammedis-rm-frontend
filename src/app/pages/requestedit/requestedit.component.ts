import { RmserviceService } from './../../core/providers/rmservice.service';
import { IMyDpOptions } from 'mydatepicker';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, trigger, transition, style, animate} from '@angular/core';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    styleUrls: ['./requestedit.component.scss'],
    templateUrl: './requestedit.component.html',
    animations: [
        trigger('fadeInOut', [
            transition(':enter', [
                style({ width: 0 }),
                animate(300, style({ width: 600 }))
            ]),
            transition(':leave', [
                animate(300, style({ width: 0 }))
            ]),
        ])
    ]
})
export class RequestEditComponent implements OnInit {
    searchdoctor: any;
    searchpatient: any;

    dataDoctors: Array<any> = [];
    dataPasien: Array<any> = [];
    dataCT: Array<any> = [];
    icdfix: Array<any> = [];
    icd9: Array<any> = [];
    listicd: Array<any> = [];
    reviewPasien: Array<any> = [];
    detailpasien: Array<{ title: string, value: string }> = [];
    norm: string;
    openpasien: boolean = false;
    selectedRow: string;
    str: string;
    DoctorName: string;
    examineStatus: string;
    openRequest = false;
    requestDone = false;
    autocompleteStatusTindakan = false;
    searchQueryICD9: any;

    NoMedrec: any = "";
    noperiksa: any;
    alasan: any;

    messageAlert: any;
    openAlert: boolean = false;
    openLoading: boolean = false;
    placeholder: string = 'Select a date';

    myDatePickerOptions: IMyDpOptions = {
        dateFormat: 'yyyy/mm/dd',
        todayBtnTxt: 'Today',
        firstDayOfWeek: 'mo',
        sunHighlight: true,
        inline: false,
        disableUntil: { year: 2016, month: 8, day: 10 },
    };

    mydate: any;

    datapasiendetail: Array<any> = [];

    private formEmail: FormGroup;

    constructor(private rmService: RmserviceService, private formBuilder: FormBuilder) {


        this.formEmail = this.formBuilder.group({
            'no': [''],
            'alasan': ['']
        });

    }


    ngOnInit() {

        this.rmService.getDataReportBasicNodok(0, 10).subscribe(({ data }) => {
            let datadoctors = [...data.examineAllByNodok];
            for (let i = 0; i < datadoctors.length; i++) {
                // console.log("DOKTER : " + datadoctors[i].nodok[0].nodok);
                this.rmService.getDataPatientExamine(datadoctors[i].nodok[0].nodok).subscribe(({ data }) => {
                    let str = JSON.stringify(data);
                    let pars = JSON.parse(str);
                    let datadoctor = [pars.examineByNodok];
                    let counter = datadoctor[0].length;
                    if (counter > 0) {
                        if (counter > 1) {
                            this.dataDoctors.push({
                                nama_dok: datadoctor[0][0].nama_dok,
                                poli: datadoctor[0][0].poli,
                                nodok: datadoctor[0][0].nodok,
                                badge: counter
                            });
                        } else {
                            this.dataDoctors.push({
                                nama_dok: datadoctor[0][0].nama_dok,
                                poli: datadoctor[0][0].poli,
                                nodok: datadoctor[0][0].nodok,
                                badge: counter
                            });
                        }
                    }
                });
            }
            console.log(this.dataDoctors);
        });
    }

    openPasien(index: string, nodok: any, nama_dok: string) {
        this.openpasien = true;
        this.selectedRow = index;
        this.DoctorName = nama_dok;
        this.rmService.getDataPatientExamine(nodok).subscribe(({ data }) => {
            let datapasien = [...data.examineByNodok];
            if (datapasien) {
                this.dataPasien = datapasien;
            } else {
                this.dataPasien = [];
            }
        });
    }

    editRequest(no_medrec: string) {
        this.NoMedrec = "";
        this.openRequest = true;
        this.detailpasien = [];
        this.rmService.getDataReportBasicById(no_medrec).subscribe(({ data }) => {
            let str = JSON.stringify(data);
            let pars = JSON.parse(str);
            let detailpasien = pars.examineJoinById;
            this.datapasiendetail = [detailpasien];
            this.noperiksa = detailpasien.no_periksa;
        });
    }

    closePasien() {
        this.openpasien = false;
    }

    async emailRequest() {
        if (this.mydate && this.alasan) {
            this.openLoading = true;
            let yearstart = this.mydate.date.year;
            let monthstart = this.mydate.date.month;
            let daystart = this.mydate.date.day;
            if (monthstart < 10) {
                monthstart = '0' + monthstart;
            }
            if (daystart < 10) {
                daystart = '0' + daystart;
            }

            let datestart = yearstart + "-" + monthstart + "-" + daystart;
            if (this.alasan) {
                await this.rmService.emailrequest(this.noperiksa, this.alasan, datestart).subscribe(
                    data => {
                        this.openLoading = false;
                        this.messageAlert = "Request Edit Berhasil Terkirim";
                        this.openAlert = true;
                        this.requestDone = true;
                        this.mydate = null;
                        this.alasan = null;
                    },
                    error => { console.log(error); this.messageAlert = "Request Edit Gagal Terkirim"; this.openAlert = true; }
                );
            }
        }else {
            this.messageAlert = "Isi Tanggal dan Alasan terlebih dahulu";
            this.openAlert = true;
        }
    }

    closeNotif() {
        this.openAlert = false;
        if (this.requestDone) {
            this.openRequest = false;
        }
        this.requestDone = false;
    }

}
