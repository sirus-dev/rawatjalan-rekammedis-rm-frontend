import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
 selector: 'field-label',
 template: `
    <span style="display: block">{{ to.label }}</span>
 `,
})

export class FieldLabelModelComponent extends FieldType {}
