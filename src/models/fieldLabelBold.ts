import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
 selector: 'field-label-bold',
 template: `
    <span style="display: block"><b>{{ to.label }}</b></span>
 `,
})

export class FieldLabelBoldModelComponent extends FieldType {}
