import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
 selector: 'field-range',
 template: `
    <label for="{{to.label}}">{{to.label}}</label>
    <input class="form-control" type="range" min={{to.min}} max={{to.max}} step={{to.step}} [formControl]="formControl" [formlyAttributes]="field" list="steplist">
 `,
})

export class FieldRangeModelComponent extends FieldType {}
