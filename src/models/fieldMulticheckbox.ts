import { Component, ViewChild, ViewContainerRef } from '@angular/core';
// import { FieldType } from '@ngx-formly/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
 selector: 'field-Multicheckbox',
 template: `
    <div class="card">
      <label class="card-header"><b>{{ to.label }}</b></label>
      <div class="card-body" fieldGroup>
        <ng-container #fieldComponent></ng-container>
      </div>
    </div>
 `,
})

export class FieldMulticheckboxModelComponent extends FieldWrapper {
    @ViewChild('fieldComponent', { read: ViewContainerRef }) fieldComponent: ViewContainerRef;
}
