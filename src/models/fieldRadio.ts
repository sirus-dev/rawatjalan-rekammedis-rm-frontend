import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'field-radio',
  template: `
  <div class="form-check" *ngFor="let radio of to.options">
    <input class="form-check-input" [formControl]="formControl" [formlyAttributes]="field"
    type="radio" id="{{radio.key}}" value="{{radio.key}}">
    <label class="form-check-label" for="{{radio.key}}">{{radio.value}}</label>
  </div>
 `,
})
export class FieldRadioModelComponent extends FieldType { }
