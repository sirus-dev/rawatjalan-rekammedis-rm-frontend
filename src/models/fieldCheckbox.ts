import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'field-checkbox',
  template: `
  <div class="form-check">
    <label class="form-check-label">
        <input type="{{ to.type }}" class="form-check-input"
        [formControl]="formControl"
        [formlyAttributes]="field">{{ to.label }}
    </label>
  </div>
 `,
})
export class FieldCheckboxModelComponent extends FieldType { }
