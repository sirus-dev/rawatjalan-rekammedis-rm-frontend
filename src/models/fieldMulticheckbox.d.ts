import { ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';
export declare class FieldMulticheckboxModelComponent extends FieldWrapper {
    fieldComponent: ViewContainerRef;
}
