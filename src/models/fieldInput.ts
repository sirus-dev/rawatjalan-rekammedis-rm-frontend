import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
 selector: 'field-input',
 template: `
    <input class="form-control" type={{to.type}} [formControl]="formControl" [formlyAttributes]="field">
 `,
})

export class FieldInputModelComponent extends FieldType {}
