import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
 selector: 'field-textarea',
 template: `
 <div class="form-group">
  <label for="{{to.label}}">{{to.label}}</label>
  <textarea class="form-control" [formControl]="formControl"
    rows=5 placeholder="{{ to.placeholder }}" [formlyAttributes]="field" ></textarea>
</div>
 `,
})

export class FieldTextareaModelComponent extends FieldType {}
