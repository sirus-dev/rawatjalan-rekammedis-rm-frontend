import { ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';
export declare class FieldMultiinputModelComponent extends FieldWrapper {
    fieldComponent: ViewContainerRef;
}
