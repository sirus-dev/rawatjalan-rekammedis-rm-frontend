import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
 selector: 'field-header-text',
 template: `
    <span style="font-size:24px; display: block">{{ to.label }}</span>
 `,
})

export class FieldHeaderTextModelComponent extends FieldType {}
