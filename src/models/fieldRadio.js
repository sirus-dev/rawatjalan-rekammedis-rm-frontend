var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
var FieldRadioModelComponent = /** @class */ (function (_super) {
    __extends(FieldRadioModelComponent, _super);
    function FieldRadioModelComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FieldRadioModelComponent = __decorate([
        Component({
            selector: 'field-radio',
            template: "\n  <div class=\"form-check\" *ngFor=\"let radio of to.options\">\n    <input class=\"form-check-input\" [formControl]=\"formControl\" [formlyAttributes]=\"field\"\n    type=\"radio\" id=\"{{radio.key}}\" value=\"{{radio.key}}\">\n    <label class=\"form-check-label\" for=\"{{radio.key}}\">{{radio.value}}</label>\n  </div>\n ",
        })
    ], FieldRadioModelComponent);
    return FieldRadioModelComponent;
}(FieldType));
export { FieldRadioModelComponent };
//# sourceMappingURL=fieldRadio.js.map