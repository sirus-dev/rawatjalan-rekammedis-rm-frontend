import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
 selector: 'field-datetime',
 template: `
 <div class="form-group">
 <strong>{{to.label}}</strong>
 <div class='input-group date' id='datetimepicker1'>
     <input [formControl]="formControl" [formlyAttributes]="field" type='date' class="form-control" />
     <span class="input-group-addon">
     </span>
 </div>
</div>
 `,
})

export class FieldDatetimeModelComponent extends FieldType {}
